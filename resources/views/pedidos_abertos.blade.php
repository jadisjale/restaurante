@extends('template')

@section('title', 'PEDIDOS ABERTOS')

@section('content')

  <style>
    .center {
      padding-left: 10px;
    }
  </style>

  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>PEDIDOS EM ABERTO</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <p class="text-muted font-13 m-b-30">
          LISTA DE TODOS OS PEDIDOS EM ABERTOS.
        </p>

        <table id="table_sell_active" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
          <thead>
          <tr>
            {{--<th>PEDIDO</th>--}}
            <th>MESAS</th>
            <th>DATA / HORA</th>
            <th>VALOR TOTAL</th>
            <th width="10px">EDITAR</th>
            <th width="10px">FINALIZAR</th>
          </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>

  <script src="vendors/jquery/dist/jquery.min.js"></script>
  <script>
      $(document).ready(function () {
          sell_active.index();
      });
  </script>

@endsection