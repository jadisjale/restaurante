@extends('template')

@section('title', 'BALANÇO')

@section('content')

   <style>
       #table_waiters_wrapper {
           padding-bottom: 34px !important;
       }
   </style>

  <div class="row">
    <div class="col-md-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>INFORME A DATA DE INÍCIO E FIM PARA VER AS FATURAS</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          <form class="form-horizontal" id="form-fatura">
            <fieldset>
              <div class="control-group">
                <div class="controls">
                  <label for="date">INFORME A DATA DE INÍCIO E FIM:</label>
                  <div class="input-prepend input-group">
                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                    <input type="text" style="width: 200px" name="date" id="date" class="form-control mousetrap" value="">
                  </div>
                </div>
              </div>
            </fieldset>
              <button class="btn btn-primary" type="submit" id="btn-consultar"> <i class="fa fa-search" aria-hidden="true"></i> CONSULTAR</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12" id="x-fatura">
          <div class="x_panel">
              <div class="x_title">
                      <h2>RESULTADO | VALOR TOTAL DO PERÍODO: <b style="color: #d9534f" id="value_all"> R$ 0,00 </b></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <button type="button" class="btn btn-danger" id="result_waiters" style="display: none">VISUALIZAR RESULTADO DOS GARÇONS</button>
                  </ul>
                  <div class="clearfix"></div>
              </div>
              <div class="x_content">
                  <table id="table_fatura" class="table table-striped table-bordered" width="100%">
                      <thead>
                      <tr>
                          <th>PEDIDO</th>
                          <th>MESAS</th>
                          <th>VALOR DO PEDIDO</th>
                          <th>DATA DO PEDIDO</th>
                          <th width="10px">AÇÕES</th>
                      </tr>
                      </thead>
                  </table>
              </div>
          </div>
      </div>
  </div>

  <script src="vendors/jquery/dist/jquery.min.js"></script>
  <script>
      $(document).ready(function () {
          fatura.board.index();
      });
  </script>

@endsection