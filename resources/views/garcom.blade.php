@extends('template')

@section('title', 'GARÇOM')

@section('content')

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>CADASTRO DE GARÇONS</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          <form class="form-horizontal form-label-left input_mask" id="save_waiter" method="post">
            <label for="name">NOME DO GARÇOM:</label>
            <input class="form-control mousetrap" id="name" name="name" type="text" placeholder="EX: JOSÉ SILVA"/>
            <input type="hidden" name="status" value="1"/>
            {{--<label for="status">GARÇOM DISPONÍVEL / INDISPONÍVEL:</label>--}}
            {{--<select id="status" name="status" class="form-control">--}}
              {{--<option value="" selected>SELECIONE UMA OPÇÃO</option>--}}
              {{--<option value="1">ATIVO</option>--}}
              {{--<option value="0">INATIVO</option>--}}
            {{--</select>--}}
            <br>
            <button type="submit" class="btn btn-primary" id="btn-submit"> <i class="fa fa-floppy-o" aria-hidden="true"></i> SALVAR GARÇOM</button>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>LISTA DE GARÇONS</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <ul class="nav nav-tabs nav-justified" role="tablist" data-original-title="" title="">
            <li id="li-active" role="presentation" class="active" data-original-title="" title=""><a href="#disponivel" aria-controls="home" role="tab" data-toggle="tab" data-original-title="" title="">DISPONÍVEL</a></li>
            <li id="li-disabled" role="presentation" data-original-title="" title=""><a href="#indisponivel" aria-controls="profile" role="tab" data-toggle="tab" data-original-title="" title="">INDISPONÍVEL</a></li>
          </ul>
          <br>
          <div class="scroll-panel" data-original-title="" title=""></div>
          <div class="tab-content" data-original-title="" title="">
            <div role="tabpanel" class="tab-pane active" id="disponivel" data-original-title="" title="">
              <table id="waiter-active" class="table table-striped table-bordered" width="100%">
                <thead>
                <tr>
                  <th>NOME</th>
                  <th width="10px">DESATIVAR</th>
                </tr>
                </thead>
              </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="indisponivel" data-original-title="" title="">
              <table id="waiter-inactive" class="table table-striped table-bordered" width="100%">
                <thead>
                <tr>
                  <th>NOME</th>
                  <th width="10px">ATIVAR</th>
                </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script src="vendors/jquery/dist/jquery.min.js"></script>
  <script>
      $(document).ready(function () {
          waiter.index();
      });
  </script>
@endsection