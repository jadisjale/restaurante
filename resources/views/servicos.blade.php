@extends('template')

@section('title', 'MESAS')

@section('content')

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>CADASTRO DE SERVIÇOS</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          <form class="form-horizontal form-label-left input_mask" id="save_service" method="post">
            <label for="description">NOME DO SERVIÇO:</label>
            <input class="form-control mousetrap" id="name" name="name" type="text" placeholder="EX: TAXA DE EMBALAGEM"/>
            <br>
            <label for="description">VALOR:</label>
            <input class="form-control money mousetrap" id="value" name="value" type="text" placeholder="EX: R$ 3,00"/>
            <input type="hidden" name="status" value="1" id="status"/>
            {{--<label for="status">MESA DISPONÍVEL / INDISPONÍVEL:</label>--}}
            {{--<select id="status" name="status" class="form-control">--}}
              {{--<option value="" selected>SELECIONE UMA OPÇÃO</option>--}}
              {{--<option value="1">DISPONÍVEL</option>--}}
              {{--<option value="0">INDISPONÍVEL</option>--}}
            {{--</select>--}}
            <br>
            <button type="submit" class="btn btn-primary" id="btn-submit"><i class="fa fa-floppy-o" aria-hidden="true"></i> SALVAR</button>
          </form>
        </div>
      </div>
    </div>


    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>LISTA DE SERVIÇOS</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <ul class="nav nav-tabs nav-justified" role="tablist" data-original-title="" title="">
            <li id="li-active" role="presentation" class="active" data-original-title="" title=""><a href="#disponivel" aria-controls="home" role="tab" data-toggle="tab" data-original-title="" title="">DISPONÍVEL</a></li>
            <li id="li-disabled" role="presentation" data-original-title="" title=""><a href="#indisponivel" aria-controls="profile" role="tab" data-toggle="tab" data-original-title="" title="">INDISPONÍVEL</a></li>
          </ul>
          <br>
          <div class="scroll-panel" data-original-title="" title=""></div>
          <div class="tab-content" data-original-title="" title="">
            <div role="tabpanel" class="tab-pane active" id="disponivel" data-original-title="" title="">
              <table id="service-active" class="table table-striped table-bordered" width="100%">
                <thead>
                <tr>
                  <th>SERVIÇO</th>
                  <th>VALOR</th>
                  <th width="10px">DESATIVAR</th>
                  <th width="10px">EDITAR</th>
                </tr>
                </thead>
              </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="indisponivel" data-original-title="" title="">
              <table id="service-disabled" class="table table-striped table-bordered" width="100%">
                <thead>
                <tr>
                  <th>SERVIÇO</th>
                  <th>VALOR</th>
                  <th width="10px">DESATIVAR</th>
                </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" tabindex="-1" role="dialog" id="modal-update-service">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">DETALHES DO SERVIÇO</h4>
        </div>
        <div class="modal-body">
          <ul class="list-group">
            <li class="list-group-item"><b>NOME: </b> <span id="ul-name"> </span></li>
            <li class="list-group-item"><b>VALOR: </b> <span id="ul-value"> </span></li>
          </ul>
          <ul class="list-group">
            <label for="update-value"> ATUALIZAR NOME DO PRODUTO: </label>
            <input type="text" id="update-name" class="form-control mousetrap" placeholder="ATUALIZAR NOME DO PRODUTO"/>
            <label for="update-value"> ATUALIZAR VALOR PARA: </label>
            <input type="text" id="update-value" class="form-control money mousetrap"/>
            <input type="hidden" id="update-id" class="form-control mousetrap"/>
          </ul>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
          <button type="button" class="btn btn-primary" id="btn-update-service">SALVAR ALTERAÇÕES</button>
        </div>
      </div>
    </div>
  </div>

  <script src="vendors/jquery/dist/jquery.min.js"></script>
  <script>
      $(document).ready(function () {
          service.index();
      });
  </script>
@endsection