@extends('template')

@section('title', 'USUÁRIO')

@section('content')

  <div class="row">
    <div class="col-md-6 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>CADASTRO DE USUÁRIO</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          <form class="form-horizontal form-label-left input_mask" id="save_user" method="post">
            <label for="name">NOME DO USUÁRIO:</label>
            <input class="form-control mousetrap" id="name" name="name" type="text" placeholder="EX: ANTONIO DA SILVA" title="CAMPO NOME OBRIGATÓRIO"/>
            <br>
              <label for="login">LOGIN:</label>
              <input class="form-control mousetrap" id="login" name="login" type="text" placeholder="EX: ANTONIO" title="CAMPO LOGIN OBRIGATÓRIO"/>
              <br>
                <label for="password">SENHA:</label>
                <input class="form-control mousetrap" id="password" name="password" type="password" placeholder="XXXXXX" title="CAMPO SENHA OBRIGATÓRIO"/>
                <br>
            <button type="submit" class="btn btn-primary" id="btn-submit"><i class="fa fa-floppy-o" aria-hidden="true"></i> SALVAR USUÁRIO</button>
          </form>
        </div>
        </div>
      </div>
    </div>

  <script src="vendors/jquery/dist/jquery.min.js"></script>
  <script>
      $(document).ready(function () {
          user.index();
      });
  </script>
@endsection