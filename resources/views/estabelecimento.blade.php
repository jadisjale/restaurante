@extends('template')

@section('title', 'ESTABELECIMENTO')

@section('content')

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>CADASTRE AQUI OS DADOS DA SUA EMPRESA</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form class="form-horizontal" id="save_establishment">
{{--                        {{ var_dump(session()->get('name') ) }}--}}
{{--                        {{ var_dump(session()->all() ) }}--}}
                        <input type="hidden" name="id" value="{{ isset($est[0] ) ? $est[0]->id : '' }}"/>
                        <label for="update-value"> NOME: </label>
                        <input type="text" id="update-name" name="establishment" class="form-control mousetrap" value="{{ isset($est[0] ) ? $est[0]->name : ''}}" placeholder="ATUALIZAR NOME DO ESTABELECIMENTO"/>
                        <br>
                        <label for="update-value"> ESTADO: </label>
                        <input type="text" id="update-state" name="state" class="form-control mousetrap" value="{{ isset($est[0] ) ? $est[0]->state : '' }}" placeholder="ATUALIZAR ESTADO DO ESTABELECIMENTO"/>
                        <br>
                        <label for="update-value"> CIDADE: </label>
                        <input type="text" id="update-city" name="city" class="form-control mousetrap" value="{{ isset($est[0] ) ? $est[0]->city : '' }}" placeholder="ATUALIZAR CIDADE DO ESTABELECIMENTO"/>
                        <br>
                        <label for="update-value"> RUA: </label>
                        <input type="text" id="update-street" name="street" class="form-control mousetrap" value="{{isset($est[0] ) ? $est[0]->street : ''}}" placeholder="ATUALIZAR RUA DO ESTABELECIMENTO"/>
                        <br>
                        <label for="update-value"> NÚMERO: </label>
                        <input type="text" id="update-number" name="number" class="form-control mousetrap" value="{{ isset($est[0] ) ? $est[0]->number : '' }}"  placeholder="ATUALIZAR NÚMERO DO ESTABELECIMENTO"/>
                        <br>
                        <label for="update-value"> TELEFONE: </label>
                        <input type="text" id="update-phone" name="phone" class="form-control mousetrap" value="{{ isset($est[0] ) ? $est[0]->phone : '' }}" placeholder="ATUALIZAR TELEFONE DO ESTABELECIMENTO"/>
                        <br>
                        <button class="btn btn-primary" type="submit" id="btn-submit">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> CADASTRAR
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            establishment.index();
        });
    </script>
@endsection