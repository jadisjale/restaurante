<!DOCTYPE html>
<html lang="pt-BR">

<style>
    label {
        font-size: medium;
    }

    table, th, td {
        /*border: 1px solid black;*/
        /*border-collapse: collapse;*/
    }
    th, td {
        padding: 5px;
    }
    th {
        text-align: left;
    }

    table td
    {
        table-layout:fixed;
        width:20px;
        overflow:hidden;
        word-wrap:break-word;
    }

    table {border: none; !important;}

</style>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ session()->get('name')}}</title>

</head>

<br style="width: 276px; margin-left: -1px;">
    <h5 style="text-align: center;">{{ session()->get('name')}}</h5>
    <div style="text-align: center; margin-top: -36px;">____________________________</div>
    <br>
    <label style="float: left">DATA: {{ $data }}</label> <label style="float: right">HORA: {{ $hora }}</label><br>

    <label style="float: left">MESA(S):&nbsp</label>
    @for ($i = 0; $i < count($mesas); $i++)
        @if(($i+1) === count($mesas))
            <label style="float: left">{{ $mesas[$i]->description }}.</label>
        @elseif($i === count($mesas))
            <label style="float: left">{{ $mesas[$i]->description }} </label>
        @else
            <label style="float: left">{{ $mesas[$i]->description }}, &nbsp</label>
        @endif
    @endfor
    <br>
    <table cellspacing="0" cellpadding="0" style="width:100%">
        <tr>
            <th width="100px"><label>PRODUTO</label></th>
            <th><label>QTDE</label></th>
            <th><label>VLR UNIT.</label></th>
            <th><label>SUB. TOTAL</label></th>
        </tr>
        @foreach ($comanda as $c)
            <tr>
                <td><label>{{ $c['description'] }}</label></td>
                <td><label>{{ $c['qtd'] }}</label></td  >
                <td><label>{{ number_format($c['value_item'] , 2, ',', '.') }}</label></td>
                <td><label style="float: right" >{{ number_format(($c['value_item']*(int)$c['qtd']) , 2, ',', '.') }}</label></td>
            </tr>
        @endforeach
    </table>
    <div class="total">
        <br>
        <b style="float:left;">TOTAL GERAL:</b> <b style="float:right;">{{ number_format($c['value_all'] , 2, ',', '.') }}</b><br>
        <label style="float: left">TOTAL DE ITENS: {{ count($comanda) }}</label>
    </div>
    <br>
    <br>
    <div class="saida" style="text-align: center">
        <label> *** VOLTE SEMPRE ***  </label>
        <br>
        <label> *** SEM VALOR FISCAL ***  </label>
    </div>

    <div class="saida-versao" style="text-align: center;">
        <br>
        <label>versao: 1.0 desenvolvido por wesoluctions</label>
    </div>
</body>

<script>
    window.print();
</script>

</html>