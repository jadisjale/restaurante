@extends('template')

@section('title', 'VENDA')

@section('content')

  <style>
    #finish {
      background-color: #d9534f;
      color: #FFFFFF;
    }

    .panel_toolbox>li>a {
      padding: 5px;
      color: #C5C7CB;
      font-size: 14px;
      /* margin-right: -44px; */
      margin-left: 4px;
    }

    .panel_toolbox>li>a {
      padding: 5px;
      color: #C5C7CB;
      font-size: 14px;
      /* margin-right: -44px; */
      margin-left: 47px;
    }

  </style>

  <form class="form-horizontal form-label-left input_mask" id="form-sell-box" method="post">
    <input class="form-control" name="boards[]" value="1" type="hidden">
    <input class="form-control" name="id_waiter" value="1" type="hidden">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12" id="panel-iten">
        <div class="x_panel">
          <div class="x_title">
            <h2>ADICIONAR ITEM A VENDA</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <label for="id_item_active">INFORME O PRODUTO</label>
            <select class="form-control mousetrap" id="id_item_active" name="id_item_active" required></select>
            <br>
            <br>
            <label for="count_item">QUANTIDADE DO PRODUTO: </label>
            <input class="form-control mousetrap" id="count_item" name="count_item" type="number" value="1" min="1" required/>
            <br>
            <input type="hidden" id="id_sell" name="id_sell">
            <button type="submit" class="btn btn-primary" id="btn-adicionar"> <i class="fa fa-cart-plus" aria-hidden="true"></i> ADICIONAR AO PEDIDO</button>
          </div>
        </div>
      </div>
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
              <h2 class="sell_id">PEDIDO: <b id="sell_id" style="color: #d9534f;">0</b> | VALOR TOTAL DO PEDIDO: <b id="value_total" style="color: #d9534f">R$ 0,00 </b></h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                <li><a id="finish"> <i class="fa fa-check" aria-hidden="true"></i> FINALIZAR PEDIDO</a>
              </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <table id="list_item_sell" class="table table-striped table-bordered" width="100%">
              <thead>
              <tr>
                <th>PRODUTO</th>
                <th width="150px" height="1px">R$ UNIT</th>
                <th width="40px" height="1px">QTD</th>
                <th width="150px" height="10px">R$ FINAL</th>
                <th width="10px">AÇÃO</th>
                <th>Sell_id</th>
              </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </form>

  <div class="modal fade" tabindex="-1" role="dialog" id="modal-sell-client">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">ATRIBUIR CLIENTE A ESTA VENDA</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <select class="form-control select2 mousetrap" id="client_id" name="client_id" style="width: 100%"></select>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
          <button type="button" class="btn btn-primary" id="sell_client">ATRIBUIR CLIENTE A ESTE PEDIDO</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" tabindex="-1" role="dialog" id="modal-save-client">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">SALVAR UM NOVO CLIENTE</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <input type="text" class="form-control" id="name_client" name="name_client" placeholder="INFORME O NOME DE UM NOVO CLIENTE" style="width: 100%" />
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
          <button type="button" class="btn btn-primary" id="save_client">SALVAR UM CLIENTE</button>
        </div>
      </div>
    </div>
  </div>

  <script src="vendors/jquery/dist/jquery.min.js"></script>
  <script>
      $(document).ready(function () {
          sell.index();
      });
  </script>

@endsection