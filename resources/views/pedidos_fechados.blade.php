@extends('template')

@section('title', 'PEDIDOS FECHADOS')

@section('content')

    <style>
        .center {
            padding-left: 10px;
        }
    </style>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>INFORME A DATA DE INÍCIO E DE FIM PARA VER OS PEDIDOS FECHADOS</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br/>
                    <form class="form-horizontal" id="form-sell-inactive">
                        <fieldset>
                            <div class="control-group">
                                <div class="controls">
                                    <label for="date">INFORME A DATA DE INÍCIO E DE FIM:</label>
                                    <div class="input-prepend input-group">
                                        <span class="add-on input-group-addon"><i
                                                    class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                        <input type="text" style="width: 200px" name="date" id="date"
                                               class="form-control mousetrap" value="">
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <button class="btn btn-primary" type="submit" id="btn-consultar">
                            <i class="fa fa-search" aria-hidden="true"></i> CONSULTAR
                        </button>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>PEDIDOS FINALIZADOS</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                        LISTA DE TODOS OS PEDIDOS FINALIZADOS.
                    </p>

                    <table id="table_sell_inactive" class="table table-striped table-bordered dt-responsive nowrap"
                           cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            {{--<th>PEDIDO</th>--}}
                            <th>MESAS</th>
                            <th>DATA / HORA</th>
                            <th>VALOR TOTAL</th>
                            <th width="10px">VISUALIZAR</th>
                            <th width="10px">COMANDA</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            sell_inactive.index();
            sell_inactive.printComanda();
        });
    </script>
@endsection