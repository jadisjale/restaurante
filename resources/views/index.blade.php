@extends('template')

@section('title', 'INÍCIO')

@section('content')

  <div class="row tile_count">
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> TOTAL DE GARÇONS</span>
      <div class="count" id="waiter"><i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
        <span class="sr-only">LOADING...</span></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> TOTAL DE CLIENTES</span>
      <div class="count" id="client"><i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
        <span class="sr-only">LOADING...</span></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> TOTAL DE USUÁRIOS</span>
      <div class="count" id="user"><i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
        <span class="sr-only">LOADING...</span></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-window-maximize" aria-hidden="true"></i> TOTAL DE MESAS</span>
      <div class="count" id="board"><i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
        <span class="sr-only">LOADING...</span></div>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-barcode" aria-hidden="true"></i> TOTAL DE PRODUTOS</span>
      <div class="count" id="item"><i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
        <span class="sr-only">LOADING...</span></div>
    </div>
    {{--<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">--}}
      {{--<span class="count_top"><i class="fa fa-shopping-cart" aria-hidden="true"></i> TOTAL DE PEDIDOS</span>--}}
      {{--<div class="count" id="sell"><i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i>--}}
        {{--<span class="sr-only">LOADING...</span></div>--}}
    {{--</div>--}}
  {{--</div>--}}
  {{--<div class="row tile_count">--}}
    {{--<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">--}}
      {{--<span class="count_top"><i class="fa fa-money" aria-hidden="true"></i> TOTAL DE DINHEIRO</span>--}}
      {{--<div class="count green" id="dinheiro">00,00</div>--}}
    {{--</div>--}}
  </div>

  <script src="vendors/jquery/dist/jquery.min.js"></script>
  <script>
      $(document).ready(function () {
          index.index();
      });
  </script>

@endsection