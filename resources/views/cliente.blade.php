@extends('template')

@section('title', 'CLIENTE')

@section('content')

  <div class="row">
    <div class="col-md-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>CADASTRO DE CLIENTES</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          <form class="form-horizontal form-label-left input_mask" id="save_cliente" method="post">
            <label for="name">NOME DO CLIENTE:</label>
            <input class="form-control mousetrap" id="name" name="name" type="text" placeholder="EX: ANTÔNIO DA SILVA" title="INFORME O NOME DO CLIENTE"/>
            <br />
            <button type="submit" class="btn btn-primary" id="btn-submit"> <i class="fa fa-floppy-o" aria-hidden="true"></i> SALVAR CLIENTE</button>
          </form>
        </div>
        </div>
      </div>
    </div>

  <div class="row">
    <div class="col-md-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>LISTA DE CLIENTES</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table id="list_clients" class="table table-striped table-bordered" width="100%">
            <thead>
            <tr>
              <th>CLIENTE</th>
            </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>

  <script src="vendors/jquery/dist/jquery.min.js"></script>
  <script>
      $(document).ready(function () {
          client.index();
      });
  </script>
@endsection