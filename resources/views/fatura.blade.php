@extends('template')

@section('title', 'FATURA')

@section('content')

  <div class="row">
    <div class="col-md-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>INFORME A DATA DE INÍCIO E DE FIM PARA VER AS FATURAS</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          <form class="form-horizontal" id="form-fatura">
            <fieldset>
              <div class="control-group">
                <div class="controls">
                  <label for="date">INFORME A DATA DE INÍCIO E DE FIM:</label>
                  <div class="input-prepend input-group">
                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                    <input type="text" style="width: 200px" name="date" id="date" class="form-control mousetrap" value="">
                  </div>
                </div>
              </div>
            </fieldset>
              <button class="btn btn-primary" type="submit">
                  <i class="fa fa-search" aria-hidden="true"></i> CONSULTAR</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12" id="x-fatura">
          <div class="x_panel">
              <div class="x_title">
                  <h2>RESULATADO | VALOR TOTAL DESSE PERÍODO: <b id="value_all" style="color: #d9534f"> R$ 0,00 </b></h2>
                  <ul class="nav navbar-right panel_toolbox">

                  </ul>
                  <div class="clearfix"></div>
              </div>
              <div class="x_content">
                  <table id="table_fatura" class="table table-striped table-bordered" width="100%">
                      <thead>
                      <tr>
                          <th>PEDIDO</th>
                          <th>VALOR DO PEDIDO</th>
                          <th>DATA DO PEDIDO</th>
                          <th width="10px">AÇÃO</th>
                      </tr>
                      </thead>
                  </table>
              </div>
          </div>
      </div>
  </div>

  <script src="vendors/jquery/dist/jquery.min.js"></script>
  <script>
      $(document).ready(function () {
          fatura.fatura.index();
      });
  </script>

@endsection