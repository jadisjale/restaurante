@extends('template')

@section('title', 'VENDAS PENDENTES')

@section('content')

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel" id="panel-boards">
        <div class="x_title">
          <h2>VEJA OS CLIENTES QUE ESTÃ0 DEVENDO</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          <label for="boards">SELECIONE UM CLIENTE</label>
          <select class="form-control select2 mousetrap" id="client_id"></select>
        </div>
      </div>
    </div>
  </div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel" id="panel-boards">
          <div class="x_title">
            <h2>VALOR DA DÍVIDA: <span style="color: #d9534f" id="value_dept"> R$ 00,00</span></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <form class="form-horizontal form-label-left input_mask" id="add_dept" method="post">
              <label>ESCOLHA UMA OPÇÃO:</label>
              <p>
                ADICIONAR:
                <input type="radio" class="flat mousetrap" name="gender" id="genderM" value="add" checked="" required /> PAGAR:
                <input type="radio" class="flat mousetrap" name="gender" id="genderF" value="remove" />
              </p>
              {{--<label for="value">Informe o valor atual da dívida</label>--}}
              <input class="form-control money mousetrap" id="valor" name="valor" type="text" placeholder="INFORME O VALOR ATUAL DA DÍVIDA DO CLIENTE SELECIONADO" value=""/>
              <br>
              <button type="submit" class="btn btn-primary" id="btn-submit">
                <i class="fa fa-refresh" aria-hidden="true"></i> ATUALIZAR VALOR DA DÍVIDA</button>
              <button type="button" class="btn btn-danger" id="remove-debt">
                <i class="fa fa-check" aria-hidden="true"></i> QUITAR DÍVIDA</button>
            </form>
          </div>
        </div>
      </div>
    </div>

  <script src="vendors/jquery/dist/jquery.min.js"></script>
  <script>
      $(document).ready(function () {
          sell_client.index();
      });
  </script>

@endsection