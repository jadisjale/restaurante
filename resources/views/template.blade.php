<!DOCTYPE html>
<html lang="pt-BR">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>{{ session()->get('name')}} - @yield('title')</title>

  <!-- Bootstrap -->
  <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
  <link href="vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
  <link href="vendors/inputFile/css/fileinput.min.css" rel="stylesheet">
  <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="vendors/select2/dist/css/select2.min.css" rel="stylesheet">
  <link href="/css/dataTables.bootstrap4.min.css" rel="stylesheet">
  <link href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
  <link href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
  <link href="vendors/iCheck/skins/flat/green.css" rel="stylesheet">

  <link href="/css/buttons.dataTables.min.css" rel="stylesheet">

  <link href="vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

  <link href="build/css/custom.min.css" rel="stylesheet">
  <link href="build/css/custom/tools.css" rel="stylesheet">

  <link href="css/toastr.min.css" rel="stylesheet">
</head>

<style>
  .badge {
    background-color: #1abb9c;;
  }

  .x_panel {
    background-color: #2a3f54;
  }

  h1 {
    color: #ffffff;
  }

  h2 {
    color: #ffffff;
  }

  h3 {
    color: #ffffff;
  }

  h4 {
    color: #ffffff;
  }

  h5 {
    color: #ffffff;
  }

  h6 {
    color: #ffffff;
  }

  label {
    color: #ffffff;
    font-size: large;
  }

  p {
    font-size: large;
    color: #ffffff !important;
  }

  /*body {*/
    /*color: black;*/
  /*}*/

  td {
    color: black;
  }

  .dataTable tr {
    font-size: large !important;
    color: #ffffff !important;
  }

  .table-condensed tr {
    font-size: large !important;
  }

  .toast-message {
    font-size: large;
    color: #ffffff !important;
  }

  .error {
    color: #1ABB9C !important;
  }

  .even {
    background: #1abb9c !important;
  }


  ul li .active a {
    color: black;
  }

  ul li a {
    color: #1abb9c;
  }

  select option.selected {
    font-weight: bold;
    color: red;
  }

  h2 small {
    color: #1abb9c;
  }

  .modal-body {
    font-size: large;
    background-color: #15202a;
  }

  .modal-header {
    background-color: #15202a;
  }

  .dropdown {
    display: none;
  }

  .panel_toolbox>li>a {
    padding: 5px;
    color: #C5C7CB;
    font-size: 14px;
    /* margin-right: -44px; */
    margin-left: 47px;
  }

  .nav-md ul.nav.child_menu li:before {
    background: #1ABB9C !important;
  }

  .icon-person {
    color: #1ABB9C !important;
  }

  .way {
    margin-bottom: 5px;
    height: 56px;
  }

  #hide-way {
    cursor:pointer;
  }

  .dtr-title {
    color: #ffffff;
  }

  .site_title {
    font-weight: 400;
    font-size: 22px;
    width: 100%;
    line-height: 34px;
    display: inline-table;
    height: 55px;
    margin: 0;
    padding-left: 10px;
  }

  .select2-container--default .select2-search--dropdown .select2-search__field
  select, textarea,
  input[type="text"], input[type="password"],
  input[type="datetime"], input[type="datetime-local"],
  input[type="date"], input[type="month"], input[type="time"],
  input[type="week"], input[type="number"], input[type="email"],
  input[type="url"], input[type="search"], input[type="tel"], input[type="color"],
  .uneditable-input {
    text-transform: uppercase !important;

  }

</style>

<body class="nav-md">
<div class="container body">
  <div class="main_container">
    <div class="col-md-3 left_col">
      <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
          <a href="/" class="site_title"><span>{{ session('name') }}</span></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
          <div class="profile_pic">
            <!--<img src="images/img.jpg" alt="..." class="img-circle profile_img">-->
          </div>
          <div class="profile_info">
            <span>BEM VINDO(A),</span>
            <h2>{{ Auth::user()->name }}</h2>
          </div>
        </div>
        <!-- /menu profile quick info -->

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
          <div class="menu_section">
            <h3></h3>
            <ul class="nav side-menu">
              <li><a><i class="fa fa-home icon-person"></i><b>CADASTRO <span class="badge">F1</span> </b><span class="fa fa-chevron-down icon-person"></span></a>
                <ul class="nav child_menu">
                  <li><a href="mesas">MESAS <span class="badge">Ctrl + M</span></a></li>
                  <li><a href="produto">PRODUTOS <span class="badge">Ctrl + P</span></a></li>
                  <li><a href="servicos">SERVIÇOS <span class="badge">Ctrl + S</span></a></li>
                </ul>
              </li>
            </ul>
            <ul class="nav side-menu">
              <li><a><i class="fa fa-address-card icon-person" aria-hidden="true"></i><b>GESTÃO <span class="badge">F2</span></b><span class="fa fa-chevron-down icon-person"></span></a>
                <ul class="nav child_menu">
                  <li><a href="estabelecimento">ESTABELECIMENTO <span class="badge">Ctrl + E</span></a></li>
                  <li><a href="garcom">GARÇONS <span class="badge">Ctrl + G</span></a></li>
                  {{--<li><a href="usuario">Usuário</a></li>--}}
                  <li><a href="cliente">CLIENTES <span class="badge">Ctrl + C</span></a></li>
                </ul>
              </li>
            </ul>
            <ul class="nav side-menu">
              <li><a><i class="fa fa-cart-plus icon-person" aria-hidden="true"></i><b>PEDIDOS <span class="badge">F3</span></b><span class="fa fa-chevron-down icon-person"></span></a>
                <ul class="nav child_menu">
                  <li><a href="venda_caixa">VENDA CAIXA <span class="badge">Ctrl+V</span></a></li>
                  <li><a href="pedidos">CADASTRAR PEDIDOS <span class="badge">Ctrl+H</span></a></li>
                  <li><a href="pedidos_abertos">PEDIDOS EM ABERTO <span class="badge">Ctrl+A</span> <span class="badge" id="count_sell_active">0</span> </a></li>
                  <li><a href="pedidos_fechados">PEDIDOS ENCERRADOS <span class="badge">Ctrl+R</span></a></li>
                </ul>
              </li>
            </ul>
            <ul class="nav side-menu">
              <li><a><i class="fa fa-file-text-o icon-person" aria-hidden="true"></i><b>RELATÓRIOS <span class="badge">F4</span></b><span class="fa fa-chevron-down icon-person"></span></a>
                <ul class="nav child_menu">
                  <li><a href="fatura">FATURA POR DATA <span class="badge">Ctrl+D</span></a></li>
                  <li><a href="fatura_mesa">FATURA POR MESA <span class="badge">Ctrl+F</span></a></li>
                  <li><a href="balanco">BALANÇO <span class="badge">Ctrl+B</span></a></li>
                </ul>
              </li>
            </ul>

            <ul class="nav side-menu">
              <li><a><i class="fa fa-windows icon-person"></i><b>EXTRAS <span class="badge">F6</span></b><span class="fa fa-chevron-down icon-person"></span></a>
                <ul class="nav child_menu">
                  <li><a href="clientes_pendente_pagamento">CLIENTE PENDENTE PAGAMENTO <span class="badge">Ctrl+K</span></a></li>
                </ul>
              </li>
            </ul>

          </div>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>

          <div class="menu_section">

          </div>

        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
          <!-- /menu footer buttons -->
        </div>
      </div>
    </div>

    <!-- top navigation -->
    <div class="top_nav">
      <div class="nav_menu">
        <nav>
          <div class="nav toggle">
            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
          </div>
          <ul class="nav navbar-nav navbar-right">
            <li class="">
              <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                {{ Auth::user()->name }}
                <span class=" fa fa-angle-down"></span>
              </a>
              <ul class="dropdown-menu dropdown-usermenu pull-right">
                <li><a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out pull-right icon icon-person"></i> SAIR
                  </a></li>
              </ul>
            </li>
            <li role="presentation" class="dropdown"></li>
          </ul>
        </nav>
      </div>
    </div>
    <!-- /top navigation -->

    <!-- page content -->
    <div class="right_col" role="main" id="conteudo">
      {{--<div class="way text-center" style="background-color: #2a3f54">--}}
          {{--<label id="way" class="icon-person" style="color: #2a3f54; font-size: smaller;"> </label>--}}
          {{--<i id="hide-way" class="fa fa-times fa-1x icon-person" aria-hidden="true"></i>--}}
      {{--</div>--}}
      @yield('content')
    </div>

    <!-- /page content -->

    <!-- footer content -->
    <footer>
      <div class="pull-right">
        WeSoluctions 2017
      </div>
      <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
  </div>
</div>

<script src="vendors/jquery/dist/jquery.min.js"></script>
<script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="build/js/plugin/jquery.mask.min.js"></script>
<script src="vendors/inputFile/js/fileinput.js"></script>
<script src="vendors/inputFile/js/locales/pt-BR.js"></script>
<script src="vendors/validation/jquery.validate.min.js"></script>
<script src="vendors/validation/validationTranslate.js"></script>
<script src="js/jquery.maskMoney.min.js"></script>
<script src="build/js/custom.min.js"></script>
<script src="vendors/select2/dist/js/select2.min.js"></script>
<script src="vendors/select2/dist/js/i18n/pt-BR.js"></script>

<script src="js/translate_validation.js"></script>
<script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.min.js"></script>

<script src="vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>

<script src="js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>

<script src="vendors/moment/min/moment.min.js"></script>
<script src="vendors/moment/locale/pt-br.js"></script>
<script src="vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="vendors/iCheck/icheck.min.js"></script>


<script src="js/jquery.confirm.min.js"></script>
<script src="js/toastr.min.js"></script>
<script src="js/tool.js"></script>
<script src="js/url.js"></script>
<script src="js/board.js"></script>
<script src="js/waiter.js"></script>
<script src="js/item.js"></script>
<script src="js/user.js"></script>
<script src="js/sell.js"></script>
<script src="js/sell_active.js"></script>
<script src="js/sell_inactive.js"></script>
<script src="js/fatura.js"></script>
<script src="js/client.js"></script>
<script src="js/sell_client.js"></script>
<script src="js/index.js"></script>
<script src="js/way.js"></script>
<script src="js/establishment.js"></script>
<script src="js/mousetrap.js"></script>
<script src="js/shortcut.js"></script>
<script src="js/service.js"></script>

</body>

<script>
    $(document).ready(function () {
        $(".select2").select2();
        tool.money();
        sell.loadItensActive();
        sell.countSellActive();
        way.index();
        template.template();
    });

    $('input, textarea').keyup(function () {
        var str = $(this).val();
        $(this).val(str.toUpperCase());
    });
</script>
</html>

