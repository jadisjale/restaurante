@extends('template')

@section('title', 'PEDIDOS')

@section('content')

  <style>
    #finish {
      background-color: #d9534f;
      color: #FFFFFF;
    }

    .panel_toolbox>li>a {
      padding: 5px;
      color: #C5C7CB;
      font-size: 14px;
      /* margin-right: -44px; */
      margin-left: 4px;
    }

    .panel_toolbox>li>a {
      padding: 5px;
      color: #C5C7CB;
      font-size: 14px;
      /* margin-right: -44px; */
      margin-left: 47px;
    }

  </style>

  <form class="form-horizontal form-label-left input_mask" id="form-item-pedido" method="post">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel" id="panel-boards">
          <div class="x_title">
            <h2>MESAS PARA ESSE PEDIDO</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <label for="boards">SELECIONE AS MESAS DISPONÍVEIS PARA ESSE PEDIDO</label>
            <select class="form-control select2 mousetrap" multiple="multiple" id="boards" name="boards[]" required></select>
            <br>
            <br>
            <button type="button" class="btn btn-danger" id="update_board" style="display: none"> <i class="fa fa-refresh" aria-hidden="true"></i> ATUALIZAR MESAS</button>
          </div>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xs-12" id="panel-iten">
        <div class="x_panel">
          <div class="x_title">
            <h2>ADICIONAR ITEM A ESSE PEDIDO</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />

            <label for="id_item_active">INFORME O PRODUTO PARA ESSE PEDIDO</label>
            <select class="form-control mousetrap" id="id_item_active" name="id_item_active" required>

            </select>
            <br>
            <br>
            <label for="count_item">QUANTIDADE DO PRODUTO: </label>
            <input class="form-control mousetrap" id="count_item" name="count_item" type="number" value="1" min="1" required/>
            <br>
            <label for="id_waiter">GARÇOM QUE VENDEU ESSE PRODUTO</label>
            <select class="form-control select2 mousetrap" id="id_waiter" name="id_waiter" required>

            </select>
            <br>
            <br>
            @if ($sell_id)
              <input type="hidden" id="id_sell" name="id_sell" value="{{$sell_id}}">
            @else
            <input type="hidden" id="id_sell" name="id_sell">
            @endif
            <button type="submit" class="btn btn-primary" id="btn-adicionar"> <i class="fa fa-cart-plus" aria-hidden="true"></i> ADICIONAR AO PEDIDO</button>
            {{--<button type="button" class="btn btn-primary hide-to-modal" id="btn-adicionar-sell-client" style="display: none">Adicionar Cliente ao pedido</button>--}}
            {{--<button type="button" class="btn btn-primary hide-to-modal" id="btn-adicionar-client" style="display: none">Cadastrar um novo cliente</button>--}}
          </div>
        </div>
      </div>


      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            @if ($sell_id)
              <h2 class="sell_id">PEDIDO: <b id="sell_id" style="color: #d9534f;">{{$sell_id}}</b> | VALOR TOTAL DO PEDIDO: <b id="value_total" style="color: #d9534f">{{$value_total}}</b></h2>
            @else
              <h2 class="sell_id">PEDIDO: <b id="sell_id" style="color: #d9534f;">0</b> | VALOR TOTAL DO PEDIDO: <b id="value_total" style="color: #d9534f">R$ 0,00 </b></h2>
            @endif
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                <li><a id="finish"> <i class="fa fa-check" aria-hidden="true"></i> FINALIZAR PEDIDO</a>
              </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <table id="list_item_sell" class="table table-striped table-bordered" width="100%">
              <thead>
              <tr>
                <th>PRODUTO</th>
                <th>VALOR UNITÁRIO</th>
                <th>QTD</th>
                <th>SUB-TOTAL</th>
                <th width="10px">AÇÃO</th>
                <th>Sell_id</th>
              </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </form>

  <div class="modal fade" tabindex="-1" role="dialog" id="modal-sell-client">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">ATRIBUIR CLIENTE A ESTA VENDA</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <select class="form-control select2 mousetrap" id="client_id" name="client_id" style="width: 100%"></select>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
          <button type="button" class="btn btn-primary" id="sell_client">ATRIBUIR CLIENTE A ESTE PEDIDO</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" tabindex="-1" role="dialog" id="modal-save-client">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">SALVAR UM NOVO CLIENTE</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <input type="text" class="form-control mousetrap" id="name_client" name="name_client" placeholder="Infome o nome de um novo cliente" style="width: 100%" />
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
          <button type="button" class="btn btn-primary" id="save_client">SALVAR UM CLIENTE</button>
        </div>
      </div>
    </div>
  </div>

  <script src="vendors/jquery/dist/jquery.min.js"></script>
  <script>
      $(document).ready(function () {
          sell.index();
      });
  </script>

@endsection