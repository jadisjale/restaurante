@extends('template')

@section('title', 'PRODUTO')

@section('content')

  <style>
    .x_panel {
      /*padding-bottom: 1px;*/
    }
  </style>

  <div class="row">
    <div class="col-md-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>CADASTRO DE PRODUTOS</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          <form class="form-horizontal form-label-left input_mask" id="save_item" method="post">
            <input type="hidden" value="1" name="status" id="status"/>
            <label for="description">NOME DO PRODUTO:</label>
            <input class="form-control mousetrap" id="description" name="description" type="text" placeholder="EX: CERVEJA 600 ML"/>
            <br>
            <label for="value">VALOR DO PRODUTO:</label>
            <input class="form-control money mousetrap" id="value" name="value" type="text" placeholder="R$ 6,50"/>
            <br>
            <label for="obs">DESCRIÇÃO DO PRODUTO:</label>
            <textarea class="form-control mousetrap" rows="4" cols="50" id="obs" name="obs" placeholder="DETALHES SOBRE O PRODUTO"></textarea>
            <br>
            <button type="submit" class="btn btn-primary" id="btn-submit"> <i class="fa fa-floppy-o" aria-hidden="true"></i> SALVAR PRODUTO</button>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>LISTA DE PRODUTOS</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <ul class="nav nav-tabs nav-justified" role="tablist" data-original-title="" title="">
            <li id="li-active" role="presentation" class="active" data-original-title="" title=""><a href="#disponivel" aria-controls="home" role="tab" data-toggle="tab" data-original-title="" title="">DISPONÍVEL</a></li>
            <li id="li-disabled" role="presentation" data-original-title="" title=""><a href="#indisponivel" aria-controls="profile" role="tab" data-toggle="tab" data-original-title="" title="">INDISPONÍVEL</a></li>
          </ul>
          <br>
          <div class="scroll-panel" data-original-title="" title=""></div>
          <div class="tab-content" data-original-title="" title="">
            <div role="tabpanel" class="tab-pane active" id="disponivel" data-original-title="" title="">
              <table id="iten-active" class="table table-striped table-bordered" width="100%">
                <thead>
                <tr>
                  <th>NOME</th>
                  <th>VALOR</th>
                  <th width="10px">ALTERAR</th>
                  <th width="10px">DESATIVAR</th>
                </tr>
                </thead>
              </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="indisponivel" data-original-title="" title="">
              <table id="iten-disabled" class="table table-striped table-bordered" width="100%">
                <thead>
                <tr>
                  <th>NOME</th>
                  <th>VALOR</th>
                  <th width="10px">ATIVAR</th>
                </tr>
                </thead>
              </table>
            </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" tabindex="-1" role="dialog" id="detalhar-produto">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">DETALHES DO PRODUTO</h4>
        </div>
        <div class="modal-body">
          <ul class="list-group">
            <li class="list-group-item"><b>DESCRIÇÃO: </b> <span id="ul-descrption"> </span></li>
            <li class="list-group-item"><b>OBSERVAÇÃO: </b> <span id="ul-obs"> </span></li>
            <li class="list-group-item"><b>PREÇO ATUAL: </b> <span id="ul-value"> </span></li>
            <li class="list-group-item"><b>DATA DE CRIAÇÃO: </b> <span id="ul-create"> </span></li>
          </ul>
          <ul class="list-group">
            <label for="update-value"> ATUALIZAR NOME DO PRODUTO: </label>
            <input type="text" id="update-name" class="form-control mousetrap" placeholder="ATUALIZAR NOME DO PRODUTO"/>
            <label for="update-value"> ATUALIZAR VALOR PARA: </label>
            <input type="text" id="update-value" class="form-control money mousetrap"/>
          </ul>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">FECHAR</button>
          <button type="button" class="btn btn-primary" id="btn-update-value">SALVAR ALTERAÇÕES</button>
        </div>
      </div>
    </div>
  </div>
  </div>

  <script src="vendors/jquery/dist/jquery.min.js"></script>
  <script>
    $(document).ready(function () {
        item.index();
    });
  </script>
@endsection