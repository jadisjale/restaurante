<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'after_or_equal'       => 'The :attribute must be a date after or equal to :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'before_or_equal'      => 'The :attribute must be a date before or equal to :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'dimensions'           => 'The :attribute has invalid image dimensions.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => 'The :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'file'                 => 'The :attribute must be a file.',
    'filled'               => 'The :attribute field is required.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'mimetypes'            => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'The :attribute must be a number.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'The :attribute field is required.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'The :attribute has already been taken.',
    'uploaded'             => 'The :attribute failed to upload.',
    'url'                  => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => array(
        'name' => array(
            'required' => 'O CAMPO NOME É OBRIGATÓRIO.',
            'min' => 'O NOME DEVE CONTER PELO MENOS :min CARACTERES.',
            'max' => 'O NOME DEVE CONTER NO MÁXIMO :max CARACTERES.'
        ),
        'password' => array(
            'required' => 'O CAMPO SENHA É OBRIGATÓRIO.',
            'min' => 'A SENHA DEVE CONTER PELO MENOS :min CARACTERES.',
            'max' => 'A SENHA DEVE CONTER NO MÁXIMO :max CARACTERES.',
            'confirmed' => 'AS DUAS SENHAS DIGITADAS NÃO CONFEREM.'
        ),
        'value' => array (
            'required' => 'O CAMPO VALOR É OBRIGATÓRIO.',
            'numeric' => 'O CAMPO VALOR NÃO É REAL.'
        ),
        'sell_id' => array (
            'required' => 'O ID DA VENDA É OBRIGATÓRIO.',
            'numeric' => 'O ID DA VENDA NÃO É REAL.'
        ),
        'item_id' => array (
            'required' => 'O ID DO PRODUTO É OBRIGATÓRIO.',
            'numeric' => 'O ID DO PRODUTO NÃO É REAL.'
        ),
        'description' => array(
            'required' => 'O CAMPO DESCRIÇÃO É OBRIGATÓRIO.',
            'min' => 'O CAMPO DESCRIÇÃO DEVE CONTER PELO MENOS :min CARACTERES.',
            'max' => 'O CAMPO  DESCRIÇÃO DEVE CONTER NO MÁXIMO :max CARACTERES.'
        ),
        'status' => array(
            'required' => 'O CAMPO STATUS É OBRIGATÓRIO.',
        ),
        'value_all' => array(
            'required' => 'O CAMPO VALOR TOTAL DA VENDA É OBRIGATÓRIO.',
            'numeric' => 'O CAMPO VALOR TOTAL DA VENDA NÃO É REAL',
        ),
        'waiter_id' => array(
            'required' => 'O CAMPO GARÇOM É OBRIGATÓRIO.',
            'numeric' => 'O CAMPO GARÇOM NÃO É REAL.',
        ),
        'group_id' => array(
            'required' => 'O CAMPO GRUPO É OBRIGATÓRIO.',
            'numeric' => 'O CAMPO GRUPO NÃO É REAL.',
        ),
        'board_id' => array(
            'required' => 'O CAMPO MESA É OBRIGATÓRIO.',
            'numeric' => 'O CAMPO MESA NÃO É REAL.',
        ),
        'value_item' => array(
            'required' => 'O CAMPO MESA É OBRIGATÓRIO.',
            'numeric' => 'O CAMPO MESA NÃO É REAL.',
        ),
        'email' => array(
            'unique' => 'ESTE LOGIN JÁ ESTÁ SENDO USADO.',
        ),
        'pay' => array(
            'required' => 'O CAMPO PAGAMENTO É OBRIGATÓRIO.',
            'numeric' => 'O CAMPO PAGAMENTO NÃO É REAL.',
        ),
        'added_on' => array(
            'required' => 'O CAMPO DATA PAGAMENTO É OBRIGATÓRIO.',
            'date' => 'O CAMPO PAGAMENTO NÃO É DATA VÁLIDA.',
        ),
        'value_dept' => array(
            'required' => 'O CAMPO DÉBITO É OBRIGATÓRIO.',
            'date' => 'O CAMPO DÉBITO NÃO É DATA VÁLIDA.',
        ),
        'qtd' => array(
            'required' => 'O CAMPO QUANTIDADE É OBRIGATÓRIO.',
            'numeric' => 'O CAMPO QUANTIDADE NÃO É REAL.',
        ),
        'state' => array(
            'required' => 'O CAMPO ESTADO É OBRIGATÓRIO.',
            'min' => 'O CAMPO ESTADO DEVE CONTER PELO MENOS :min CARACTERES.',
        ),
        'city' => array(
            'required' => 'O CAMPO CIDADE É OBRIGATÓRIO.',
            'min' => 'O CAMPO CIDADE DEVE CONTER PELO MENOS :min CARACTERES.',
        ),
        'street' => array(
            'required' => 'O CAMPO RUA É OBRIGATÓRIO.',
            'min' => 'O CAMPO RUA DEVE CONTER PELO MENOS :min CARACTERES.',
        ),
        'number' => array(
            'required' => 'O CAMPO NÚMERO É OBRIGATÓRIO.',
            'min' => 'O CAMPO NÚMERO DEVE CONTER PELO MENOS :min CARACTERES.',
        ),
        'phone' => array(
            'required' => 'O CAMPO TELEFONE É OBRIGATÓRIO.',
            'min' => 'O CAMPO TELEFONE DEVE CONTER PELO MENOS :min CARACTERES.',
        ),
    ),

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
