<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellIten extends Model
{
    //
    protected $fillable = ['sell_id', 'item_id', 'waiter_id', 'value_item'];
}
