<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    //
    protected $fillable = ['board_id', 'sell_id'];

    public function boards()
    {
        return $this->hasMany('App\Board');
    }
}
