<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 24/02/2017
 * Time: 18:50
 */

namespace App\Service;


use App\Board;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReportService extends ServiceController
{
    private $MSG_EXCEPTION = 'ERRO AO CARREGAR DADOS ';

    public function teste()
    {
        return $this->returnJson($this->codeSuccess, $this->messageSuccess, 'teste');
    }

    public function selectAllSellInactiveByDate(Request $request)
    {
        try {
            $sell_inactive_by_date = DB::table('sells')
                ->select('*')
                ->whereBetween('created_at', [$request->input('data_inicio'), $request->input('data_fim') ])
                ->where('sells.status', false)
                ->get();
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $sell_inactive_by_date);
        } catch (\Exception $e) {
            return $this->returnJson($this->codeError, $this->messageError, $this->MSG_EXCEPTION);
        }
    }

    public function selectSellWaiterByDate(Request $request)
    {
        try {
            $selectSellWaiterByDate = DB::table('sell_itens')
                ->join('waiters', 'sell_itens.waiter_id', '=', 'waiters.id')
                ->join('itens', 'sell_itens.item_id', '=', 'itens.id')
                ->join('sells', 'sell_itens.sell_id', '=', 'sells.id')
                ->select('itens.id as id_item', 'waiters.id as id_waiter','sell_itens.sell_id', 'sell_itens.created_at', 'waiters.name', 'itens.description', 'sell_itens.value_item')
                ->whereBetween('sells.created_at', [$request->input('data_inicio'), $request->input('data_fim') ])
                ->where('sells.status', false)
                ->orderBy('sell_itens.sell_id', 'desc')
                ->orderBy('itens.id', 'desc')
                ->orderBy('waiters.id', 'desc')
                ->get();
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $selectSellWaiterByDate);
        } catch (\Exception $e) {
            return $this->returnJson($this->codeError, $this->messageError, $this->MSG_EXCEPTION);
        }
    }

    public function quantityWaiter()
    {
        try {
            $waiter = DB::table('waiters');
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $waiter->count());
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }
    }

    public function selectSellByBoard (Request $request) {
        try {
            $selectSellByBoard = DB::table('groups')
                ->join('boards', 'boards.id', '=', 'groups.board_id')
                ->join('sells', 'sells.id', '=', 'groups.sell_id')
                ->select('sells.id', 'boards.description', 'sells.value_all', 'sells.created_at')
                ->whereBetween('sells.created_at', [$request->input('data_inicio'), $request->input('data_fim') ])
                ->where('sells.status', false)
                ->orderBy('sells.id', 'desc')
                ->get();
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $selectSellByBoard);
        } catch (\Exception $e) {
            return $this->returnJson($this->codeError, $this->messageError, $this->MSG_EXCEPTION);
        }
    }

    public function quantityBoard()
    {
        try {
            $board = DB::table('boards');
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $board->count());
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }
    }

    public function quantityItem()
    {
        try {
            $item = DB::table('itens');
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $item->count());
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }
    }

    public function qtdSellOpen()
    {
        try {
            $sell = DB::table('sells')
            ->where('sells.status', true)
            ->get();
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $sell->count());
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }
    }

    public function qtdSellClose()
    {
        try {
            $sell = DB::table('sells')
                ->where('sells.status', false)
                ->get();
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $sell->count());
        } catch (Exception $exception) {
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }
    }

    public function valueAllSell()
    {
        try {
            $sell = DB::table('sells')
                ->select(DB::raw('sum(sells.value_all)'))
                ->where('sells.status', false)
                ->get();
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $sell);
        } catch (Exception $exception) {
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }
    }

    public function countClient()
    {
        try {
            $clients = DB::table('clients');
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $clients->count());
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }
    }

    public function countUser()
    {
        try {
            $users = DB::table('users');
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $users->count());
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }
    }
}