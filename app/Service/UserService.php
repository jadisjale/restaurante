<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 24/02/2017
 * Time: 18:50
 */

namespace App\Service;


use App\User;
use League\Flysystem\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserService extends ServiceController implements InterfaceServiceController
{

    private $user;
    private $arrayValidation = array();

    function __construct()
    {
        $this->user = new User();
    }

    public function save(Request $request)
    {
        try {
            $this->user->name = $request->input('name');
            $this->user->email = $request->input('email');
            $this->user->password = $request->input('password');
            if (!$this->validation()){
                $this->user-> save();
                return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->user);
            } else {
                return $this->returnJson($this->codeError, $this->validation, $this->arrayValidation);
            }
        } catch (Exception $e){
            return $this->returnJson($this->codeError, $this->messageError, null);
        }
    }

    public function update(Request $request, $id)
    {

    }

    public function remove(Request $request, $id)
    {
        // TODO: Implement remove() method.
    }

    public function findAll(Request $request)
    {
        // TODO: Implement findAll() method.
    }

    public function findPk(Request $request, $id)
    {
        try {
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->user->find($id));
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }
    }

    public function validation()
    {
        $validator = Validator::make(
            array(
                'name' => $this->user->name,
                'email' => $this->user->email,
                'password' => $this->user->password
            ),
            array(
                'name' => 'required|min:3',
                'email' => 'required|min:3',
                'password' => 'required|min:3|max:6'

            )
        );

        if ($validator->fails())
        {
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {
                array_push($this->arrayValidation, $message);
            }
        }

        return $this->arrayValidation ? true : false;
    }


}