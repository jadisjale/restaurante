<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 24/02/2017
 * Time: 18:50
 */

namespace App\Service;


use App\Client;
use League\Flysystem\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ClientService extends ServiceController implements InterfaceServiceController
{

    private $client;
    private $arrayValidation = array();
    private $MSG_REGISTER = ' CADASTRADO(A) COM SUCESSO! ';
    private $MSG_NAME_EQUAL = ' JÁ EXISTE UM CLIENTE COM ESSE NOME ';
    private $MSG_UPDATE_DEPT = ' VALOR DA DIVIDA ATUALIZADO COM SUCESSO! ';
    private $MSG_EXCEPTION_SAVE = ' NÃO FOI POSSÍVEL SALVAR ESSE CLIENTE ';
    private $MSG_EXCEPTION_FIND_ALL = ' ERRO AO CONSULTAR LISTA DE CLIENTES ';
    private $MSG_EXCEPTION_FIND_PK = ' ERRO AO CARREGAR DADOS DO CLIENTE ';

    function __construct()
    {
        $this->client = new Client();
    }

    public function save(Request $request)
    {
        try {
            $this->client->name = $request->input('name');
            if (!$this->validation()){
                if ($this->exist($this->client, 'name', $this->client->name)->isEmpty()) {
                    $this->client->save();
                    return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->client->name . $this->MSG_REGISTER);
                } else {
                    return $this->returnJson($this->codeError, $this->messageError, $this->MSG_NAME_EQUAL . $this->client->name);
                }
            } else {
                return $this->returnJson($this->codeError, $this->validation, $this->arrayValidation);
            }
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $this->MSG_EXCEPTION_SAVE);
        }
    }

    public function updateDept (Request $request) {
        try {
            $id = $request->input('id');
            $this->client->value_dept = $request->input('value_dept');
            if (!$this->validationDept()) {
                $this->client->where('id', $id)->update(['value_dept' => $this->client->value_dept ]);
                return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->MSG_UPDATE_DEPT);
            } else {
                return $this->returnJson($this->codeError, $this->validation, $this->arrayValidation);
            }
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception->getMessage());
        }
    }

    public function update(Request $request, $id)
    {

    }

    public function remove(Request $request, $sell_id)
    {

    }

    public function findAll(Request $request){
        try {
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->client->all());
        } catch (Exception $e) {
            return $this->returnJson($this->codeError, $this->messageError, $this->MSG_EXCEPTION_FIND_ALL);

        }
    }

    public function findPk(Request $request, $id)
    {
        try {
            $result = $this->client->where('id', $id)->get();
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $result);
        } catch (\Exception $e) {
            return $this->returnJson($this->codeError, $this->messageError, $this->MSG_EXCEPTION_FIND_PK);
        }

    }

    public function validation()
    {
        $validator = Validator::make(
            array(
                'name' => $this->client->name,
            ),
            array(
                'name' => 'required|min:3|max:50',
            )
        );

        if ($validator->fails())
        {
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {
                array_push($this->arrayValidation, $message);
            }
        }

        return $this->arrayValidation ? true : false;
    }

    public function validationDept()
    {
        $validator = Validator::make(
            array(
                'pay' => $this->client->value_dept,
            ),
            array(
                'pay' => 'required|numeric',
            )
        );

        if ($validator->fails())
        {
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {
                array_push($this->arrayValidation, $message);
            }
        }

        return $this->arrayValidation ? true : false;
    }

}