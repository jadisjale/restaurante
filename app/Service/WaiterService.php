<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 24/02/2017
 * Time: 18:50
 */

namespace App\Service;


use App\Waiter;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\App;
use League\Flysystem\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WaiterService extends ServiceController implements InterfaceServiceController
{

    private $waiter;
    private $arrayValidation = array();
    private $MSG_SAVE = ' JÁ EXISTE UM GARÇOM COM O NOME ';
    private $MSG_ID = ' ID NÃO ENCONTRADO ';

    function __construct()
    {
        $this->waiter = new Waiter();
    }

    public function save(Request $request)
    {
        try {
            $this->waiter->name = $request->input('name');
            $this->waiter->status = $request->input('status');
            if (!$this->validation()){
                if ($this->exist($this->waiter, 'name', $this->waiter->name)->isEmpty()) {
                    $this->waiter->save();
                    return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->waiter);
                } else {
                    return $this->returnJson($this->codeError, $this->messageError, $this->MSG_SAVE . $this->waiter->name);
                }
                return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->waiter);
            } else {
                return $this->returnJson($this->codeError, $this->validation, $this->arrayValidation);
            }
        } catch (QueryException $e){
            App::error(function(QueryException $exception)
            {
                return $exception->getMessage();
            });
        }
    }

    public function update(Request $request, $id)
    {
        try {
            if ($id) {
                $result = $this->waiter->where('id', $id)->update(['status' => $request->input('status')]);
                return $this->returnJson($this->codeSuccess, $this->messageSuccess, $result);
            } else {
                return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->MSG_ID);
            }
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception->getMessage());
        }
    }

    public function remove(Request $request, $id)
    {

    }

    public function findAll(Request $request)
    {
        try {
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->waiter->select('*')->get());
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }
    }

    public function findAllWaiterActivated(Request $request)
    {
        try {
            return $this->returnJson($this->codeSuccess, $this->messageSuccess,
                $this->waiter
                    ->where('waiters.name', '<>', 'ADMIN')
                    ->where('status', 1)
                    ->get());
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }
    }
    public function findAllWaiterDisabled(Request $request)
    {
        try {
            return $this->returnJson($this->codeSuccess, $this->messageSuccess,
                $this->waiter
                    ->where('waiters.name', '<>', 'ADMIN')
                    ->where('status', 0)
                    ->get());
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }
    }


    public function findPk(Request $request, $id)
    {
        try {
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->waiter->where('id', $id)->get());
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }
    }

    public function validation()
    {
        $validator = Validator::make(
            array(
                'name' => $this->waiter->name,
                'status' => $this->waiter->status,
            ),
            array(
                'name' => 'required|min:3|max:50',
                'status' => 'required',
            )
        );

        if ($validator->fails())
        {
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {
                array_push($this->arrayValidation, $message);
            }
        }

        return $this->arrayValidation ? true : false;
    }


}