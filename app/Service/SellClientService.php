<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 24/02/2017
 * Time: 18:50
 */

namespace App\Service;


use App\Http\Controllers\ReportController;
use App\SellClient;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SellClientService extends ServiceController implements InterfaceServiceController
{

    private $sellClient;
    private $arrayValidation = array();
    private $MSG_SAVE = ' ATRIBUÍDO COM SUCESSO! ';
    private $MSG_UPDATE = ' MESA ESTÁ OCUPADA NO MOMENTO! ';
    private $MSG_SAVE_EXCEPTION = ' NÃO FOI POSSÍVEL ATRIBUIR O CLIENTE AO PEDIDO! ';
    private $MSG_REMOVE_EXCEPTION = ' ERRO AO TENTAR ATUALIZAR O CLIENTE COM O PEDIDO! ';
    private $MSG_EXCEPTION = ' ERRO AO CARREGAR DADOS ';

    function __construct()
    {
        $this->sellClient = new SellClient();
    }

    public function save(Request $request)
    {
        try {
            DB::beginTransaction();
            $this->sellClient->sell_id = $request->input('sell_id');
            $this->sellClient->client_id = $request->input('client_id');
            $this->sellClient->status = $request->input('status');
            if (!$this->validation()) {
                $this->removeBeforeSave($this->sellClient->sell_id);
                $this->sellClient->save();
                DB::commit();
                return $this->returnJson($this->codeSuccess, $this->codeSuccess, $this->MSG_SAVE);
            } else {
                return $this->returnJson($this->codeError, $this->validation, $this->arrayValidation);
            }
        } catch (Exception $exception) {
            DB::rollBack();
            return $this->returnJson($this->codeError, $this->messageError, $this->MSG_SAVE_EXCEPTION);
        }
    }

    public function update(Request $request, $id)
    {
        $board = $this->checkBoardsOccupied($id);
        try {
            if (!$board->isEmpty()) {
                return $this->returnJson($this->codInfo, $this->messageInfo, $this->MSG_UPDATE);
            } else {
                $result = $this->board->where('id', $id)->update(['status' => $request->input('status')]);
                return $this->returnJson($this->codeSuccess, $this->messageSuccess, $result);
            }
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception->getMessage());
        }

    }

    public function remove(Request $request, $sell_id)
    {

    }

    public function findAll(Request $request){

    }

    private function removeBeforeSave($sell_id) {
        try {
            $this->sellClient->where('sell_id', $sell_id)->delete();
        } catch (Exception $e) {
            throw new Exception($this->MSG_REMOVE_EXCEPTION);
        }
    }

    public function findPk(Request $request, $id)
    {
        // TODO: Implement findPk() method.
    }

    public function validation()
    {
        $validator = Validator::make(
            array(
                'sell_id' => $this->sellClient->sell_id,
                'client_id' => $this->sellClient->client_id,
                'status' => $this->sellClient->status,
            ),
            array(
                'sell_id' => 'required',
                'client_id' => 'required',
                'status' => 'required',
            )
        );

        if ($validator->fails())
        {
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {
                array_push($this->arrayValidation, $message);
            }
        }

        return $this->arrayValidation ? true : false;
    }

    public function getAllSellByClient(Request $request) {
        try {
            $getAllSellByClient = DB::table('sell_clients')
                ->join('sells', 'sell_clients.sell_id', '=', 'sells.id')
                ->join('clients', 'sell_clients.client_id', '=', 'clients.id')
                ->select('sells.id', 'sells.value_all')
                ->where('clients.id', $request->input('client_id'))
                ->where('sell_clients.status', false)
                ->get();
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $getAllSellByClient);
        } catch (\Exception $e) {
            return $this->returnJson($this->codeError, $this->messageError, $this->MSG_EXCEPTION);
        }
    }

}