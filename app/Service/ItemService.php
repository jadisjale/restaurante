<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 24/02/2017
 * Time: 18:50
 */

namespace App\Service;


use App\Iten;
use League\Flysystem\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class ItemService extends ServiceController implements InterfaceServiceController
{

    private $item;
    private $MSG_NAME_EQUAL = ' JÁ EXISTE UM ITEM COM ESSE NOME ';
    private $MSG_UPDATE = ' ID NÃO ENCONTRADO ';
    private $arrayValidation = array();

    function __construct()
    {
        $this->item = new Iten();
    }

    public function save(Request $request)
    {
        try {
            $this->item->description = $request->description;
            $this->item->value = $request->value;
            $this->item->obs = $request->obs;
            $this->item->status = $request->status;
            if (!$this->validation()){
                if ($this->exist($this->item, 'description', $this->item->description)->isEmpty()) {
                    $this->item->save();
                    return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->item);
                } else {
                    return $this->returnJson($this->codeError, $this->messageError, $this->MSG_NAME_EQUAL . $this->item->description);
                }
            } else {
                return $this->returnJson($this->codeError, $this->validation, $this->arrayValidation);
            }
        } catch (Exception $e){
            return $this->returnJson($this->codeError, $this->messageError, null);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            if ($id) {
                $result = $this->item->where('id', $id)->update(['description' => $request->input('description'),
                    'value' => $request->input('value'),
                    'obs' => $request->input('obs'),
                    'status' => $request->input('status')]);
                return $this->returnJson($this->codeSuccess, $this->messageSuccess, $result);
            } else {
                return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->MSG_UPDATE);
            }
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception->getMessage());
        }
    }

    public function updateStatus(Request $request, $id)
    {
        try {
            if ($id) {
                $result = $this->item->where('id', $id)->update(['status' => $request->input('status')]);
                return $this->returnJson($this->codeSuccess, $this->messageSuccess, $result);
            } else {
                return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->MSG_UPDATE);
            }
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception->getMessage());
        }
    }



    public function remove(Request $request, $id)
    {
        // TODO: Implement remove() method.
    }


    public function findAll(Request $request)
    {
        try {
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->item->all());
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }
    }

    public function findPk(Request $request, $id)
    {
        try {
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->item->find($id));
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }
    }

    public function findAllItensActivated(Request $request)
    {
        try {
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->item->where('status', 1)->get());
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }

    }

    public function findAllItensDisabled(Request $request)
    {
        try {
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->item->where('status', 0)->get());
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, null);
        }

    }

    public function validation()
    {
        $validator = Validator::make(
            array(
                'description' => $this->item->description,
                'value' => $this->item->value,
                'obs' => $this->item->obs,
                'status' => $this->item->status
            ),
            array(
                'description' => 'required|min:3|max:50',
                'value' => 'required|numeric',
                'status' => 'required',
            )
        );

        if ($validator->fails())
        {
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {
                array_push($this->arrayValidation, $message);
            }
        }

        return $this->arrayValidation ? true : false;
    }

    public function getItemAuto(Request $request)
    {
        try {
            $itemActive = $this->item->select('id', 'description', 'value')
                ->where('description', 'like', '%' . strtoupper ($request->q) . '%')
                ->where('status', true)
                ->limit(10)
                ->get();
            $array = array();
            foreach ($itemActive as $active) {
                $item = array(
                    "id" => $active['id'],
                    "text" => $active['description'] . ' - ' . 'R$' . number_format($active['value'], 2, ',', '.')
                );
                array_push($array, $item);
            }
            return $array;
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }
    }

}