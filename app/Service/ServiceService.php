<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 23/12/2017
 * Time: 00:23
 */

namespace App\Service;


use App\Service;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class ServiceService extends ServiceController implements InterfaceServiceController
{

    private $service;
    private $arrayValidation = array();
    private $MSG_ALERT = 'JÁ EXISTE UM SERVIÇO COM O NOME ';
    private $MSG_ID = 'ID INVÁLIDO ';
    private $MSG_ERRO = 'OCORREU UM ERRO ';
    private $MSG_EXCEPTION_STATUS = ' ERRO AO ALTERAR STATUS DO PEDIDO ';

    function __construct()
    {
        $this->service = new Service();
    }

    public function save(Request $request)
    {
        try {
            $this->service->name = $request->input('name');
            $this->service->value = $request->input('value');
            $this->service->status = $request->input('status');
            if (!$this->validation()){
                if ($this->exist($this->service, 'name', $this->service->name)->isEmpty()) {
                    $this->service->save();
                    return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->service);
                } else {
                    return $this->returnJson($this->codeError, $this->messageError, $this->MSG_ALERT . $this->service->name);
                }
            } else {
                return $this->returnJson($this->codeError, $this->validation, $this->arrayValidation);
            }
        } catch (Exception $e){
            return $this->returnJson($this->codeError, $this->messageError, $this->MSG_ERRO);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $this->service->name = $request->input('name');
            $this->service->value = $request->input('value');
            if (!$this->validationUpdate()){
                if ($this->exist($this->service, 'name', $this->service->name)->isEmpty()) {
                    $result = $this->service->where('id', $id)->update(
                        [
                            'name'   => $this->service->name,
                            'value'  =>  $this->service->value,
                        ]
                    );
                    return $this->returnJson($this->codeSuccess, $this->messageSuccess, $result);
                } else {
                    return $this->returnJson($this->codeError, $this->messageError, $this->MSG_ALERT . $this->service->name);
                }
            } else {
                return $this->returnJson($this->codeError, $this->validation, $this->arrayValidation);
            }
        } catch (Exception $e){
            return $this->returnJson($this->codeError, $this->messageError, $this->MSG_ERRO);
        }
    }

    public function remove(Request $request, $id)
    {
        // TODO: Implement remove() method.
    }

    public function findAll(Request $request)
    {
        // TODO: Implement findAll() method.
    }

    public function getAllInactive()
    {
        try {
            return $this->returnJson($this->codeSuccess, $this->messageSuccess,
                $this->service
                    ->where('status', false)
                    ->get());
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }
    }

    public function getAllActive()
    {
        try {
            return $this->returnJson($this->codeSuccess, $this->messageSuccess,
                $this->service
                    ->where('status', true)
                    ->get());
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }
    }

    public function updateStatus($id, $status)
    {
        try {
            if ($id && $status) {
                $this->service->where('id', $id)->update(['status' => $status]);
                return $this->returnJson($this->codeSuccess, $this->messageSuccess, 'ATUALIZADO COM SUCESSO');
            } else {
                return $this->returnJson($this->codeError, $this->messageError, $this->MSG_ID);
            }
        } catch (Exception $e) {
            return $this->returnJson($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function getAllSerivesBySell($sell_id)
    {
        try {
            $services = DB::table('sells as sell')
                ->join('sell_services as sell_service', 'sell.id', '=', 'sell_service.sell_id')
                ->join('services as service', 'sell_service.service_id', '=', 'service.id')
                ->select('service.*')
                ->where('sell.id', $sell_id)
                ->get();
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $services);
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }
    }

    public function findPk(Request $request, $id)
    {
        // TODO: Implement findPk() method.
    }

    public function validation()
    {
        $validator = Validator::make(
            array(
                'name' => $this->service->name,
                'value' => $this->service->value ,
                'status' => $this->service->status,
            ),
            array(
                'name'   => 'required|min:1',
                'value'  => 'required|numeric',
                'status' => 'required',
            )
        );

        if ($validator->fails()) {
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {
                array_push($this->arrayValidation, $message);
            }
        }

        return $this->arrayValidation ? true : false;
    }

    public function validationUpdate()
    {
        $validator = Validator::make(
            array(
                'name' => $this->service->name,
                'value' => $this->service->value ,
            ),
            array(
                'name'   => 'required|min:1',
                'value'  => 'required|numeric',
            )
        );

        if ($validator->fails()) {
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {
                array_push($this->arrayValidation, $message);
            }
        }

        return $this->arrayValidation ? true : false;
    }
}