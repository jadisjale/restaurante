<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 24/02/2017
 * Time: 18:50
 */

namespace App\Service;


use App\Board;
use App\Sell;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BoardService extends ServiceController implements InterfaceServiceController
{

    private $board;
    private $arrayValidation = array();
    private $MSG_ALERT = 'JÁ EXISTE UMA MESA COM O NOME OU N° ';
    private $MSG_ALERT_BUSY = 'MESA ESTÁ OCUPADA NO MOMENTO ';
    private $MSG_ID = 'ID INVÁLIDO ';
    private $MSG_EXCEPTION = 'ERRO AO ALTERAR STATUS DA MESA ';

    function __construct()
    {
        $this->board = new Board();
    }

    public function save(Request $request)
    {
        try {
            $this->board->description = $request->input('description');
            $this->board->status = $request->input('status');
            if (!$this->validation()){
                if ($this->exist($this->board, 'description', $this->board->description)->isEmpty()) {
                    $this->board->save();
                    return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->board);
                } else {
                    return $this->returnJson($this->codeError, $this->messageError, $this->MSG_ALERT . $this->board->description);
                }
            } else {
                return $this->returnJson($this->codeError, $this->validation, $this->arrayValidation);
            }
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }
    }

    public function update(Request $request, $id)
    {
        $board = $this->checkBoardsOccupied($id);
        try {
            if (!$board->isEmpty()) {
                return $this->returnJson($this->codInfo, $this->messageInfo, $this->MSG_ALERT_BUSY);
            } else {
                $result = $this->board->where('id', $id)->update(['status' => $request->input('status')]);
                return $this->returnJson($this->codeSuccess, $this->messageSuccess, $result);
            }
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception->getMessage());
        }

    }

    /**
     * Verificar se a mesa esta em um pedido aberto
    */
    public function updateStatus($id, $status)
    {
        try {
            if ($id) {
                $this->board->where('id', $id)->update(['status' => $status]);
                return true;
            } else {
                throw new Exception($this->MSG_ID);
            }
        } catch (Exception $e) {
            throw new Exception($this->MSG_EXCEPTION);
        }

    }

    private function checkBoardsOccupied($id) {
        try {
            $board = DB::table('boards')
                ->select('boards.id')
                ->join('groups', 'groups.board_id', '=', 'boards.id')
                ->join('sells', 'sells.id', '=', 'groups.sell_id')
                ->where('sells.status', true)
                ->where('boards.id', '=', $id)
                ->get();
            return $board;
        } catch (\Exception $e){
            throw new Exception($e->getMessage());
        }
    }

    public function remove(Request $request, $sell_id)
    {

    }

    public function findAll(Request $request){

    }

    public function findAllBoardActivated(Request $request)
    {
        try {
            return $this->returnJson($this->codeSuccess, $this->messageSuccess,
                $this->board
                    ->where('boards.description', '<>', 'ADMIN')
                    ->where('status', 1)
                    ->get());
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }

    }

    public function findAllBoardDisabled(Request $request)
    {
        try {
            return $this->returnJson($this->codeSuccess, $this->messageSuccess,
                $this->board
                    ->where('boards.description', '<>', 'ADMIN')
                    ->where('status', 0)
                    ->get());
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, null);
        }

    }

    public function findPk(Request $request, $id)
    {
        // TODO: Implement findPk() method.
    }

    public function validation()
    {
        $validator = Validator::make(
            array(
                'description' => $this->board->description,
                'status' => $this->board->status,
            ),
            array(
                'description' => 'required|min:1|max:50',
                'status' => 'required|min:1|max:1',
            )
        );

        if ($validator->fails())
        {
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {
                array_push($this->arrayValidation, $message);
            }
        }

        return $this->arrayValidation ? true : false;
    }

}