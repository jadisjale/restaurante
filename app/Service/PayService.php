<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 24/02/2017
 * Time: 18:50
 */

namespace App\Service;


use App\pay;
use League\Flysystem\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PayService extends ServiceController implements InterfaceServiceController
{

    private $pay;
    private $arrayValidation = array();
    private $MSG_REGISTER = ' CADASTRADO COM SUCESSO! ';
    private $MSG_EXCEPTION_SAVE = ' NÃO FOI POSSÍVEL SALVAR ESSE PAYE ';
    private $MSG_EXCEPTION_FIND_ALL = ' ERRO AO CONSULTAR LISTA DE PAYES ';

    function __construct()
    {
        $this->pay = new Pay();
    }

    public function save(Request $request)
    {
        try {
            $this->pay->client_id = $request->input('client_id');
            $this->pay->pay = $request->input('pay');
            $this->pay->added_on = $request->input('added_on');
            if (!$this->validation()) {
                $this->pay->save();
                return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->pay->name . $this->MSG_REGISTER);
            } else {
                return $this->returnJson($this->codeError, $this->messageError, $this->arrayValidation);
            }
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $this->MSG_EXCEPTION_SAVE);
        }
    }

    public function update(Request $request, $id)
    {
        try {

        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception->getMessage());
        }

    }

    public function remove(Request $request, $sell_id)
    {

    }

    public function findAll(Request $request){
        try {
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->pay->all());
        } catch (Exception $e) {
            return $this->returnJson($this->codeError, $this->messageError, $this->MSG_EXCEPTION_FIND_ALL);
        }
    }

    public function findPk(Request $request, $id)
    {
        // TODO: Implement findPk() method.
    }

    public function validation()
    {
        $validator = Validator::make(
            array(
                'client_id' => $this->pay->client_id,
                'pay' => $this->pay->pay,
                'added_on' => $this->pay->added_on,
            ),
            array(
                'client_id' => 'required',
                'pay' => 'required|numeric',
                'added_on' => 'required|'
            )
        );

        if ($validator->fails())
        {
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {
                array_push($this->arrayValidation, $message);
            }
        }

        return $this->arrayValidation ? true : false;
    }

}