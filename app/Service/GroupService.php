<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 24/02/2017
 * Time: 18:50
 */

namespace App\Service;


use App\Board;
use App\Group;
use League\Flysystem\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GroupService extends ServiceController implements InterfaceServiceController
{

    private $group;
    private $arrayValidation = array();

    function __construct()
    {
        $this->group = new Group();
    }

    public function save(Request $request)
    {
        try {
            $this->group->board_id = $request->input('board_id');
            $this->group->sell_id = $request->input('sell_id');
            if (!$this->validation()){
                $this->group->save();
                return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->group);
            } else {
                return $this->returnJson($this->codeError, $this->validation, $this->arrayValidation);
            }
        } catch (Exception $e){
            return $this->returnJson($this->codeError, $this->messageError, null);
        }
    }

    public function update(Request $request, $id)
    {
        // TODO: Implement update() method.
    }

    public function remove(Request $request, $sell_id)
    {
        // TODO: Implement remove() method.
        try {
            $boards_disable = new Group();
            $b = $boards_disable->select('*')->where('sell_id', $sell_id)->get();
            foreach ($b as $value) {
                $board = new Board();
                $board->id =$value->board_id;
                $board->where('id', $board->id)->update(['status' => 1]);
            }
            $this->group->where('sell_id', $sell_id)->delete();
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function findAll(Request $request)
    {
        // TODO: Implement findAll() method.
    }

    public function findPk(Request $request, $id)
    {
        // TODO: Implement findPk() method.
    }

    public function validation()
    {
        $validator = Validator::make(
            array(
                'board_id' => $this->group->board_id,
                'sell_id' => $this->group->sell_id,
            ),
            array(
                'board_id' => 'required|numeric',
                'sell_id' => 'required|numeric',
            )
        );

        if ($validator->fails())
        {
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {
                array_push($this->arrayValidation, $message);
            }
        }

        return $this->arrayValidation ? true : false;
    }


}