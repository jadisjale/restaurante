<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 24/02/2017
 * Time: 18:50
 */

namespace App\Service;


use App\Board;
use App\Establishment;
use App\Sell;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EstablishmentService extends ServiceController implements InterfaceServiceController
{

    private $establishment;
    private $arrayValidation = array();
    private $MSG_ALERT = 'JÁ EXISTE UM ESTABELECIMENTO COM ESSE NOME ';
    private $MSG_ID = 'ID INVÁLIDO ';
    private $MSG_EXCEPTION = 'ERRO AO ALTERAR DADOS DO ESTABELECIMENTO ';

    function __construct()
    {
        $this->establishment = new Establishment();
    }

    public function save(Request $request)
    {
        try {
            $this->establishment->name = $request->input('establishment');
            $this->establishment->state = $request->input('state');
            $this->establishment->city = $request->input('city');
            $this->establishment->street = $request->input('street');
            $this->establishment->number = $request->input('number');
            $this->establishment->phone = $request->input('phone');
            if (!$this->validation()){
                $this->establishment->save();
                $this->session($request);
                return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->establishment);
            } else {
                return $this->returnJson($this->codeError, $this->validation, $this->arrayValidation);
            }
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            if ($id) {
                $result = $this->establishment->where('id', $id)->update(['name' => $request->input('establishment'),
                    'state' => $request->input('state'),
                    'city' => $request->input('city'),
                    'street' => $request->input('street'),
                    'number' => $request->input('number'),
                    'phone' => $request->input('phone')]);
                $this->session($request);
                return $this->returnJson($this->codeSuccess, $this->messageSuccess, $result);
            } else {
                return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->MSG_ID);
            }
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception->getMessage());
        }

    }
    /**
     * Verificar se a mesa esta em um pedido aberto
    */
    public function saveUpdate(Request $request)
    {
        try {
            if ($request->input('id') == null) {
                return $this->save($request);
            } else {
                return $this->update($request, $request->input('id'));
            }
        } catch (Exception $e) {
            throw new Exception($this->MSG_EXCEPTION);
        }
    }

    public function session(Request $request)
    {
        $request->session()->forget('id');
        $request->session()->forget('name');
        $request->session()->forget('state');
        $request->session()->forget('city');
        $request->session()->forget('street');
        $request->session()->forget('number');
        $request->session()->forget('key');
        $request->session()->forget('phone');
        $est = DB::table('establishments')->where('id', 1)->get();
        $request->session()->put('id', isset($est[0] ) ? $est[0]->id : '');
        $request->session()->put('name', isset($est[0] ) ? $est[0]->name : 'ESTABELECIMENTO');
        $request->session()->put('state', isset($est[0] ) ? $est[0]->state : '');
        $request->session()->put('city', isset($est[0] ) ? $est[0]->city : '');
        $request->session()->put('street', isset($est[0] ) ? $est[0]->street : '');
        $request->session()->put('number', isset($est[0] ) ? $est[0]->number : '');
        $request->session()->put('phone', isset($est[0] ) ? $est[0]->phone : '');
    }

    public function remove(Request $request, $sell_id)
    {

    }

    public function findAll(Request $request){

    }


    public function findPk(Request $request, $id)
    {
        // TODO: Implement findPk() method.
    }

    public function validation()
    {
        $validator = Validator::make(
            array(
                'name' => $this->establishment->name,
                'state' => $this->establishment->state,
                'city' => $this->establishment->city,
                'street' => $this->establishment->street,
                'number' => $this->establishment->number,
                'phone' => $this->establishment->phone,
            ),
            array(
                'name' => 'required|min:1|max:50',
                'state' => 'required|min:2',
                'city' => 'required|min:1',
                'street' => 'required|min:1',
                'number' => 'required|min:1',
                'phone' => 'required|min:8',
            )
        );

        if ($validator->fails())
        {
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {
                array_push($this->arrayValidation, $message);
            }
        }

        return $this->arrayValidation ? true : false;
    }

}