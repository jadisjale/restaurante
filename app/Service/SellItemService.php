<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 24/02/2017
 * Time: 18:50
 */

namespace App\Service;

use App\Board;
use App\Group;
use App\Iten;
use App\Sell;
use App\SellIten;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Psy\Exception\Exception;

class SellItemService extends ServiceController implements InterfaceServiceController
{

    private $sellItem;
    private $boarService;
    private $groupService;
    private $sellService;
    private $sell;
    private $item;
    private $arrayValidation = array();
    private $MSG_REMOVE_ITEM = ' PRODUTO REMOVIDO COM SUCESSO! ';
    private $MSG_REMOVE_EXCEPTION = ' ERRO AO REMOVER OS PRODUTOS! ';
    private $MSG_FINISH_SELL = ' PEDIDO FINALIZADO COM SUCESSO! ';
    private $MSG_UPDATE_BOARD = ' MESA ATUALIZADA COM SUCESSO! ';

    function __construct()
    {
        $this->sellItem = new SellIten();
        $this->sell = new Sell();
        $this->item = new Iten();
        $this->boarService = new BoardService();
        $this->sellService = new SellService();
        $this->groupService = new GroupService();
    }

    public function removeItem(Request $request) {
        try {
            $item_id = $request->input('item_id');
            $sell_id = $request->input('sell_id');
            $this->sellItem
                ->where('sell_id', '=', $sell_id)
                ->where('item_id', '=', $item_id)
                ->delete();
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->MSG_REMOVE_ITEM);
        } catch (\Exception $e){
            return $this->returnJson($this->codeError, $this->messageError, $this->MSG_REMOVE_EXCEPTION);
        }
    }

    public function save(Request $request)
    {

        try {
            DB::beginTransaction();
            $boards = $request->input('boards');
            if ($request->input('id_sell')) {
                $this->sell->id = $request->input('id_sell');
            } else {
                $this->sell->value_all = 0.0;
                $this->sell->status = 1;
                $this->sell->save();

                foreach ($boards as $value) {
                    $group = new Group();
                    $board = new Board();
                    $board->id = $value;
                    $group->sell_id = $this->sell->id;
                    $group->board_id = $board->id;
                    $group->save();
                    $board->where('id', $board->id)->update(['status' => 0]);
                }
            }

            $item_temp = $this->item->find($request->input('id_item_active'));
            $count = $request->input('count_item'); //quantidade do item
            for ($i = 1; $i <= $count; $i++) {
                $sellItem_temp = new SellIten();
                $sellItem_temp->sell_id = $this->sell->id; //id do pedido
                $sellItem_temp->item_id = $item_temp->id; // id do item
                $sellItem_temp->waiter_id = $request->input('id_waiter'); //id do garcon
                $sellItem_temp->value_item = $item_temp->value; //valor
                $sellItem_temp->save();
            }
            DB::commit();
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->sell->id);
        } catch (QueryException $e){
            DB::rollBack();
            return $this->returnJson($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function sellBox(Request $request)
    {

        try {
            DB::beginTransaction();
            $boards = $request->input('boards');
            if ($request->input('id_sell')) {
                $this->sell->id = $request->input('id_sell');
            } else {
                $this->sell->value_all = 0.0;
                $this->sell->status = 1;
                $this->sell->save();

                foreach ($boards as $value) {
                    $group = new Group();
                    $board = new Board();
                    $board->id = 1;
                    $group->sell_id = $this->sell->id;
                    $group->board_id = $board->id == 1;
                    $group->save();
                    $board->where('id', $board->id)->update(['status' => 0]);
                }
            }

            $item_temp = $this->item->find($request->input('id_item_active'));
            $count = $request->input('count_item'); //quantidade do item
            for ($i = 1; $i <= $count; $i++) {
                $sellItem_temp = new SellIten();
                $sellItem_temp->sell_id = $this->sell->id; //id do pedido
                $sellItem_temp->item_id = $item_temp->id; // id do item
                $sellItem_temp->waiter_id = $request->input(1); //id do garcon
                $sellItem_temp->value_item = $item_temp->value; //valor
                $sellItem_temp->save();
            }
            DB::commit();
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->sell->id);
        } catch (QueryException $e){
            DB::rollBack();
            return $this->returnJson($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function getInfoSell($id) {
        $details_sell = DB::table('sells')
            ->join('sell_itens', 'sells.id', '=', 'sell_itens.sell_id')
            ->join('itens', 'sell_itens.item_id', '=', 'itens.id')
            ->join('waiters', 'sell_itens.waiter_id', '=', 'waiters.id')
            ->select('sell_itens.sell_id', 'itens.description', 'waiters.name', 'sell_itens.value_item', 'sells.created_at', 'sell_itens.id', 'itens.id as item_id')
            ->where('sell_itens.sell_id', $id)
            ->orderBy('itens.description', 'desc')
            ->get();
        return $this->returnJson($this->codeSuccess, $this->messageSuccess, $details_sell);
    }

    function removeItemSell ($id) {
        try {
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->sellItem->where('id', $id)->delete());
        } catch (QueryException $e){
            return $this->returnJson($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function update(Request $request, $id)
    {
        // TODO: Implement update() method.
    }

    public function remove(Request $request, $id)
    {
        // TODO: Implement remove() method.
    }

    public function findAll(Request $request)
    {
        // TODO: Implement findAll() method.
    }

    public function findPk(Request $request, $id)
    {
        // TODO: Implement findPk() method.
    }

    public function countSellAtctive () {
        try {
            $item = $this->sell->where('status', 1)->count();
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $item);
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }
    }

    public function validation()
    {
        $validator = Validator::make(
            array(
                'sell_id' => $this->sellItem->sell_id,
                'item_id' => $this->sellItem->item_id,
                'waiter_id' => $this->sellItem->waiter_id,
                'value_item' => $this->sellItem->value_item,
            ),
            array(
                'value_all' => 'required|numeric',
            )
        );

        if ($validator->fails())
        {
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {
                array_push($this->arrayValidation, $message);
            }
        }

        return $this->arrayValidation ? true : false;
    }

    public function getSellActive(){
        $details_sell = DB::table('sells')
            ->join('groups', 'sells.id', '=', 'groups.sell_id')
            ->join('boards', 'groups.board_id', '=', 'boards.id')
            ->select('sells.id', 'sells.created_at', 'boards.description', 'sells.value_all')
            ->where('sells.status', true)
            ->get();
        return $this->returnJson($this->codeSuccess, $this->messageSuccess, $details_sell);
    }

    public function finishSell($id) {
        try {
            DB::beginTransaction();
            $boards_sell = DB::table('sells')
                ->join('groups', 'sells.id', '=', 'groups.sell_id')
                ->join('boards', 'groups.board_id', '=', 'boards.id')
                ->select('boards.id')
                ->where('sells.status', true)
                ->where('sells.id', $id)
                ->get();

            foreach ($boards_sell as $board){
                $this->boarService->updateStatus($board->id, 1);
            }

            $this->sellService->updateStatus($id, 0);

            DB::commit();
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->MSG_FINISH_SELL);
        } catch (QueryException $e){
            DB::rollBack();
            return $this->returnJson($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function getBoardsBySell($id) {
        $boards_sell = DB::table('boards')
            ->join('groups', 'boards.id', '=', 'groups.board_id')
            ->select('boards.id', 'boards.description')
            ->where('groups.sell_id', $id)
            ->get();
        return $this->returnJson($this->codeSuccess, $this->messageSuccess, $boards_sell);
    }

    public function getBoardsBySellActive($id) {
        $boards_sell = DB::table('boards')
            ->select('boards.id', 'boards.description')
            ->where('boards.id', '!=', $id)
            ->where('boards.status', true)
            ->get();
        return $this->returnJson($this->codeSuccess, $this->messageSuccess, $boards_sell);
    }

    public function getSellInactive(Request $request) {
        $details_sell = DB::table('sells')
            ->join('groups', 'sells.id', '=', 'groups.sell_id')
            ->join('boards', 'groups.board_id', '=', 'boards.id')
            ->select('sells.id', 'sells.created_at', 'boards.description', 'sells.value_all')
            ->whereBetween('sells.created_at', [$request->input('data_inicio'), $request->input('data_fim') ])
            //->where('boards.description', '<>', 'ADMIN')
            ->where('sells.status', false)
            ->get();
        return $this->returnJson($this->codeSuccess, $this->messageSuccess, $details_sell);
    }

    public function checkIfSellActive($id) {
        $var = $this->sell->select('*')->where('id', $id)->first();
        return $this->returnJson($this->codeSuccess, $this->messageSuccess, $var);
    }

    public function getValueBySell($id){
        return $this->sell->select('value_all')->where('id', $id)->first();
    }

    public function updateCountBoards (Request $request) {
        try {

            DB::beginTransaction();
            $boards = $request->input('boards');
            $sell_id = $request->input('id_sell');
            $this->groupService->remove($request, $sell_id);

            foreach ($boards as $value){
                $group = new Group();
                $group->board_id = $value;
                $group->sell_id = $sell_id;
                $group->save();

                $board = new Board();
                $board->id = $value;
                $board->where('id', $board->id)->update(['status' => 0]);
            }

            DB::commit();
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->MSG_UPDATE_BOARD);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnJson($this->codeError, $this->messageError, $e->getMessage());
        }
    }
}