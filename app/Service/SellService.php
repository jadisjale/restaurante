<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 24/02/2017
 * Time: 18:50
 */

namespace App\Service;


use App\Sell;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SellService extends ServiceController implements InterfaceServiceController
{

    private $sell;
    private $boarService;
    private $arrayValidation = array();
    private $MSG_UPDATE_STATUS = ' ID INVÁLIDO ';
    private $MSG_UPDATE_PRICE = ' PREÇO ATUALIZADO COM SUCESSO! ';
    private $MSG_UPDATE_PRICE_ELSE = ' NÃO FOI POSSÍVEL INDENTIFICAR O ID DA VENDA! ';
    private $MSG_EXCEPTION_STATUS = ' ERRO AO ALTERAR STATUS DO PEDIDO ';

    function __construct()
    {
        $this->sell = new Sell();
    }

    public function save(Request $request)
    {
        try {
            $this->sell->value_all = $request->input('value_all');
            if (!$this->validation()){
                $this->sell->save();
                return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->sell);
            } else {
                return $this->returnJson($this->codeError, $this->validation, $this->arrayValidation);
            }
        } catch (Exception $e){
            return $this->returnJson($this->codeError, $this->messageError, null);
        }
    }

    public function getSellService()
    {
        try {
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->sell->with('sellServices')->get());
        } catch (\Exception $e) {
            return $this->returnJson($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function countSellAtctive () {
        try {
            $item = $this->sell->where('status', 1)->count();
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $item);
        } catch (Exception $exception){
            return $this->returnJson($this->codeError, $this->messageError, $exception);
        }
    }

    public function validation()
    {
        $validator = Validator::make(
            array(
                'value_all' => $this->sell->value_all,
            ),
            array(
                'value_all' => 'required|numeric',
            )
        );

        if ($validator->fails())
        {
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {
                array_push($this->arrayValidation, $message);
            }
        }

        return $this->arrayValidation ? true : false;
    }

    public function updateStatus($id, $status)
    {
        try {
            if ($id) {
                $this->sell->where('id', $id)->update(['status' => $status]);
                return true;
            } else {
                throw new Exception($this->MSG_UPDATE_STATUS);
            }
        } catch (Exception $e) {
            throw new Exception($this->MSG_EXCEPTION_STATUS);
        }
    }

    function updatePrice (Request $request) {
        try {
            $id = $request->input('id');
            $value_all = $request->input('value_all');
            if ($request->input('id')) {
                $this->sell->where('id', $id)->update(['value_all' => $value_all]);
                return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->MSG_UPDATE_PRICE);
            } else {
                return $this->returnJson($this->codeError, $this->messageError, $this->MSG_UPDATE_PRICE_ELSE);
            }
        } catch (QueryException $e) {
            return $this->returnJson($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function update(Request $request, $id)
    {
        // TODO: Implement update() method.
    }

    public function remove(Request $request, $id)
    {
        // TODO: Implement remove() method.
    }

    public function findAll(Request $request)
    {
        // TODO: Implement findAll() method.
    }

    public function findPk(Request $request, $id)
    {
        // TODO: Implement findPk() method.
    }

    public function deleteSell($id)
    {
        try {
            DB::beginTransaction();
            $boards = DB::table('groups')
                ->select('board_id')
                ->where('sell_id', $id)
                ->get();
            $this->boarService = new BoardService();
            foreach ($boards as $board){
                $this->boarService->updateStatus($board->board_id, 1);
            }
            DB::table('groups')->where('sell_id', $id)->delete();
            DB::table('sells')->where('id', $id)->delete();
            DB::commit();
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, 'Pedido removido com sucesso!');
        } catch (QueryException $e) {
            DB::rollBack();
            return $this->returnJson($this->codeError, $this->messageError, 'Ocorreu um erro ao excluir pedido');
        } catch (Exception $e) {
            DB::rollBack();
            return $this->returnJson($this->codeError, $this->messageError, 'Ocorreu um erro ao excluir pedido');
        }
    }

}