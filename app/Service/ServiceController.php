<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 24/02/2017
 * Time: 18:52
 */

namespace app\Service;
header('Content-Type: application/json; charset=UTF-8 ');


class ServiceController
{

    protected $codeSuccess = true;
    protected $codeError = false;
    protected $codInfo = 1;
    protected $messageSuccess = 'SUCESSO ';
    protected $messageError = 'ERRO ';
    protected $messageInfo = 'INFORMAÇÃO ';
    protected $validation = 'VALIDAÇÃO ';

    protected function returnJson($code, $message, $dados)
    {
        $json = [
            'code' => $code,
            'message' => $message,
            'data' => $dados
        ];

        return json_encode($json);
    }

    public function exist($model, $key, $value) {
        /**
         * Os atributos que são únicos
         * 1 nome da mesa (feito)
         * 2 nome do garçon
         * 3 nome do item
         * 4 login do usuário
         */
        return $model->where($key, $value)->select($key)->get();
    }
}