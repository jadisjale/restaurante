<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Waiter extends Model
{
    protected $fillable = ['name', 'status'];

    public function sell()
    {
        return $this->hasOne('App\Sell', 'waiter_id');
    }
}
