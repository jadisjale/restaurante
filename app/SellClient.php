<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellClient extends Model
{
    //
    protected $fillable = ['sell_id', 'client_id', 'status'];

}
