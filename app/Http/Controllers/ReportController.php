<?php

namespace App\Http\Controllers;

use App\Service\ItemService;
use App\Service\ReportService;
use App\Service\SellItemService;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    private $reportService;

    function __construct()
    {
        $this->reportService = new ReportService();
    }

    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    public function teste()
    {
        return $this->reportService->teste();
    }

    public function selectAllSellInactiveByDate(Request $request)
    {
        return $this->reportService->selectAllSellInactiveByDate($request);
    }

    public function selectSellWaiterByDate(Request $request)
    {
        return $this->reportService->selectSellWaiterByDate($request);
    }

    public function selectSellByBoard(Request $request)
    {
        return $this->reportService->selectSellByBoard($request);
    }

    public function quantityBoard()
    {
        return $this->reportService->quantityBoard();
    }

    public function quantityWaiter()
    {
        return $this->reportService->quantityWaiter();
    }

    public function quantityItem()
    {
        return $this->reportService->quantityItem();
    }

    public function qtdSellOpen()
    {
        return $this->reportService->qtdSellOpen();
    }

    public function qtdSellClose()
    {
        return $this->reportService->qtdSellClose();
    }

    public function valueAllSell()
    {
        return $this->reportService->valueAllSell();
    }

    public function countClient()
    {
        return $this->reportService->countClient();
    }

    public function countUser()
    {
        return $this->reportService->countUser();
    }

}
