<?php

namespace App\Http\Controllers;

use App\Iten;
use App\Service\SellService;
use App\Service\ItemService;
use Illuminate\Http\Request;

class SellController extends Controller
{
    private $sellService;
    //
    function __construct()
    {
        $this->sellService = new SellService();
    }

    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->sellService->save($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        return  $this->userService->getUserById($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function countSellAtctive()
    {
        return $this->sellService->countSellAtctive();
    }

    public function updatePrice (Request $request) {
        return $this->sellService->updatePrice($request);
    }

    public function teste () {
        return $this->sellService->getSellService();
    }

    public function deleteSell($id) {
        return $this->sellService->deleteSell($id);
    }

}