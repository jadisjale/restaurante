<?php

namespace App\Http\Controllers;

use App\Service\ServiceService;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    private $serviceService;
    //
    function __construct()
    {
        $this->serviceService = new ServiceService();
    }

    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->serviceService->save($request);
    }

    public function getServiceActivated()
    {
        return $this->serviceService->getAllActive();
    }

    public function getServiceDisabled()
    {
        return $this->serviceService->getAllInactive();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateStatus(Request $request, $id)
    {
        return $this->serviceService->updateStatus($id, $request->input('status'));
    }

    public function updateServiceObject(Request $request, $id)
    {
        return $this->serviceService->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}