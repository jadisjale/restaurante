<?php

namespace App\Http\Controllers;

use App\Establishment;
use App\Iten;
use App\Service\BoardService;
use App\Service\EstablishmentService;
use App\Service\ItemService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ViewController extends Controller
{

    function __construct()
    {
        $this->middleware('auth');
    }

    public function estabelecimento()
    {
        $establishments = DB::table('establishments')->where('id', 1)->get();
        return view('estabelecimento')->with('est', $establishments);
    }

    public function index(Request $request)
    {
        $est = new EstablishmentService();
        $est->session($request);
        return view('index');
    }

    public function vendaCaixa()
    {
        return view('venda_caixa');
    }

    public function mesas()
    {
        return view('mesas');
    }

    public function garcom()
    {
        return view('garcom');
    }

    public function produto()
    {
        return view('produto');
    }

    public function usuario()
    {
        return view('usuario');
    }

    public function pedidosAbertos()
    {
        return view('pedidos_abertos');
    }

    public function pedidosFechados()
    {
        return view('pedidos_fechados');
    }

    public function fatura()
    {
        return view('fatura');
    }

    public function faturaMesa()
    {
        return view('pedidos_mesa');
    }

    public function balanco()
    {
        return view('balanco');
    }

    public function pedidoMesa()
    {
        return view('pedidos_mesa');
    }

    public function cliente()
    {
        return view('cliente');
    }

    public function clientePendentePagamento()
    {
        return view('cliente_pendente_pagamento');
    }

    public function home()
    {
        return view('index');
    }

    public function servicos()
    {
        return view('servicos');
    }

    public function extrato($id)
    {
        $comanda = DB::table('sell_itens')
            ->join('itens', 'sell_itens.item_id', '=', 'itens.id')
            ->join('sells', 'sells.id', '=', 'sell_itens.sell_id')
            ->select('itens.description', 'sell_itens.value_item', 'sells.value_all', 'sells.created_at')
            ->where('sells.status', false)
            ->where('sells.id', $id)
            ->orderBy('itens.description', 'desc')
            ->get();

        $mesas = DB::table('groups')
            ->join('boards', 'boards.id', '=', 'groups.board_id')
            ->select('boards.description')
            ->where('groups.sell_id', $id)
            ->orderBy('boards.description', 'asc')
            ->get();

        $teste = $this->super_unique($comanda, 'description');
        $format = array();

        foreach ($teste as $t) {
            $count = 0;
            foreach ($comanda as $c) {
                if ($t->description === $c->description){
                    $count++;
                }
            }
            $item = array('qtd'=>$count);
            array_push($format, array_merge((array)$t, $item));
        }

        $data = date("d/m/Y");
        $hora = date("H:i");

        return view('comanda')
            ->with('comanda', $format)
            ->with('mesas', $mesas)
            ->with('data', $data)
            ->with('hora', $hora);
    }

    function super_unique($array,$key)
    {
        $temp_array = array();
        foreach ($array as &$v) {
            if (!isset($temp_array[$v->$key])) {
                $temp_array[$v->$key] =& $v;
            }
        }
        $array = array_values($temp_array);
        return $array;
    }
}
