<?php

namespace App\Http\Controllers;

use App\Service\ItemService;
use App\Service\SellItemService;
use Illuminate\Http\Request;

class SellItemController extends Controller
{
    private $sellItemService;
    private $item;

    function __construct()
    {
        $this->sellItemService = new SellItemService();
        $this->item = new ItemService();
    }

    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->sellItemService->save($request);
    }

    public function sellBox(Request $request)
    {
        return $this->sellItemService->sellBox($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->sellItemService->removeItemSell($id);
    }

    public function getItemAuto (Request $request){
        return $this->item->getItemAuto($request);
    }

    public function getInfoSell($id)
    {
        return $this->sellItemService->getInfoSell($id);
    }

    public function getSellActive(){
        return $this->sellItemService->getSellActive();
    }

    public function getSellInactive(Request $request){
        return $this->sellItemService->getSellInactive($request);
    }

    public function finishSell ($id) {
        return $this->sellItemService->finishSell($id);
    }

    public function sellDetails (Request $request) {
        $value_total = $this->sellItemService->getValueBySell($request->input('sell_id'));
        $value_total->value_all = 'R$ ' . number_format($value_total->value_all, 2, ',', '.');
        return view('pedidos')
            ->with('sell_id', $request->input('sell_id'))
            ->with('value_total', $value_total->value_all);
    }

    public function sellDetailsInactive (Request $request) {
        $value_total = $this->sellItemService->getValueBySell($request->input('sell_id'));
        $value_total->value_all = 'R$ ' . number_format($value_total->value_all, 2, ',', '.');
        return view('pedidos')
            ->with('sell_id', $request->input('sell_id'))
            ->with('value_total', $value_total->value_all);
    }

    public function sell () {
        return view('pedidos')
            ->with('sell_id', null)
            ->with('value_total', null);
    }

    public function getBoardsBySell ($id) {
        return $this->sellItemService->getBoardsBySell($id);
    }

    public function checkIfSellActive ($id) {
        return $this->sellItemService->checkIfSellActive($id);
    }

    public function updateCountBoards (Request $request) {
        return $this->sellItemService->updateCountBoards($request);
    }

    public function getBoardsBySellActive ($id) {
        return $this->sellItemService->getBoardsBySellActive($id);
    }

    public function removeItem (Request $request) {
        return $this->sellItemService->removeItem($request);
    }

}
