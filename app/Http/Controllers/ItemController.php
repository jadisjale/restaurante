<?php

namespace App\Http\Controllers;

use App\Iten;
use App\Service\ItemService;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    private $itemService;
    //
    function __construct()
    {
        $this->itemService = new ItemService();
    }

    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->itemService->save($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return  $this->userService->getUserById($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->itemService->update($request, $id);
    }

    public function updateStatus(Request $request, $id)
    {
        return $this->itemService->updateStatus($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function findAll(Request $request)
    {
        return $this->itemService->findAll($request);
    }

    public function findById(Request $request, $id)
    {
        return $this->itemService->findPk($request, $id);
    }

    public function findAllItensActivated(Request $request)
    {
        return $this->itemService->findAllItensActivated($request);
    }

    public function findAllItensDisabled(Request $request)
    {
        return $this->itemService->findAllItensDisabled($request);
    }

}
