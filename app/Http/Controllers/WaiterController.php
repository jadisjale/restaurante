<?php

namespace App\Http\Controllers;

use App\Iten;
use App\Service\WaiterService;
use App\Service\ItemService;
use Illuminate\Http\Request;

class WaiterController extends Controller
{
    private $waiterService;
    //
    function __construct()
    {
        $this->waiterService = new WaiterService();
    }

    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->waiterService->save($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return  $this->userService->getUserById($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->waiterService->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function findAll(Request $request)
    {
        return $this->waiterService->findAll($request);
    }

    public function findAllWaiterActivated(Request $request)
    {
        return $this->waiterService->findAllWaiterActivated($request);
    }

    public function findAllWaiterDisabled(Request $request)
    {
        return $this->waiterService->findAllWaiterDisabled($request);
    }

}
