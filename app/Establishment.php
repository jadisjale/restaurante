<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 14/11/2017
 * Time: 16:05
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Establishment extends Model
{
    protected $fillable = ['name', 'state', 'city', 'street', 'number', 'phone'];
}

