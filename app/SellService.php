<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellService extends Model
{
    protected $fillable = ['sell_id', 'service_id'];

    public function service()
    {
        return $this->hasOne('App\Service');
    }
}
