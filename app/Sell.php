<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sell extends Model
{
    protected $fillable = ['value_all, status'];

    public function sellsItens()
    {
        return $this->hasMany('App\SellItem', 'sell_id');
    }

    public function waiter()
    {
        return $this->hasOne('App\Waiter');
    }

    public function sellServices()
    {
        return $this->hasMany('App\SellService', 'sell_id');
    }

    public function services()
    {
        return $this->hasMany('App\Service', 'sell_id');
    }
}
