<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Iten extends Model
{
    protected $fillable = ['description', 'value', 'obs', 'status'];

    public function sellsItens()
    {
        return $this->hasMany('App\SellItem', 'item_id');
    }
}
