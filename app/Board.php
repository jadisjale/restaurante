<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    //
    protected $fillable = ['description', 'status'];

    public function group()
    {
        return $this->hasOne('App\Group', 'board_id');
    }
}
