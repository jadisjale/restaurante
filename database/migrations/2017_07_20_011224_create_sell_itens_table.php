<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellItensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sell_itens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sell_id')->unsigned();
            $table->foreign('sell_id')->references('id')->on('sells');
            $table->bigInteger('item_id')->unsigned();
            $table->foreign('item_id')->references('id')->on('itens');
            $table->bigInteger('waiter_id')->unsigned();
            $table->foreign('waiter_id')->references('id')->on('waiters');
            $table->double('value_item', 15, 8);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sell_itens');
    }
}