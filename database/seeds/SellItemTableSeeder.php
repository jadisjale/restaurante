<?php

use Illuminate\Database\Seeder;

class SellItemTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('sell_itens')->truncate();
    }
}