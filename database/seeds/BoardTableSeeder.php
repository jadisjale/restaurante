<?php

use Illuminate\Database\Seeder;
use App\Board;

class BoardTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('boards')->delete();

        Board::create([
            'description' => 'ADMIN',
            'status' => true
        ]);
    }
}