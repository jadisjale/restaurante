<?php

use Illuminate\Database\Seeder;
use App\Group;

class GroupTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('groups')->truncate();
    }
}