<?php

use Illuminate\Database\Seeder;

class SellTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('sells')->truncate();
    }
}