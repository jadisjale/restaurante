<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call('UserTableSeeder');
        $this->call('WaiterTableSeeder');
        $this->call('BoardTableSeeder');
//        $this->call('ItemTableSeeder');
//        $this->call('SellTableSeeder');
//        $this->call('GroupTableSeeder');
//        $this->call('SellItemTableSeeder');


//        DB::table('users')->insert([
//            'name' => 'teste',
//            'login' => 'jair',
//            'password' => '2710',
//        ]);
//
//        DB::table('waiters')->insert([
//            'name' => 'Maria',
//            'active' => true,
//        ]);
//
//        DB::table('itens')->insert([
//            'description' => 'description',
//            'value' => 19.80,
//            'obs' => 'obs',
//        ]);
//
//        DB::table('boards')->insert([
//            'description' => 'teste'
//        ]);
}
}
