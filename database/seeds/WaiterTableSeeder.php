<?php

use Illuminate\Database\Seeder;
use App\Waiter;

class WaiterTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('waiters')->delete();

        Waiter::create([
            'name' => 'ADMIN',
            'status' => true
        ]);
    }
}