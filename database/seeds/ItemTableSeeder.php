<?php

use Illuminate\Database\Seeder;
use App\Item;

class ItemTableSeeder extends Seeder
{

    public function run()
    {
        //DB::table('itens')->truncate();

        Item::create([
            'description' => 'CERVEJA SKOL 600 ML',
            'value' => 6.00,
            'obs' => 'SKOL',
            'status' => true
        ]);
    }
}