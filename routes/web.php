<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
 *got Rotas para usuario
 */
Route::post('/saveUser', 'UserController@store');
Route::get('/getAllUser', 'UserController@findAll');
/*
 *Rotas para usuario
 */

/*
 *Rotas para item
 */
Route::post('/saveItem', 'ItemController@store');
Route::post('/updateItem/{id}', 'ItemController@update');
Route::post('/updateItemStatus/{id}', 'ItemController@updateStatus');
Route::get('/getAllItem', 'ItemController@findAll');
Route::post('/findById/{id}', 'ItemController@findById');
Route::get('/getItensActivated', 'ItemController@findAllItensActivated');
Route::get('/getItensDisabled', 'ItemController@findAllItensDisabled');
/*
 *Rotas para item
 */

/*
 *Rotas para mesa
 */
Route::post('/saveBoard', 'BoardController@store');
Route::post('/getBoard', 'BoardController@findAll');
Route::get('/getBoardActivated', 'BoardController@findAllBoardActivated');
Route::get('/getBoardDisabled', 'BoardController@findAllBoardDisabled');
Route::post('/updateBoard/{id}', 'BoardController@update');
/*
 *Rotas para mesa
 */

/*
 *Rotas para garçon
 */
Route::post('/saveWaiter', 'WaiterController@store');
Route::post('/updateWaiter/{id}', 'WaiterController@update');
Route::get('/getWaiterActivated', 'WaiterController@findAllWaiterActivated');
Route::get('/getWaiterDisabled', 'WaiterController@findAllWaiterDisabled');
Route::get('/getAllWaiter', 'WaiterController@findAll');
/*
 *Rotas para garçon
 */

/*
 *Rotas para venda item
 */
Route::post('/saveSellItem', 'SellItemController@store');
Route::get('/itemAuto', 'SellItemController@getItemAuto');
Route::get('/getSellActive', 'SellItemController@getSellActive');
Route::post('/getSellInactive', 'SellItemController@getSellInactive');
Route::get('/detalhes_venda/{id}', 'SellItemController@getInfoSell');
Route::get('/remove_item/{id}', 'SellItemController@destroy');
Route::get('/finishSell/{id}', 'SellItemController@finishSell');
Route::get('/pedidos_detalhes', 'SellItemController@sellDetails');
Route::get('/pedidos_fechados_detalhes', 'SellItemController@sellDetailsInactive');
Route::get('/pedidos', 'SellItemController@sell');
Route::post('/vendaCaixa', 'SellItemController@sellBox');
Route::get('/getBoardsBySell/{id}', 'SellItemController@getBoardsBySell');
Route::get('/checkIfSellActive/{id}', 'SellItemController@checkIfSellActive');
Route::get('/getBoardsBySellActive/{id}', 'SellItemController@getBoardsBySellActive');
Route::post('/updateCountBoards', 'SellItemController@updateCountBoards');
Route::post('/removeItem', 'SellItemController@removeItem');
/*
 *Rotas para venda item
 */

/*
 *Rotas para venda
 */
Route::post('/saveSell', 'SellController@store');
Route::get('/countSellActive', 'SellController@countSellAtctive');
Route::post('/updatePrice', 'SellController@updatePrice');
Route::get('/teste1', 'SellController@teste');
Route::get('/deleteSell/{id}', 'SellController@deleteSell');

/*
 *Rotas para venda
 */

/*
 *Rotas para grupo
 */
Route::post('/saveGroup', 'GroupController@store');
/*
 *Rotas para grupo
 */

/*
 *Rotas para Relatorios
 */
Route::post('/teste', 'ReportController@teste');
Route::post('/selectAllSellInactiveByDate', 'ReportController@selectAllSellInactiveByDate');
Route::post('/selectSellWaiterByDate', 'ReportController@selectSellWaiterByDate');
Route::post('/selectSellByBoard', 'ReportController@selectSellByBoard');
/*Estatistica*/
Route::get('/quantityWaiter', 'ReportController@quantityWaiter');
Route::get('/quantityBoard', 'ReportController@quantityBoard');
Route::get('/quantityItem', 'ReportController@quantityItem');
Route::get('/quantitySellOpen', 'ReportController@qtdSellOpen');
Route::get('/quantitySellClose', 'ReportController@qtdSellClose');
Route::get('/valueAllSell', 'ReportController@valueAllSell');
Route::get('/countClient', 'ReportController@countClient');
Route::get('/countUser', 'ReportController@countUser');
/*
 *Rotas para Relatorios
 */

/*
 *Rotas para Client
 */
Route::post('/saveClient', 'ClientController@store');
Route::get('/getClient', 'ClientController@findAll');
Route::post('/updateDept', 'ClientController@updateDept');
/*
 *Rotas para Client

/*
 *Rotas para Client
 */
Route::post('/saveSellClient', 'SellClientController@store');
Route::get('/getAllSellByClient', 'SellClientController@getAllSellByClient');

/*
 *Rotas para Client

/*
 * Novas Rotas (teste)
 */

/*
 *Rotas para Servicos
 */
Route::post('/saveService', 'ServiceController@store');
Route::get('/getServiceActivated', 'ServiceController@getServiceActivated');
Route::get('/getServiceDisabled', 'ServiceController@getServiceDisabled');
Route::post('/updateService/{id}', 'ServiceController@updateStatus');
Route::post('/updateServiceObject/{id}', 'ServiceController@updateServiceObject');

/*
 *Rotas para Client


/*
 * Gerar a comanda pelo o id do pedido
 */
Route::get('/comanda/{id}', 'ViewController@extrato')->name('extrato');

/*
 * Rota para estabelecimento
 */
Route::post('/establishmentSave', 'EstablishmentController@saveUpdate');
Route::post('/establishmentUpdate/{id}', 'EstablishmentController@saveUpdate');

/*
 * rotas de configuracoes
 */
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('key:generate');
    // return what you want
});


Auth::routes();
Route::get('/home', 'ViewController@index')->name('home');
Route::get('/', 'ViewController@index')->name('home');
Route::get('/mesas', 'ViewController@mesas')->name('mesas');
Route::get('/garcom', 'ViewController@garcom')->name('garcom');
Route::get('/produto', 'ViewController@produto')->name('produto');
Route::get('/estabelecimento', 'ViewController@estabelecimento')->name('estabelecimento');
Route::get('/usuario', 'ViewController@usuario')->name('usuario');
Route::get('/venda_caixa', 'ViewController@vendaCaixa')->name('venda_caixa');
Route::get('/pedidos_abertos', 'ViewController@pedidosAbertos')->name('pedidos_abertos');
Route::get('/pedidos_fechados', 'ViewController@pedidosFechados')->name('pedidos_fechados');
Route::get('/fatura', 'ViewController@fatura')->name('fatura');
Route::get('/balanco', 'ViewController@balanco')->name('balanco');
Route::get('/pedidos_mesa', 'ViewController@pedidoMesa')->name('pedidos_mesa');
Route::get('/fatura_mesa', 'ViewController@faturaMesa')->name('fatura_mesa');
Route::get('/cliente', 'ViewController@cliente')->name('cliente');
Route::get('/clientes_pendente_pagamento', 'ViewController@clientePendentePagamento')->name('cliente_pendente_pagamento');
Route::get('/servicos', 'ViewController@servicos')->name('servicos');
