/**
 * Created by jsj on 21/03/2017.
 */

var tools = {

    caminho_view: 'view/',

    carregarHtml: function (alvo, html) {
        $(alvo).load(tools.caminho_view+html);
    },

    chamarHtml: function () {
        $('.chamar_pagina').click(function (e) {
            e.preventDefault();
            var html = $(this).attr('href');
            console.log(html);
            tools.carregarHtml($('#conteudo'), html);
        });
    },
    
    inserirMascaraCpf: function () {
        $(".cpf").mask("999.999.999-99");
    }
};