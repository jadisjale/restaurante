/**
 * Created by jsj on 21/03/2017.
 */
var proprietario = {

    validarFormulario: function () {
        var validator = $('#formCadastroProprietario').validate({

            rules: {
                nomeProprietario: {
                    required: true,
                    minlength: 3
                },
                estadoCivilProprietario: {
                    required: true
                },
                profissaoProprietario: {
                    required: true,
                    minlength: 3
                },
                dataExpedicaoProprietario: {
                    required: true
                },
                cpfProprietario: {
                    required: true
                },
                domiciliadoProprietario: {
                    required: true
                },
                estadoProprietario: {
                    required: true
                },
                bairro: {
                    cidadeProprietario: true
                },
                identidadeProprietario: {
                    required: true
                },
                orgaoExpedidorProprietario: {
                    required: true
                },
                cidadeProprietario: {
                    required: true
                },
                bairroProprietario: {
                    required: true
                },
            },

            submitHandler: function( form ) {
                proprietario.save();
                return false;
            }

        });
    },

    save: function () {
        console.log('chamar metodo salvar');
    },

};
