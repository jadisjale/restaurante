/**
 * Created by jsj on 23/07/2017.
 */
var index = {

    message_erro_default: ' ERRO AO CONSULTAR DADOS',

    index: function () {
        index.quantityWaiter();
        index.quantityBoard();
        index.quantityItem();
        index.quantitySellClose();
        index.valueAllSell();
        index.countClient();
        index.countUser();

        Number.prototype.formatMoney = function(c, d, t){
            var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };
    },

    quantityWaiter: function () {
        tool.ajax(url.report.quantityWaiter, null, url.methods.GET, function (result) {
            if (result.code === true) {
                $('#waiter').html(result.data);
            } else {
                tool.messageToast('error', index.message_erro_default);
            }
        });
    },

    quantityBoard: function () {
        tool.ajax(url.report.quantityBoard, null, url.methods.GET, function (result) {
            if (result.code === true) {
                $('#board').html(result.data);
            } else {
                tool.messageToast('error', index.message_erro_default);
            }
        });
    },

    quantityItem: function () {
        tool.ajax(url.report.quantityItem, null, url.methods.GET, function (result) {
            if (result.code === true) {
                $('#item').html(result.data);
            } else {
                tool.messageToast('error', index.message_erro_default);
            }
        });
    },

    quantitySellClose: function () {
        tool.ajax(url.report.quantitySellClose, null, url.methods.GET, function (result) {
            if (result.code === true) {
                $('#sell').html(result.data);
            } else {
                tool.messageToast('error', index.message_erro_default);
            }
        });
    },

    valueAllSell: function () {
        tool.ajax(url.report.valueAllSell, null, url.methods.GET, function (result) {
            if (result.code === true) {
                if (result.data[0].sum !== null) {
                    var value = parseFloat(result.data[0].sum).formatMoney(2, ',', '.');
                    $('#dinheiro').html(value);
                }
            } else {
                tool.messageToast('error', index.message_erro_default);
            }
        });
    },


    countClient: function () {
        tool.ajax(url.report.countClient, null, url.methods.GET, function (result) {
            if (result.code === true) {
                $('#client').html(result.data);
            } else {
                tool.messageToast('error', index.message_erro_default);
            }
        });
    },

    countUser: function () {
        tool.ajax(url.report.countUser, null, url.methods.GET, function (result) {
            if (result.code === true) {
                $('#user').html(result.data);
            } else {
                tool.messageToast('error', index.message_erro_default);
            }
        });
    },


};