/**
 * Created by jsj on 23/07/2017.
 */

var url = {

    methods: {
        GET: 'GET',
        POST: 'POST'
    },

    board: {
        saveBoard: '/saveBoard',
        getBoardActivated: '/getBoardActivated',
        getBoardDisabled: '/getBoardDisabled',
        updateBoard: '/updateBoard/',
        getBoardsBySell: '/getBoardsBySell/',
        getBoardsBySellActive: '/getBoardsBySellActive/'
    },

    waiter: {
        saveWaiter: '/saveWaiter',
        getWaiterActivated: '/getWaiterActivated',
        getWaiterDisabled: '/getWaiterDisabled',
        updateWaiter: '/updateWaiter/'
    },

    item: {
        saveItem: '/saveItem',
        getAllItem: '/getAllItem',
        findById: '/findById/',
        updateItem: '/updateItem/',
        getItensActivated: '/getItensActivated/',
        getItensDisabled: '/getItensDisabled/',
        updateItemStatus: '/updateItemStatus/',
        itemAuto: '/itemAuto'
    },

    user: {
        saveUser: '/saveUser',
        getAllUser: '/getAllUser'
    },

    group: {
        saveGroup: '/saveGroup'
    },

    sellItem: {
        saveSellItem: '/saveSellItem',
        detailsSell: '/detalhes_venda/',
        remove_item: '/remove_item/',
        getBoardsBySell: '/getBoardsBySell/',
        checkIfSellActive: '/checkIfSellActive/',
        updateCountBoards: '/updateCountBoards',
        removeItem: '/removeItem'
    },

    sell: {
        countSellActive: '/countSellActive',
        getSellActive: '/getSellActive',
        getSellInactive: '/getSellInactive',
        updatePrice: '/updatePrice',
        finishSell: '/finishSell/',
        pedidos_detalhes: '/pedidos_detalhes',
        deleteSell: '/deleteSell/'
    },

    report: {
        selectAllSellInactiveByDate: '/selectAllSellInactiveByDate',
        selectSellWaiterByDate: '/selectSellWaiterByDate',
        selectSellByBoard: '/selectSellByBoard',
        /*Estatística*/
        quantityWaiter: '/quantityWaiter',
        quantityBoard: '/quantityBoard',
        quantityItem: '/quantityItem',
        quantitySellClose: '/quantitySellClose',
        valueAllSell: '/valueAllSell',
        countClient: '/countClient',
        countUser: '/countUser'
    },

    client: {
        saveClient: '/saveClient',
        getClient: '/getClient',
        updateDept: '/updateDept'
    },

    sellClient: {
        saveSellClient: '/saveSellClient',
        getAllSellByClient: '/getAllSellByClient'
    },

    establishment: {
        saveEstablishment: '/establishmentSave',
        findById: '/findById/'
    },

    pages: {

        board: '/mesas',
        product: '/produto',
        establishment: '/estabelecimento',
        waiter: '/garcom',
        client: '/cliente',
        sellBox: '/venda_caixa',
        requests: '/pedidos',
        openOrders: '/pedidos_abertos',
        closedOrders: '/pedidos_fechados',
        dateInvoice: '/fatura',
        billTable: '/fatura_mesa',
        balance: '/balanco',
        customersPendingPayment: '/clientes_pendente_pagamento',
        service: '/servicos'
    },

    service: {
        save: '/saveService',
        getServiceActivated: '/getServiceActivated',
        getServiceDisabled: '/getServiceDisabled',
        updateService: '/updateService/',
        updateServiceObject: '/updateServiceObject/'
    }

};