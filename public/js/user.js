/**
 * Created by jsj on 23/07/2017.
 */
var user = {

    form: '#save_user',
    tableActive: null,
    tableDisabled: null,
    btn_submit: 'btn-submit',

    index: function () {
        user.validador();
        // user.loadUserActive();
        // user.loadUserDisabled();
        user.drawTables();
    },

    saveUser: function () {
        tool.disabled(user.btn_submit);
        tool.ajax(url.user.saveUser, $(user.form).serialize(), url.methods.POST, function (result) {
            if (result.code === true) {
                tool.messageToast('success', result.data.name + ' CRIADO COM SUCESSO!');
                tool.resetForm(user.form);
                //user.tableActive.ajax.reload();
                //user.tableDisabled.ajax.reload();
            } else {
                tool.messageToast('error', result.message  + ': ' + result.data.toString().replace(',', ' '));
            }
            tool.enabled(user.btn_submit);
        });
    },

    validador : function() {
        $(user.form).validate({
            rules: {
                name: {
                    required: true,
                    minlength: 3
                },
                login: {
                    required: true,
                    minlength: 3
                },
                password: {
                    required: true
                },
            },
            submitHandler: function() {
                user.saveUser();
                //console.log($(user.form).serialize());
                //console.warn($(user.form).serialize());
                return false;
            }
        });
    },
    
   /** loadUserActive: function () {
        user.tableActive = $('#user-active').DataTable({

            "bPaginate": false,
            "bFilter": false,
            "bInfo": false,

            "language": {
                "url": "js/Portuguese-Brasil.json"
            },
            "ajax": {
                "url":url.user.getUserActivated,
                "dataSrc": "data"
            },
            "columns": [
                {"data": "name"},
                {"data": "id"}
            ],
            columnDefs: [
                {
                    render: function (data) {
                        return "<a href= '" + data + "' class='update-disabled'><span class='label label-danger'>Desativar</span></a>";
                    },
                    targets: 1,
                    "searchable": false,
                    className: "dt-body-center"
                },
                {orderable: false, targets: [1]}
            ]
        });

        user.tableActive.on('click', '.update-disabled', function (e) {
            e.preventDefault();
            var id = $(this).attr('href');
            user.update(id, 0);
        });
    },

    loadUserDisabled: function () {
        user.tableDisabled = $('#user-disabled').DataTable({

            "bPaginate": false,
            "bFilter": false,
            "bInfo": false,

            "language": {
                "url": "js/Portuguese-Brasil.json"
            },
            "ajax": {
                "url": url.user.getUserDisabled,
                "dataSrc": "data"
            },
            "columns": [
                {"data": "description"},
                {"data": "id"}
            ]
        });

        user.tableDisabled.on('click', '.update-active', function (e) {
            e.preventDefault();
            var id = $(this).attr('href');
            user.update(id, 1);
        });
    }, */

    drawTables: function () {
        $('#li-active').click(function () {
            //user.tableActive.ajax.reload();
        });
        $('#li-disabled').click(function () {
            //user.tableDisabled.ajax.reload();
        });
    },

    update: function (id, status) {
        var data = {'status': status};
        tool.ajax(url.user.updateUser+id, data, url.methods.POST, function (result) {
            if (result.code === true) {
                tool.messageToast('success', 'USUÁRIO ATUALIZADO COM SUCESSO!');
                user.tableActive.ajax.reload();
                user.tableDisabled.ajax.reload();
            } else {
                tool.messageToast('error', result.message  + ': NÃO FOI POSSÍVEL ATUALIZAR O USUÁRIO');
            }
        });
    }
};