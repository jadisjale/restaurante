/**
 * Created by jsj on 23/07/2017.
 */
var fatura = {

    form: '#form-fatura',
    table: '#table_fatura',
    $table_object: null,
    value_all: '',
    table_waiter: null,

    run: function () {
        $(fatura.form).submit();
    },

    fatura: {
        index: function () {
            fatura.calendar();
            fatura.validatorFatura();
            fatura.loadTableFatura();
            fatura.run();
        }
    },

    balanco: {
        index: function () {
            fatura.calendar();
            fatura.validatorBalanco();
            fatura.loadTableBalanco();
            fatura.run();
            fatura.table_waiter = $('#table_waiters').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        header: true,
                        footer: true,
                        extend: 'print',
                        text:'<i class="fa fa-print" aria-hidden="true"></i> IMPRIMIR',
                        exportOptions: {
                            columns: [0, 1, 2 ]
                        },
                        customize: function (win) {
                            $(win.document.body).find('table').addClass('display').css('font-size', '15px');
                            $(win.document.body).find('tr:nth-child(odd) td').each(function(index){
                                $(this).css('background-color','#D0D0D0');
                            });
                            $(win.document.body).find('h1').css('text-align','center');
                        }
                    }
                ],
                "language": {
                    "url": "js/Portuguese-Brasil.json"
                }
            });
        }
    },

    board: {
        index: function() {
            fatura.calendar();
            fatura.validatorBoard();
            fatura.loadTableBoard();
            fatura.run();
        }
    },

    calendar: function () {
        $('#date').daterangepicker({
            timePicker: false,
            timePickerIncrement: 30,
            locale: {
                format: 'DD/MM/YYYY',
                applyLabel: 'APLICAR',
                cancelLabel: 'CANCELAR'
            }
        });
    },

    convertDateBrToDefault: function () {

        var date = $('#date').val();
        date = date.split('-');

        var hora_inicio = '00:00:00';
        var hora_fim = '24:00:00';
        var data_inicio = date[0].trim();
        var data_fim = date[1].trim();

        data_inicio = formartDate(data_inicio);
        data_fim = formartDate(data_fim);

        data_inicio = data_inicio + " " + hora_inicio;
        data_fim = data_fim + " " + hora_fim;

         return {'data_inicio': data_inicio, 'data_fim': data_fim};

        function formartDate (date) {
            date = date.split('/');
            var dia = date[0];
            var mes = date[1];
            var ano = date[2];

            return ano + '-' + mes + '-' + dia;
        }
    },

    validatorBalanco : function() {
        $(fatura.form).validate({
            rules: {
                date: {
                    required: true
                }
            },
            submitHandler: function() {
                fatura.selectSellWaiterByDate();
                return false;
            }
        });
    },

    validatorFatura: function() {
        $(fatura.form).validate({
            rules: {
                date: {
                    required: true
                }
            },
            submitHandler: function() {
                fatura.getDataFatura();
                return false;
            }
        });
    },

    validatorBoard: function() {
        $(fatura.form).validate({
            rules: {
                date: {
                    required: true
                }
            },
            submitHandler: function() {
                fatura.getDataBoard();
                return false;
            }
        });
    },

    getDataFatura: function () {
        $('#btn-adicionar').prop('disabled', true);
        tool.ajax(url.report.selectAllSellInactiveByDate, fatura.convertDateBrToDefault(), url.methods.POST, function (result) {
            if (result.code === true) {
                fatura.value_all = fatura.addItenFatura(result.data);
            } else {
                tool.messageToast('error', 'ERRO AO CRIAR UM RELATÓRIO');
            }
            $('#btn-adicionar').prop('disabled', false);
        });
    },

    getDataBoard: function () {
        $('#btn-adicionar').prop('disabled', true);
        tool.ajax(url.report.selectSellByBoard, fatura.convertDateBrToDefault(), url.methods.POST, function (result) {
            if (result.code === true) {
                var data = result.data;
                var array_reduce = data.reduce(function(field, e1) {
                    var boards = '';
                    $.each(data, function(key, value) {
                        if (e1.id === value.id) {
                            boards = boards + value.description.toString() + ', ';
                        }
                    });
                    boards = boards.substring(0,boards.length - 2);
                    var matches = field.filter(function(e2){return e1.id === e2.id});
                    if (matches.length === 0) {
                        var item = $.extend({}, e1, {'boards': boards});
                        field.push(item);
                    }
                    return field;
                }, []);

                fatura.addItenBoard(array_reduce);

            } else {
                tool.messageToast('error', 'ERRO AO CRIAR UM RELATÓRIO');
            }
            $('#btn-adicionar').prop('disabled', false);
        });
    },

    selectSellWaiterByDate: function () {
        $('#btn-consultar').prop('disabled', true);
        tool.ajax(url.report.selectSellWaiterByDate, fatura.convertDateBrToDefault(), url.methods.POST, function (result) {
            if (result.code === true) {
                fatura.addItenBalanca(result.data);
            } else {
                tool.messageToast('error', 'ERRO AO CRIAR UM RELATÓRIO');
            }
            $('#btn-consultar').prop('disabled', false);
        });
    },

    loadTableFatura: function () {
        fatura.$table_object = $(fatura.table).DataTable({
            "language": {
                "url": "js/Portuguese-Brasil.json"
            },
            dom: 'Bfrtip',
            buttons: [
                {
                    header: true,
                    footer: true,
                    extend: 'print',
                    text:'<i class="fa fa-print" aria-hidden="true"></i> IMPRIMIR',
                    exportOptions: {
                        columns: [0, 1, 2 ]
                    },
                    customize: function (win) {
                        $(win.document.body).find('table').addClass('display').css('font-size', '15px');
                        $(win.document.body).find('tr:nth-child(odd) td').each(function(index){
                            $(this).css('background-color','#D0D0D0');
                        });
                        $(win.document.body).find('h1').css('text-align','center');
                    }
                }
            ]

        });
    },

    loadTableBalanco: function () {
        fatura.$table_object = $(fatura.table).DataTable({
            "language": {
                "url": "js/Portuguese-Brasil.json"
            },
            dom: 'Bfrtip',
            buttons: [
                {
                    header: true,
                    footer: true,
                    extend: 'print',
                    text:'<i class="fa fa-print" aria-hidden="true"></i> IMPRIMIR',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4]
                    },
                    customize: function (win) {
                        $(win.document.body).find('table').addClass('display').css('font-size', '15px');
                        $(win.document.body).find('tr:nth-child(odd) td').each(function(index){
                            $(this).css('background-color','#D0D0D0');
                        });
                        $(win.document.body).find('h1').css('text-align','center');
                    }
                }
            ]

        });
    },

    loadTableBoard: function () {
        fatura.$table_object = $(fatura.table).DataTable({
            "language": {
                "url": "js/Portuguese-Brasil.json"
            },
            dom: 'Bfrtip',
            buttons: [
                {
                    header: true,
                    footer: true,
                    extend: 'print',
                    text:'<i class="fa fa-print" aria-hidden="true"></i> IMPRIMIR',
                    exportOptions: {
                        columns: [0, 1, 2, 3]
                    },
                    customize: function (win) {
                        $(win.document.body).find('table').addClass('display').css('font-size', '15px');
                        $(win.document.body).find('tr:nth-child(odd) td').each(function(index){
                            $(this).css('background-color','#D0D0D0');
                        });
                        $(win.document.body).find('h1').css('text-align','center');
                    }
                }
            ]

        });
    },

    addItenFatura: function (data) {
        fatura.$table_object.clear().draw();
        Number.prototype.formatMoney = function(c, d, t){
            var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };
        var value_total = 0;
        $.each(data, function (k, v) {
            value_total = value_total + (parseFloat(v.value_all));
            fatura.$table_object.row.add([
                v.id,
                'R$ ' + parseFloat(v.value_all).formatMoney(2, ',', '.'),
                tool.formatDateHour(v.created_at),
                "<a href='/pedidos_fechados_detalhes?sell_id="+v.id+"' class='editar center'><span class='label label-primary'>VISUALIZAR</span></a>"
            ]).draw( false );
        });
        value_total = 'R$ ' + parseFloat(value_total).formatMoney(2, ',', '.');
        $('#value_all').text(value_total);
        if (value_total) {
            document.title = 'VALOR TOTAL DO PEDIDO: ' + value_total + '. PERÍODO: ' + $('#date').val();
        } else {
            document.title = 'RESTAURANTE FATURA';
        }
        return value_total;
    },

    addItenBalanca: function (data) {

        fatura.$table_object.clear().draw();
        Number.prototype.formatMoney = function(c, d, t){
            var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };

        var list = [];
        var value_all = 0;

        data = fatura.formatData(data);

        $.each(data, function (k, v) {
            list.push(getValuesSum(data, v.name));
            v.value_item = parseFloat(v.value_item);
            value_all += (v.value_item * v.count);
            var valor_unit = v.value_item;
            v.value_item = v.count * v.value_item;

            fatura.$table_object.row.add([
                v.sell_id,
                tool.formatDateHour(v.created_at),
                v.description,
                v.count,
                'R$ ' + valor_unit.formatMoney(2, ',', '.'),
                'R$ ' + v.value_item.formatMoney(2, ',', '.'),
                v.name
            ]).draw( false );
        });

        var waiters = [];

        $.each(list, function(i, el) {
            if (!list[el.name]) {
                list[el.name] = true;
                waiters.push(el);
            }
        });

        function getValuesSum (data, name) {
            var sum = 0;
            $.each(data, function(index, value) {
                var capacity = value.value_item * value.count;
                capacity = parseFloat(capacity);
                var company = value.name;
                if (company === name) {
                    sum += capacity;
                }
            });
            var por = sum;
            por = parseFloat(por) * 0.10;
            por = 'R$ ' + por.formatMoney(2, ',', '.');
            sum = 'R$ ' + parseFloat(sum).formatMoney(2, ',', '.');
            return {'name':name, 'value_all': sum, 'por': por};
        }

        value_all = 'R$ ' + parseFloat(value_all).formatMoney(2, ',', '.');
        $('#value_all').text(value_all);

        if (value_all !== 'R$ 0,00') {
            document.title = 'VALOR TOTAL DO PERÍODO: ' + value_all + '. PERÍODO: ' + $('#date').val();
        } else {
            document.title = 'RESTAURANTE BALANÇO';
        }

        fatura.showModalBalanco(waiters);
    },

    addItenBoard: function (data) {

        fatura.$table_object.clear().draw();
        Number.prototype.formatMoney = function(c, d, t){
            var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };

        var value_all = 0;

        $.each(data, function (k, v) {
            v.value_all = parseFloat(v.value_all);
            value_all += v.value_all;
            fatura.$table_object.row.add([
                v.id,
                v.boards,
                'R$ ' + v.value_all.formatMoney(2, ',', '.'),
                tool.formatDateHour(v.created_at),
                "<a href='/pedidos_fechados_detalhes?sell_id="+v.id+"' class='editar center'><span class='label label-primary'>VISUALIZAR</span></a>"
            ]).draw( false );
        });

        value_all = 'R$ ' + parseFloat(value_all).formatMoney(2, ',', '.');
        $('#value_all').text(value_all);

        if (value_all !== 'R$ 0,00') {
            document.title = 'VALOR TOTAL DO PERÍODO: ' + value_all + '. PERÍODO: ' + $('#date').val();
        } else {
            document.title = 'RESTAURANTE FATURA POR MESA';
        }
    },


    showModalBalanco: function (waiters) {
        $('#result_waiters').fadeIn(500);
        fatura.showResultWaiter(waiters);
    },

    showResultWaiter: function (data) {
        $('#result_waiters').click(function () {
            $('#modal_balaco').modal('show');
            fatura.table_waiter.clear().draw();
            $.each(data, function (k, v) {
                console.log(data);
                fatura.table_waiter.row.add( [
                    v.name,
                    v.value_all,
                    v.por
                ]).draw( false );
            });
        });
    },

    formatData: function (data) {

        var result = [];
        var json = data;
        $.each(json, function (k, v) {
            var count = 1;
            var item;
            $.each(json, function (key, value) {
                if (k !== key) {
                    if (comparar(v, value) === true) {
                        count++;
                    }
                }
            });
            item = $.extend({}, v, {'count': count});
            result.push(item);
        });

        function comparar(data, espelho) {
            if (data.created_at === espelho.created_at && data.description === espelho.description && data.id_item === espelho.id_item && data.id_waiter === espelho.id_waiter && data.name === espelho.name && data.sell_id === espelho.sell_id && data.value_item === espelho.value_item) {
                return true;
            } else {
                return false;
            }
        }

        function unique(a) {
            var isAdded,
                arr = [];
            for(var i = 0; i < a.length; i++) {
                isAdded = arr.some(function(v) {
                    return isEqual(v, a[i]);
                });
                if( !isAdded ) {
                    arr.push(a[i]);
                }
            }
            return arr;
        }

        function isEqual(a, b) {
            var prop;
            for( prop in a ) {
                if ( a[prop] !== b[prop] ) return false;
            }
            for( prop in b ) {
                if ( b[prop] !== a[prop] ) return false;
            }
            return true;
        }

        return unique(result);

    },

};