/**
 * Created by jsj on 13/11/2017.
 */
var establishment = {

    form: '#save_establishment',
    btn_submit: 'btn-submit',
    btn_submit_update: 'btn-update-value',

    id: null,
    name: null,
    state: null,
    city: null,
    street: null,
    number: null,
    phone: null,


    index: function () {
        establishment.validador();
        $('#update-name').focus();
        // establishment.buttonUpdateValue();
    },

    updateValue: function (name, state, city, street, number, phone) {
        var data = {
            'name': name,
            'state': state,
            'city': city,
            'street': street,
            'number': number,
            'phone': phone
        };
        tool.disabled(establishment.btn_submit_update);
        tool.ajax(url.establishment.updateEstablishment+establishment.id, data, url.methods.POST, function (result) {
            if (result.code === true) {
                tool.messageToast('success', ' ESTABELECIMENTO ALTERADO COM SUCESSO');
                $('#update-value').val('');
                $(establishment.modal).modal('hide');
                establishment.reloadTable();
            } else {
                tool.messageToast('error', result.message  + ': ' + result.data.toString().replace(',', ' '));
            }
            tool.enabled(establishment.btn_submit_update);
        });
    },

    saveEstablishment: function () {
        var value = $('#value').val();
        value = value.split(' ')[1];
        var data = {
            'name': $('#name').val(),
            'state': $('#state').val(),
            'city': $('#city').val(),
            'street': $('#street').val(),
            'number': $('#number').val(),
            'phone': $('#phone').val()
        };
        tool.disabled(establishment.btn_submit);
        tool.ajax(url.establishment.saveEstablishment, data, url.methods.POST, function (result) {
            if (result.code === true) {
                tool.messageToast('success', result.data.name + ' CRIADO COM SUCESSO!');
                tool.resetForm(establishment.form)
                item.reloadTable();
            } else {
                tool.messageToast('error', result.message  + ': ' + result.data.toString().replace(',', ' '));
            }
            tool.enabled(establishment.btn_submit);
        });
    },

    findById: function (id) {
        tool.ajax(url.establishment.findById+id, null, url.methods.POST, function (result) {
            if (result.code === true) {
                $(establishment.modal).modal('show');
                establishment.id = result.data.id;
                establishment.name = result.data.name;
                establishment.state = result.data.state;
                establishment.city = result.data.city;
                establishment.street = result.data.street;
                establishment.phone = result.data.phone;
            } else {
                tool.messageToast('error', result.message  + ': NÃO FOI POSSÍVEL LOCALIZAR ESTE ESTABELECIMENTO' );
            }
        });
    },

    validador : function() {
        $(establishment.form).validate({
            rules: {
                establishment: {
                    required: true,
                    minlength: 3
                },
                state: {
                    required: true,
                    minlength: 2
                },
                city: {
                    minlength: 1
                },
                street: {
                    required: true,
                    minlength: 1
                },
                number: {
                    minlength: 1
                },
                phone: {
                    minlength: 8
                }
            },
            submitHandler: function() {
                establishment.save();
                return false;
            }
        });
    },

    buttonUpdateValue: function () {
        $("#"+establishment.btn_submit_update).click(function () {
            var name = $('#update-name').val();
            var state = $('#update-state').val();
            var city = $('#update-city').val();
            var street = $('#update-street').val();
            var number = $('#update-number').val();
            var phone = $('#update-phone').val();
            if (name !== '' && state !== '' && city !== '' && street !== '' && number !== '' && phone !== '') {
               establishment.updateValue(name, state, city, street, number, phone);
            } else {
               tool.messageToast('error', 'TODOS OS CAMPOS OBRIGATÓRIOS');
            }
        });
    },

    save: function () {
        tool.disabled(establishment.btn_submit);
        tool.ajax(url.establishment.saveEstablishment, $(establishment.form).serialize(), url.methods.POST, function (result) {
            if (result.code === true) {
                tool.messageToast('success', 'ATUALIZADO COM SUCESSO!');
                // tool.resetForm(establishment.form);
            } else {
                tool.messageToast('error', result.message  + ': ' + result.data.toString().replace(',', ' '));
            }
            tool.enabled(establishment.btn_submit);
        });
    }
};