/**
 * Created by jsj on 23/07/2017.
 */
var sell = {

    select_boards: '#boards',
    clients: '#client_id',
    select_item: '#id_item_active',
    select_waiter: '#id_waiter',
    form_item_sell: '#form-item-pedido',
    form_item_sell_box: '#form-sell-box',
    count_item: '#count_item',
    table_sell_item: null,

    modal_sell_client: "#modal-sell-client",
    modal_save_client: "#modal-save-client",

    value_static: null,

    itens : {
      id_item: null,
      name_item: null,
      id_waiter: null,
      name_waiter: null,
      count_item: null ,
      value_item: null,
      value_total: null
    },

    index: function () {
        sell.checkId();
        sell.loadWaiterActive();
        sell.validadorItemSell();
        sell.checkIfSellActive();
        sell.finishSell();
        sell.updateQTD();
        sell.updateBoards();
        sell.getClient();
        sell.modalSellClient();
        sell.ajaxSellClient();
        sell.modallAddClient();
        sell.ajaxSaveClient();
        sell.validSellBox();
        // sell.loadData();
    },

    updateQTD: function () {
        sell.table_sell_item.on('click', '.remove_item', function (e) {
            e.preventDefault();
            var tr = $(this).parent('td').parent('tr');
            var d = sell.table_sell_item.row( tr ).data();
            var item_id = d[5];
            var sell_id = $(this).attr('href');
            sell.confirmDelete(sell_id, item_id, tr);
        });
    },

    confirmDelete: function (sell_id, item_id, tr) {
        $.confirm({
            text: "REALMENTE DESEJA REMOVER ESSE ITEM?",
            title: "CONFIRMAÇÃO REQUERIDA",
            confirm: function() {
                sell.removeItem(sell_id, item_id, tr);
            },
            cancel: function() {
            },
            confirmButton: "SIM EU QUERO",
            cancelButton: "NÃO",
            confirmButtonClass: "btn-primary",
            cancelButtonClass: "btn-danger",
            dialogClass: "modal-dialog"
        });
    },

    removeItem: function (sell_id, item_id, tr) {
        var data = {
            'sell_id': sell_id,
            'item_id': item_id
        };
        tool.ajax(url.sellItem.removeItem, data, url.methods.POST, function (result) {
            if (result.code === true) {
                tool.messageToast('success', 'PRODUTO RETIRADO DO PEDIDO COM SUCESSO');
                sell.table_sell_item.clear().draw();
                sell.loadData();
            } else {
                tool.messageToast('error', 'ERRO AO REMOVER PRODUTO DO PEDIDO');
            }
        });
    },

    loadBoardsActive : function(){
        tool.ajax(url.board.getBoardActivated, null, url.methods.GET, function (result) {
            $(sell.select_boards).html('');
            var cmb = "";
            $.each(result.data, function(i, value) {
                cmb = cmb + '<option value="' + value.id + '">'
                    + value.description + '</option>';
            });
            $(sell.select_boards).append(cmb);
        });
    },

    loadWaiterActive : function(){
        tool.ajax(url.waiter.getWaiterActivated, null, url.methods.GET, function (result) {
            $(sell.select_waiter).html();
            var cmb = "<option value='' selected>SELECIONE UM GARÇOM</option>";
            $.each(result.data, function(i, value) {
                cmb = cmb + '<option value="' + value.id + '">'
                    + value.name + '</option>';
            });
            $(sell.select_waiter).append(cmb);
        });
    },

    update: function (id, status) {
        var data = {'status': status};
        tool.ajax(url.board.updateBoard+id, data, url.methods.POST, function (result) {
            //implementar
        });
    },

    loadItensActive: function () {
        $(sell.select_item).select2({
            language: "pt-BR",
            allowClear: true,
            placeholder: 'INFORME O NOME DO PRODUTO',
            ajax: {
                url: url.item.itemAuto,
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });
    },

    validadorItemSell : function() {
        $(sell.form_item_sell).validate({
            rules: {
                id_item_active: {
                    required: true
                },
                count_item: {
                    required: true
                },
                id_waiter: {
                    required: true
                },
                boards: {
                    required: true
                }
            },
            submitHandler: function() {
                sell.saveSellItem();
                return false;
            }
        });
    },

    initTableSellItem: function () {
        sell.table_sell_item = $('#list_item_sell').DataTable({
            "language": {
                "url": "js/Portuguese-Brasil.json"
            }
        });
    },

    saveSellItem: function () {
        $('#btn-adicionar').prop('disabled', true);
        tool.ajax(url.sellItem.saveSellItem, $(sell.form_item_sell).serialize(), url.methods.POST, function (result) {
            if (result.code === true) {
                sell.itens.id_item = result.data;
                $('#id_sell').val(result.data);
                $('#count_item').val(1);
                // $("#boards").prop("disabled", true);
                tool.messageToast('success', 'PRODUTO ADICIONADO AO PEDIDO');
                sell.countSellActive();
                $('.sell_id').fadeIn(500);
                $('#sell_id').text(sell.itens.id_item);
                sell.table_sell_item.clear().draw();
                sell.loadData();
                $('.hide-to-modal').fadeIn(500);
                $('#update_board').fadeIn(500);

                $('#id_item_active').html('<option value="" selected>Selecione uma opção</option>');

            } else {
                tool.messageToast('error', 'ERRO AO CRIAR PEDIDO E ADICIONAR PRODUTO');
                console.log(result);
            }
            $('#btn-adicionar').prop('disabled', false);
        });
    },

    loadTableItens: function () {

        selectOption();

        function selectOption() {

            var url = $(location).attr('href');
            url = url.split('/');
            url = url[3];
            url = url.split('?');

            if (url[0] === "pedidos_detalhes") {
                loadOption2();
                sell.loadData();
                $('#update_board').fadeIn(500);
                $('.hide-to-modal').fadeIn(500);
            } else if (url[0] === "pedidos_fechados_detalhes") {
                loadOption1();
                sell.loadData();
                $('#boards').prop('disabled', true);
                $('#finish').css('display', 'none');
            } else {
                loadOption2();
                $('#finish').attr("disabled", true);
            }
        }

        function loadOption1() {
            sell.table_sell_item = $('#list_item_sell').DataTable({

                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'print',
                        message: $('.sell_id').text(),
                        text:'<i class="fa fa-print" aria-hidden="true"></i> IMPRIMIR',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        },
                        customize: function (win) {
                            $(win.document.body).find('table').addClass('display').css('font-size', '15px');
                            $(win.document.body).find('tr:nth-child(odd) td').each(function(index){
                                $(this).css('background-color','#D0D0D0');
                            });
                            $(win.document.body).find('h1').css('text-align','center');
                        }
                    }
                ],
                columnDefs: [
                    {
                        "targets": [4, 5],
                        "visible": false,
                        "searchable": false
                    }
                ],
                "language": {
                    "url": "js/Portuguese-Brasil.json"
                }
            });
        }

        function loadOption2() {
            sell.table_sell_item = $('#list_item_sell').DataTable({
                "language": {
                    "url": "js/Portuguese-Brasil.json"
                },
                "columnDefs": [
                    {
                        "targets": [ 5 ],
                        "visible": false,
                        "searchable": false
                    },
                ]
            });

            Number.prototype.formatMoney = function(c, d, t){
                var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
                return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
            };
        }
    },

    getValueTotal: function () {
        var value_total = 0;
        sell.table_sell_item.rows().every( function () {
            var d = this.data();
            var value_unt = d[3];
            value_unt = value_unt.split(' ');
            value_unt = tool.convertMoedaToFloat (value_unt[1].trim());
            value_total = value_total +  value_unt;
        });
        sell.itens.value_total = value_total;
        sell.itens.value_total = 'R$ ' + sell.itens.value_total.formatMoney(2, ',', '.');
        $('#value_total').text(sell.itens.value_total);
        sell.updatePrice(value_total);
        return sell.itens.value_total;
    },

    countSellActive: function () {
        tool.ajax(url.sell.countSellActive, null, url.methods.GET, function (result) {
            if (result.code === true) {
                $('#count_sell_active').text(result.data);
            } else {
                tool.messageToast('error', 'ERRO AO TENTAR CONTAR O NÚMERO DE PEDIDOS ABERTO');
            }
        });
    },

    updatePrice: function (value_total) {
        var data = {
            'id' : sell.itens.id_item,
            'value_all' : value_total
        };
        tool.ajax(url.sell.updatePrice, data, url.methods.POST, function (result) {
            if (result.code === false) {
                tool.messageToast('error', 'NÃO FOI POSSÍVEL ATUALIZAR O VALOR DO PEDIDO');
            }
        });
    },

    checkId: function () {
        var id = $('#id_sell').val();
        sell.itens.id_item = id ? id : null;
        if (sell.itens.id_item !== null) {
            sell.loadTableItens();
            $('.sell_id').fadeIn(500);
            sell.getBoardsBySell();
        } else {
            sell.loadBoardsActive();
            sell.loadTableItens();
        }
    },

    getBoardsBySell: function () {
        tool.ajax(url.board.getBoardsBySell+sell.itens.id_item, null, url.methods.GET, function (result) {
            if (result.code === true) {
                sell.autoSelectBoards(result.data);
                // $("#boards").prop("disabled", true);
            } else {
                tool.messageToast('error', 'NÃO FOI POSSÍVEL CARREGAR AS MESAS DOS PEDIDOS');
            }
        });
    },

    autoSelectBoards: function (data) {
        $(sell.select_boards).html();
        var cmb = "";
        var boards_selectd = [];
        $.each(data, function(i, value) {
            boards_selectd.push(value.id);
            cmb = cmb + '<option value="' + value.id + '" selected>'
                + value.description + '</option>';
        });
        $(sell.select_boards).append(cmb);

        tool.ajax(url.board.getBoardsBySellActive+sell.itens.id_item, null, url.methods.GET, function (result) {
            if (result.code === true) {
                var cmb = "";
                $.each(result.data, function(i, value) {
                    if(jQuery.inArray(value.id, boards_selectd) == -1) {
                        cmb = cmb + '<option value="' + value.id + '">'
                            + value.description + '</option>';
                    }
                });
                $(sell.select_boards).append(cmb);
            } else {
                tool.messageToast('error', 'ERRO AO CONSULTAR O RESTANTE DAS MESAS');
            }
        });
    },

    checkIfSellActive: function () {
        if (sell.itens.id_item !== null){
            tool.ajax(url.sellItem.checkIfSellActive+sell.itens.id_item, null, url.methods.GET, function (result) {
                if (result.code === true) {
                    if (result.data.status === false) {
                        $('#panel-iten').css('display', 'none');
                    }
                } else {
                    tool.messageToast('error', 'ERRO AO VERIFICAR SE O PEDIDO ESTÁ FINALIZADO');
                }
                // $('#panel-iten').css('display', 'none');
            });
        }
    },

    finishSell: function () {
        $('#finish').click(function () {
            $.confirm({
                text: "REALMENTE DESEJA FINALIZAR ESSE PEDIDO?",
                title: "CONFIRMAÇÃO REQUERIDA! APÓS CONFIRMAR O SISTEMA VOLTARÁ PARA A TELA ANTERIOR",
                confirm: function() {
                    sell.ajaxFinishSell();
                },
                cancel: function() {
                },
                confirmButton: "SIM EU QUERO",
                cancelButton: "NÃO",
                confirmButtonClass: "btn-primary",
                cancelButtonClass: "btn-danger",
                dialogClass: "modal-dialog"
            });
        });
    },

    ajaxFinishSell: function () {
        if(sell.itens.id_item !== null) {
            tool.ajax(url.sell.finishSell+sell.itens.id_item, null, url.methods.GET, function (result) {
                if (result.code === true) {
                    $(location).attr('href', 'pedidos_abertos');
                    tool.comanda(sell.itens.id_item);
                } else {
                    tool.messageToast('error', 'ERRO AO FINALIZAR O PEDIDO');
                }
            });
        } else {
            tool.messageToast('error', 'NÃO HÁ ITENS NOS PEDIDOS');
        }
    },

    loadData: function () {
        tool.ajax(url.sellItem.detailsSell+sell.itens.id_item, null, url.methods.GET, function (result) {
            if (result.code === true) {
                if (result.data.length === 0) {
                    sell.deleteSell();
                } else {
                    sell.formatData(result.data);
                }
            } else {
                tool.messageToast('error', 'ERRO AO CONSULTAR OS PRODUTOS DESSA VENDA');
            }
        });
    },

    formatData: function (data) {

        var array_reduce = data.reduce(function(field, e1) {
            var matches = field.filter(function(e2){return e1.description === e2.description});
            if (matches.length === 0) {
                field.push(e1);
            }
            return field;
        }, []);

        var array_merge = [];
        $.each(array_reduce, function (k, v) {
            if (v.description === array_reduce[k].description) {
                var count = getCountArrayByElement(data, v.description);
                var merge = $.extend({}, v, {'count': count});
                array_merge.push(merge);
            }
        });

        function getCountArrayByElement (array, value) {
            var keys = Object.keys(array);
            var result = keys.filter(function(v) {
                return array[v].description === value;
            });
            return result.length;
        }

        sell.addItem(array_merge);
    },
    
    addItem: function (array) {
        $.each(array, function (k, v) {
            var value_total = v.value_item * v.count;
            sell.table_sell_item.row.add( [
                v.description,
                'R$ ' + parseFloat(v.value_item).formatMoney(2, ',', '.'),
                v.count,
                'R$ ' + parseFloat(value_total).formatMoney(2, ',', '.'),
                "<a href= '" + v.sell_id + "' class='remove_item'><span class='label label-danger'>REMOVER PRODUTO</span></a>",
                v.item_id
            ]).draw( false );
        });
        sell.getValueTotal();
    },

    deleteSell: function () {
        tool.ajax(url.sell.deleteSell+sell.itens.id_item, null, url.methods.GET, function (result) {
            if (result.code === true) {
                $(location).attr('href', 'pedidos');
            } else {
                tool.messageToast('error', 'ERRO AO EXCLUIR O PEDIDO');
            }
        });
    },

    updateBoards: function () {
        $('#update_board').click(function (e) {
            var valid = $('#boards').val();
            if (valid === null) {
                tool.messageToast('info', "PRECISA TER PELO MENOS UMA MESA INFORMADA!", false);
            } else {
                sell.ajaxUpdateBoard();
            }
        });
    },

    ajaxUpdateBoard: function () {
        tool.ajax(url.sellItem.updateCountBoards, $(sell.form_item_sell).serialize(), url.methods.POST, function (result) {
            if (result.code === true) {
                tool.messageToast('success', 'MESA ATUALIZADA COM SUCESSOERRO');
            } else {
                tool.messageToast('error', 'ERRO AO ATUALIZAR MESAS DO PEDIDO');
            }
        });
    },

    getClient: function () {
        Number.prototype.formatMoney = function(c, d, t){
            var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };
        tool.ajax(url.client.getClient, null, url.methods.GET, function (result) {
            if (result.code === true) {
                $(sell.clients).html('');
                var cmb = "<option value=''> SELECIONE UMA OPÇÃO </option>";
                $.each(result.data, function(i, value) {
                    var value_dept = 'R$ ' + parseFloat(value.value_dept).formatMoney(2, ',', '.');
                    cmb = cmb + '<option value="' + value.id + '">'
                        + value.name +' - ' + value_dept + '</option>';
                });
                $(sell.clients).append(cmb);
            } else {
                tool.messageToast('error', result.data);
            }
        });
    },

    modalSellClient: function () {
        $("#btn-adicionar-sell-client").click(function () {
           $(sell.modal_sell_client).modal({backdrop: 'static', keyboard: false});
        });
    },

    ajaxSellClient: function () {
        $("#sell_client").click(function () {
            $("#sell_client").attr('disabled', true);
            var client_id = $('#client_id').val();
            if (client_id !== '') {
                var data = {
                    'sell_id': sell.itens.id_item,
                    'client_id': client_id,
                    'status': false
                };
                tool.ajax(url.sellClient.saveSellClient, data, url.methods.POST, function (result) {
                    if (result.code === true) {
                        tool.messageToast('success', result.data);
                        $(sell.modal_sell_client).modal('hide');
                        // $('.hide-to-modal').css('display', 'none');
                        $("#sell_client").attr('disabled', false);
                    } else {
                        tool.messageToast('error', result.data);
                        $("#sell_client").attr('disabled', false);
                    }
                });
            } else {
                $("#sell_client").attr('disabled', false);
                tool.messageToast('warning', "INFORME UM CLIENTE");
            }
        });
    },

    modallAddClient: function () {
        $("#btn-adicionar-client").click(function () {
            $(sell.modal_save_client).modal({backdrop: 'static', keyboard: false});
        });
    },

    ajaxSaveClient: function () {
        $("#save_client").click(function () {
            $("#save_client").attr('disabled', true);
            var name = $('#name_client').val();
            if (name !== '') {
                var data = {
                    'name': name
                };
                tool.ajax(url.client.saveClient, data, url.methods.POST, function (result) {
                    if (result.code === true) {
                        tool.messageToast('success', result.data);
                        $(sell.modal_save_client).modal('hide');
                        setTimeout(function(){ $(sell.modal_sell_client).modal({backdrop: 'static', keyboard: false}); }, 1500);
                        sell.getClient();
                        $("#save_client").attr('disabled', false);
                    } else {
                        tool.messageToast('error', result.data);
                        $("#save_client").attr('disabled', false);
                    }
                });
            } else {
                $("#save_client").attr('disabled', false);
                tool.messageToast('warning', "INFORME O NOME DO NOVO CLIENTE");
            }
        });
    },
    
    validSellBox: function () {
        $(sell.form_item_sell_box).validate({
            rules: {
                id_item_active: {
                    required: true
                },
                count_item: {
                    required: true
                }
            },
            submitHandler: function() {
                sell.saveSellBox();
                return false;
            }
        });
    },

    saveSellBox: function () {
        $('#btn-adicionar').prop('disabled', true);
        tool.ajax(url.sellItem.saveSellItem, $(sell.form_item_sell_box).serialize(), url.methods.POST, function (result) {
            if (result.code === true) {
                sell.itens.id_item = result.data;
                $('#id_sell').val(result.data);
                $('#count_item').val(1);
                tool.messageToast('success', 'PRODUTO ADICIONADO AO PEDIDO');
                sell.countSellActive();
                $('.sell_id').fadeIn(500);
                $('#sell_id').text(sell.itens.id_item);
                sell.table_sell_item.clear().draw();
                sell.loadData();
                $('.hide-to-modal').fadeIn(500);
                $('#update_board').fadeIn(500);
                $('#id_item_active').html('<option value="" selected>Selecione uma opção</option>');
            } else {
                tool.messageToast('error', 'ERRO AO CRIAR PEDIDO E ADICIONAR PRODUTO');
                console.log(result);
            }
            $('#btn-adicionar').prop('disabled', false);
        });
    }

};