/**
 * Created by jsj on 23/07/2017.
 */
var item = {

    form: '#save_item',
    tableItemActive: null,
    tableItemDisabled: null,
    btn_submit: 'btn-submit',
    btn_submit_update: 'btn-update-value',
    modal: '#detalhar-produto',

    id: null,
    desc: null,
    obs: null,
    value: null,

    index: function () {
        item.validador();
        item.buttonUpdateValue();
        item.loadItensDisabled();
        item.loadItensActive();
        $('#description').focus();
        Number.prototype.formatMoney = function(c, d, t){
            var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };
    },

    updateValue: function (value, name) {
        value = value.split(' ')[1];
        var data = {
            'description': name,
            'value': tool.convertMoedaToFloat(value),
            'obs': item.obs,
            'status': 1
        };
        tool.disabled(item.btn_submit_update);
        tool.ajax(url.item.updateItem+item.id, data, url.methods.POST, function (result) {
            if (result.code === true) {
                tool.messageToast('success', ' PRODUTO ALTERADO COM SUCESSO');
                $('#update-value').val('');
                $(item.modal).modal('hide');
                item.reloadTable();
            } else {
                tool.messageToast('error', result.message  + ': ' + result.data.toString().replace(',', ' '));
            }
            tool.enabled(item.btn_submit_update);
        });
    },

    saveItem: function () {
        var value = $('#value').val();
        value = value.split(' ')[1];
        var data = {
            'description': $('#description').val(),
            'value': tool.convertMoedaToFloat(value),
            'obs': $('#obs').val(),
            'status': $('#status').val()
        };
        tool.disabled(item.btn_submit);
        tool.ajax(url.item.saveItem, data, url.methods.POST, function (result) {
            if (result.code === true) {
                tool.messageToast('success', result.data.description + ' CRIADO COM SUCESSO!');
                tool.resetForm(item.form);
                item.reloadTable();
            } else {
                tool.messageToast('error', result.message  + ': ' + result.data.toString().replace(',', ' '));
            }
            tool.enabled(item.btn_submit);
        });
    },

    getItembyId: function (id) {
        tool.ajax(url.item.findById+id, null, url.methods.POST, function (result) {
            if (result.code === true) {
                $(item.modal).modal('show');
                item.id = result.data.id;
                item.desc = result.data.description;
                item.obs = result.data.obs;
                item.value = result.data.value;
                var date = tool.formatDateHour(result.data.created_at);
                var value = parseFloat(item.value);
                value  = 'R$ ' + value.formatMoney(2, ',', '.');
                item.detalhes(item.desc, item.obs, value, date);
            } else {
                tool.messageToast('error', result.message  + ': NÃO FOI POSSÍVEL LOCALIZAR ESTE ITEM' );
            }
        });
    },

    validador : function() {
        $(item.form).validate({
            rules: {
                description: {
                    required: true,
                    minlength: 3
                },
                value: {
                    required: true
                },
                obs: {
                    minlength: 3
                },
                status: {
                    required: true
                }
            },
            submitHandler: function() {
                item.saveItem();
                return false;
            }
        });
    },
    
    loadItensActive: function () {
        item.tableItemActive = $('#iten-active').DataTable({
            "language": {
                "url": "js/Portuguese-Brasil.json"
            },
            "ajax": {
                "url":url.item.getItensActivated,
                "dataSrc": "data"
            },
            "columns": [
                {"data": "description"},
                {"data": "value"},
                {"data": "id"},
                {"data": "id"}
            ],
            columnDefs: [
                {
                    render: function (data) {
                        return "<a href= '" + data + "' class='detalhar'><span class='label label-primary'>ALTERAR</span></a>";
                    },
                    targets: 2,
                    "searchable": false,
                    className: "dt-body-center"
                },
                {
                    render: function (data) {
                        return "<a href= '" + data + "' class='dissabled'><span class='label label-danger'>DESATIVAR</span></a>";
                    },
                    targets: 3,
                    "searchable": false,
                    className: "dt-body-center"
                },

                {
                    render: function (data) {
                        data = parseFloat(data);
                        return 'R$ ' + data.formatMoney(2, ',', '.');
                    },
                    targets: 1
                },
                {orderable: false, targets: [2]}
            ]
        });

        item.tableItemActive.on('click', '.detalhar', function (e) {
            e.preventDefault();
            var id = $(this).attr('href');
            item.getItembyId(id);
        });

        item.tableItemActive.on('click', '.dissabled', function (e) {
            e.preventDefault();
            var id = $(this).attr('href');
            item.updateStatus(id, 0);
        });

        Number.prototype.formatMoney = function(c, d, t){
            var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };

    },

    loadItensDisabled: function () {
        item.tableItemDisabled = $('#iten-disabled').DataTable({
            "language": {
                "url": "js/Portuguese-Brasil.json"
            },
            "ajax": {
                "url":url.item.getItensDisabled,
                "dataSrc": "data"
            },
            "columns": [
                {"data": "description"},
                {"data": "value"},
                {"data": "id"}
            ],
            columnDefs: [
                {
                    render: function (data) {
                        return "<a href= '" + data + "' class='enable'><span class='label label-primary'>ATIVAR</span></a>";
                    },
                    targets: 2,
                    "searchable": false,
                    className: "dt-body-center"
                },

                {
                    render: function (data) {
                        data = parseFloat(data);
                        return 'R$ ' + data.formatMoney(2, ',', '.');
                    },
                    targets: 1
                },
                {orderable: false, targets: [2]}
            ]
        });

        item.tableItemDisabled.on('click', '.enable', function (e) {
            e.preventDefault();
            var id = $(this).attr('href');
            item.updateStatus(id, 1);
        });

        Number.prototype.formatMoney = function(c, d, t){
            var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };

    },

    reloadTable: function () {
        item.tableItemDisabled.ajax.reload();
        item.tableItemActive.ajax.reload();
    },

    detalhes: function (desc, obs, value, date) {
        $('#ul-descrption').text(desc);
        $('#ul-obs').text(obs);
        $('#ul-value').text(value);
        $('#ul-create').text(date);
        $('#update-value').val(value);
        $('#update-name').val(desc);
    },

    buttonUpdateValue: function () {
        $("#"+item.btn_submit_update).click(function () {
            var value = $('#update-value').val();
            var name = $('#update-name').val();
            if (value !== '' && name !== '') {
               item.updateValue(value, name);
            } else {
               tool.messageToast('error', 'INFORME O VALOR/NOME QUE DESEJA ATUALIZAR');
            }
        });
    },

    updateStatus: function (id, stauts) {
        var data = {'status': stauts};
        tool.ajax(url.item.updateItemStatus+id, data, url.methods.POST, function (result) {
            if (result.code === true) {
                item.reloadTable();
                tool.messageToast('success', result.message  + ': STATUS ATUALIZADO COM SUCESSO');
            } else {
                tool.messageToast('error', result.message  + ': NÃO FOI POSSÍVEL ATUALIZAR O PRODUTO');
            }
        });
    }
};