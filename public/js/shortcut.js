/**
 * Created by jsj on 11/12/2017.
 */

var template = {

    template: function () {
        this.gestion();
    },

    gestion: function () {

        /**
         * Atalhos de menu
         */

        Mousetrap.bind('f1', function(e) {
            e.preventDefault();
            $('.fa-home').click();
        });

        Mousetrap.bind('f2', function(e) {
            e.preventDefault();
            $('.fa-address-card').click();
        });

        Mousetrap.bind('f3', function(e) {
            e.preventDefault();
            $('.fa-cart-plus').click();
        });

        Mousetrap.bind('f4', function(e) {
            e.preventDefault();
            $('.fa-file-text-o').click();
        });


        Mousetrap.bind('f6', function(e) {
            e.preventDefault();
            $('.fa-windows').click();
        });

        /**
         * Atalhos de sub menu
         */

        Mousetrap.bind('ctrl+m', function(e) {
            e.preventDefault();
            window.location.href = url.pages.board;
        });

        Mousetrap.bind('ctrl+p', function(e) {
            e.preventDefault();
            window.location.href = url.pages.product;
        });

        Mousetrap.bind('ctrl+s', function(e) {
            e.preventDefault();
            window.location.href = url.pages.service;
        });

        Mousetrap.bind('ctrl+e', function(e) {
            e.preventDefault();
            window.location.href = url.pages.establishment;
        });

        Mousetrap.bind('ctrl+g', function(e) {
            e.preventDefault();
            window.location.href = url.pages.waiter;
        });

        Mousetrap.bind('ctrl+c', function(e) {
            e.preventDefault();
            window.location.href = url.pages.client;
        });

        Mousetrap.bind('ctrl+v', function(e) {
            e.preventDefault();
            window.location.href = url.pages.sellBox;
        });

        Mousetrap.bind('ctrl+h', function(e) {
            e.preventDefault();
            window.location.href = url.pages.requests;
        });

        Mousetrap.bind('ctrl+a', function(e) {
            e.preventDefault();
            window.location.href = url.pages.openOrders;
        });

        Mousetrap.bind('ctrl+r', function(e) {
            e.preventDefault();
            window.location.href = url.pages.closedOrders;
        });

        Mousetrap.bind('ctrl+d', function(e) {
            e.preventDefault();
            window.location.href = url.pages.dateInvoice;
        });

        Mousetrap.bind('ctrl+f', function(e) {
            e.preventDefault();
            window.location.href = url.pages.billTable;
        });

        Mousetrap.bind('ctrl+b', function(e) {
            e.preventDefault();
            window.location.href = url.pages.balance;
        });

        Mousetrap.bind('ctrl+k', function(e) {
            e.preventDefault();
            window.location.href = url.pages.customersPendingPayment;
        });
    }
};