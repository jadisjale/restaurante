/**
 * Created by jsj on 23/07/2017.
 */

var sell_client = {

    value_default: 'R$ 00,00',
    clients: '#client_id',
    form_add_dept: '#add_dept',
    id_client: null,

    index: function () {
        sell_client.validadorUpdateprice();
        sell.getClient();
        sell_client.selectClient();
        sell_client.removeDebt();
    },

    removeDebt: function () {
        $('#remove-debt').click(function () {
            sell_client.ajaxupdatePrice(false);
        });
    },

    selectClient: function () {
        $(sell_client.clients).change(function () {
            sell_client.id_client = $(this).val();
            var text = $(this).find('option:selected').text();
            text = text.split('-')[1].trim();
            $('#value_dept').text(text);
        });
    },

    validadorUpdateprice: function() {
        $(sell_client.form_add_dept).validate({
            rules: {
                valor: {
                    required: true
                }
            },
            submitHandler: function() {
                sell_client.ajaxupdatePrice(true);
                return false;
            }
        });
    },

    ajaxupdatePrice: function (value) {
        if (sell_client.id_client !== null) {

            var result = true;

            var valor_debt = $('#value_dept').text();
            valor_debt = valor_debt.split(' ')[1];
                valor_debt = tool.convertMoedaToFloat(valor_debt);

            if (value === true) {
                value = $('#valor').val();
                value = value.split(' ')[1];
                value = tool.convertMoedaToFloat(value);

                var action = $("#add_dept input[type='radio']:checked").val();

                if (action === 'add') {
                    value = value + valor_debt;
                } else {
                    if (value > valor_debt) {
                        tool.messageToast('info', 'VALOR PAGO NÃO PODE SER MAIOR DO QUE O VALOR DA DÍVIDA');
                        result = false;
                    } else {
                        value = valor_debt - value;
                    }
                }
            } else {
                value = 0;
            }
            var data = {
                'id': sell_client.id_client,
                'value_dept': value
            };
            if (result === true) {
                tool.ajax(url.client.updateDept, data, url.methods.POST, function (result) {
                    if (result.code === true) {
                        if (value === 0) {
                            tool.messageToast('success', 'VALOR QUITADO');
                        } else {
                            tool.messageToast('success', 'VALOR ATUALIZADO COM SUCESSO');
                        }
                        sell.getClient();
                        sell_client.id_client = null;
                        $('#value_dept').text(sell_client.value_default);
                        $('#valor').val('');
                    } else {
                        tool.messageToast('error', 'ERRO AO TENTAR ATUALIZAR VALOR');
                    }
                });
            }
        } else {
            tool.messageToast('warning', 'ESCOLHA UM CLIENTE');
        }
    }
};