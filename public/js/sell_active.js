/**
 * Created by jsj on 05/08/2017.
 */

var sell_active = {

    table_sell_active: null,
    table: '#table_sell_active',

    index: function () {
        sell_active.loadData();
        sell_active.actionTable();
    },

    loadData: function () {
        tool.ajax(url.sell.getSellActive, null, url.methods.GET, function (result) {
            if (result.code === true) {
                sell_active.loadTable(sell_active.formatData(result.data));
            } else {
                tool.messageToast('error', 'ERRO AO CARREGAR PEDIDOS EM ABERTOS');
            }
        });
    },

    formatData: function (data) {

        var array_reduce = data.reduce(function(field, e1){
            var matches = field.filter(function(e2){return e1.id== e2.id});
            if (matches.length == 0){
                field.push(e1);
            }return field;
        }, []);

        var array = (JSON.stringify(array_reduce));
        array = $.parseJSON(array);

        var array_sell_active = [];
        $.each(array, function (key, value) {
            array_sell_active.push(mergeData(data, value.id));
        });

        return array_sell_active;

        function mergeData(data, id) {

            var boards = '';
            var array_pedido = [];

            $.each(data, function(key, value) {
                if (id === value.id) {
                    boards = boards + value.description.toString() + ', ';
                }
            });

            boards = boards.substring(0,boards.length - 2);

            $.each(data, function(key, value){
                if (id === value.id) {
                    var sell = {
                        "id": value.id,
                        "created_at": value.created_at,
                        "value_all": value.value_all
                    };
                    var sell_board = $.extend(sell, {"boards": boards});
                    array_pedido.push(sell_board);
                    return false;
                }
            });
            return array_pedido;
        }
    },
    
    loadTable: function (data) {

        sell_active.table_sell_active = $(sell_active.table).DataTable({
            "language": {
                "url": "js/Portuguese-Brasil.json"
            },
            initComplete: function () {

            }
        });

        $.each(data, function (k, v) {
           sell_active.addRowTable(v[0]);
        });
    },

    addRowTable: function (value) {

        Number.prototype.formatMoney = function(c, d, t){
            var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };

        sell_active.table_sell_active.row.add( [
            // value.id,
            value.boards,
            tool.formatDateHour(value.created_at),
            'R$ ' + parseFloat(value.value_all).formatMoney(2, ',', '.'),
            "<a href='"+value.id+"' class='editar center'><span class='label label-success'>EDITAR</span></a>",
            "<a href='"+value.id+"' class='finalizar center'><span class='label label-primary'>FINALIZAR</span></a>"
        ]).draw( false );
    },

    actionTable: function () {

        $('#table_sell_active').on('click','.editar', function(e) {
            e.preventDefault();
            var id = $(this).attr('href');
            window.location.href = "/pedidos_detalhes?sell_id="+id;
        });

        $('#table_sell_active').on('click','.finalizar', function(e) {
            e.preventDefault();
            var id = $(this).attr('href');
            var tr = $(this).parents('tr');
            sell_active.confirmDelete(id, tr);
        });
    },

    confirmDelete: function (id, tr) {
        $.confirm({
            text: "REALMENTE DESEJA FINALIZAR ESSE PEDIDO?",
            title: "CONFIRMAÇÃO REQUERIDA",
            confirm: function() {
                sell_active.finishSell(id , tr);
            },
            cancel: function() {
            },
            confirmButton: "SIM EU QUERO",
            cancelButton: "NÃO",
            confirmButtonClass: "btn-primary",
            cancelButtonClass: "btn-danger",
            dialogClass: "modal-dialog"
        });
    },

    finishSell: function (id, tr) {
        tool.ajax(url.sell.finishSell+id, null, url.methods.GET, function (result) {
            if (result.code === true) {
                tool.messageToast('success', 'PEDIDO FINALIZADO');
                sell_active.removeRowTable(tr);
                sell.countSellActive();
                tool.comanda(id);
                sell_active.table_sell_active.clear().draw();
                sell_active.table_sell_active.destroy();
                sell_active.loadData();
            } else {
                tool.messageToast('error', 'ERRO AO FINALIZAR O PEDIDO');
            }
        });
    },

    removeRowTable: function (tr) {
        sell_active.table_sell_active
            .row( tr )
            .remove()
            .draw();
    },

    editSell: function (id) {

    },

};