var separator = '>>';
var init = 'INÍCIO'+separator;

var way = {

    url: '',
    base: '/',
    init: 'INÍCIO',
    way: '',

    menus: {
        'CADASTRO'  : {
            'MESAS'                     : init + 'CADASTROS' + separator + 'MESAS',
            'PRODUTOS'                  : init + 'CADASTROS' + separator + 'PRODUTOS'
        },
        'GESTAO'    : {
            'GARCONS'                   : init + 'GESTÃO' + separator + 'GARÇONS',
            'CLIENTES'                  : init + 'GESTÃO' + separator + 'CLIENTES'
        },
        'PEDIDOS'   : {
            'CADASTRAR_PEDIDO'          : init + 'PEDIDOS' + separator + 'CADASTRAR PEDIDO',
            'PEDIDOS_EM_ABERTO'         : init + 'PEDIDOS' + separator + 'PEDIDOS EM ABERTO',
            'PEDIDOS_ENCERRADOS'        : init + 'PEDIDOS' + separator + 'PEDIDOS ENCERRADOS',
            'PEDIDOS_FECHADOS'          : init + 'PEDIDOS' + separator + 'PEDIDOS ENCERRADOS' + separator + 'PEDIDOS FECHADOS DETALHES',
        },
        'RELATORIOS': {
            'FATURA_POR_DATA'           : init + 'RELATÓRIOS' + separator + 'FATURA POR DATA',
            'FATURA_POR_MESA'           : init + 'RELATÓRIOS' + separator + 'FATURA POR MESA',
            'BALANCO'                   : init + 'RELATÓRIOS' + separator + 'BALANCO'
        },
        'EXTRA'     : {
            'CLIENTE_PENDENTE_PAGAMENTO': init + 'EXTRA' + separator + 'CLIENTE PENDENTE PAGAMENTO'
        },

    },

    index: function () {
        way.constructWay();
        way.mouseOuverWay();
    },

    constructWay: function () {
        switch(window.location.pathname) {
            case way.base+'mesas':
                way.way = way.menus.CADASTRO.MESAS;
                break;
            case way.base+'produto':
                way.way = way.menus.CADASTRO.PRODUTOS;
                break;
            case way.base+'garcon':
                way.way = way.menus.GESTAO.GARCONS;
                break;
            case way.base+'cliente':
                way.way = way.menus.GESTAO.CLIENTES;
                break;
            case way.base+'pedidos_abertos':
                way.way = way.menus.PEDIDOS.PEDIDOS_EM_ABERTO;
                break;
            case way.base+'pedidos':
                way.way = way.menus.PEDIDOS.CADASTRAR_PEDIDO;
                break;
            case way.base+'pedidos_fechados':
                way.way = way.menus.PEDIDOS.PEDIDOS_ENCERRADOS;
                break;
            case way.base+'fatura':
                way.way = way.menus.RELATORIOS.FATURA_POR_DATA;
                break;
            case way.base+'balanco':
                way.way = way.menus.RELATORIOS.BALANCO;
                break;
            case way.base+'fatura_mesa':
                way.way = way.menus.RELATORIOS.FATURA_POR_MESA;
                break;
            case way.base+'clientes_pendente_pagamento':
                way.way = way.menus.EXTRA.CLIENTE_PENDENTE_PAGAMENTO;
                break;
            case way.base+'pedidos_fechados_detalhes':
                way.way = way.menus.PEDIDOS.PEDIDOS_FECHADOS;
                break;
            default:
                way.way = way.init;
                break;
        }

        way.setWay();
    },

    setWay: function () {
        $('#way').text(way.way);
    },


    mouseOuverWay: function () {
        var way = $('.way');
        var count = 500;

        way.mouseover(function() {
            $(this).animate({
                height: '79px'
            }, count, "linear", function(e) {
                count = 0;
            });
        });

        $('#hide-way').click(function () {
            way.animate({
                height: '56px'
            }, 500, "linear", function(e) {
                count = 500;
            });
        });

    }

};