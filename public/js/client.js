/**
 * Created by jsj on 23/07/2017.
 */
var client = {

    table: null,
    form: '#save_cliente',
    btn_submit: 'btn-submit',

    index: function () {
        client.validador();
        client.loadClients();
        $('#name').focus();
    },

    saveClient: function () {
        tool.disabled(client.btn_submit);
        tool.ajax(url.client.saveClient, $(client.form).serialize(), url.methods.POST, function (result) {
            if (result.code === true) {
                tool.messageToast('success', 'CLIENTE ADICIONADO COM SUCESSO!');
                tool.resetForm(client.form);
                client.table.ajax.reload();
            } else {
                tool.messageToast('error', result.message  + ': ' + result.data.toString().replace(',', ' '));
            }
            tool.enabled(client.btn_submit);
        });
    },

    validador : function() {
        $(client.form).validate({
            rules: {
                name: {
                    required: true,
                    minlength: 3
                }
            },
            submitHandler: function() {
                client.saveClient();
                return false;
            }
        });
    },

    loadClients: function () {
        client.table = $('#list_clients').DataTable({
            "language": {
                "url": "js/Portuguese-Brasil.json"
            },
            "ajax": {
                "url":url.client.getClient,
                "dataSrc": "data"
            },
            "columns": [
                {"data": "name"},
            ]
        });

        item.tableItemActive.on('click', '.detalhar', function (e) {
            e.preventDefault();
            var id = $(this).attr('href');
            item.getItembyId(id);
        });

        item.tableItemActive.on('click', '.dissabled', function (e) {
            e.preventDefault();
            var id = $(this).attr('href');
            item.updateStatus(id, 0);
        });

        Number.prototype.formatMoney = function(c, d, t){
            var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };

    },

};