/**
 * Created by jsj on 05/08/2017.
 */

var sell_inactive = {

    table_sell_inactive: null,
    table: '#table_sell_inactive',
    form_sell_inactive: '#form-sell-inactive',

    index: function () {
        tool.calendar();
        // sell_inactive.loadData();
        sell_inactive.validador();

        sell_inactive.table_sell_inactive = $(sell_inactive.table).DataTable({
            "language": {
                "url": "js/Portuguese-Brasil.json"
            }
        });

        $(sell_inactive.form_sell_inactive).submit();

    },

    printComanda: function () {
        $('#table_sell_inactive tbody').on( 'click', '.comanda', function (e) {
            e.preventDefault();
            tool.comanda($(this).attr('href'));
        });
    },

    loadData: function () {
        $('#btn-consultar').prop('disabled', true);
        tool.ajax(url.sell.getSellInactive, fatura.convertDateBrToDefault(), url.methods.POST, function (result) {
            if (result.code === true) {
                sell_inactive.loadTable(sell_inactive.formatData(result.data));
            } else {
                tool.messageToast('error', 'ERRO AO CARREGAR PEDIDOS FECHADOS');
            }
            $('#btn-consultar').prop('disabled', false);
        });
    },

    validador: function() {
        $(sell_inactive.form_sell_inactive).validate({
            rules: {
                date: {
                    required: true
                }
            },
            submitHandler: function() {
                sell_inactive.loadData();
                return false;
            }
        });
    },

    formatData: function (data) {

        var array_reduce = data.reduce(function(field, e1){
            var matches = field.filter(function(e2){return e1.id== e2.id});
            if (matches.length == 0){
                field.push(e1);
            }return field;
        }, []);
        var array = (JSON.stringify(array_reduce));
        array = $.parseJSON(array);
        var array_sell_inactive = [];
        $.each(array, function (key, value) {
            array_sell_inactive.push(mergeData(data, value.id));
        });
        return array_sell_inactive;

        function mergeData(data, id) {

            var boards = '';
            var array_pedido = [];

            $.each(data, function(key, value) {
                if (id === value.id) {
                    boards = boards + value.description.toString() + ', ';
                }
            });

            boards = boards.substring(0,boards.length - 2);

            $.each(data, function(key, value){
                if (id === value.id) {
                    var sell = {
                        "id": value.id,
                        "created_at": value.created_at,
                        "value_all": value.value_all
                    };
                    var sell_board = $.extend(sell, {"boards": boards});
                    array_pedido.push(sell_board);
                    return false;
                }
            });
            return array_pedido;
        }
    },
    
    loadTable: function (data) {

        sell_inactive.table_sell_inactive.clear().draw();

        $.each(data, function (k, v) {
           sell_inactive.addRowTable(v[0]);
        });
    },

    addRowTable: function (value) {

        Number.prototype.formatMoney = function(c, d, t){
            var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };

        sell_inactive.table_sell_inactive.row.add( [
            // value.id,
            value.boards,
            // value.created_at,
            tool.formatDateHour(value.created_at),
            'R$ ' + parseFloat(value.value_all).formatMoney(2, ',', '.'),
            "<a href='/pedidos_fechados_detalhes?sell_id="+value.id+"' class='editar center'><span class='label label-primary'>VISUALIZAR</span></a>",
            "<a href='"+value.id+"' class='comanda center'><span class='label label-primary'>COMANDA</span></a>"
        ]).draw( false );
    },

    confirmDelete: function (id, tr) {
        $.confirm({
            text: "REALMENTE DESEJA FINALIZAR ESSE PEDIDO?",
            title: "CONFIRMAÇÃO REQUERIDA",
            confirm: function() {
                sell_inactive.finishSell(id , tr);
            },
            cancel: function() {
            },
            confirmButton: "SIM EU QUERO",
            cancelButton: "NÃO",
            confirmButtonClass: "btn-primary",
            cancelButtonClass: "btn-danger",
            dialogClass: "modal-dialog"
        });
    },

    finishSell: function (id, tr) {
        tool.ajax(url.sell.finishSell+id, null, url.methods.GET, function (result) {
            if (result.code === true) {
                tool.messageToast('success', 'PEDIDO FINALIZADO');
                sell_inactive.removeRowTable(tr);
                sell.countSellActive();
            } else {
                tool.messageToast('error', 'ERRO AO FINALIZAR O PEDIDO');
            }
        });
    },

    removeRowTable: function (tr) {
        sell_inactive.table_sell_inactive
            .row( tr )
            .remove()
            .draw();
    },
};