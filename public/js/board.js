/**
 * Created by jsj on 23/07/2017.
 */
var board = {

    form: '#save_mesa',
    tableActive: null,
    tableDisabled: null,
    btn_submit: 'btn-submit',

    index: function () {
        board.validador();
        board.loadBoardActive();
        board.loadBoardDisabled();
        board.drawTables();
        $('#description').focus();
    },

    saveBoard: function () {
        tool.disabled(board.btn_submit);
        tool.ajax(url.board.saveBoard, $(board.form).serialize(), url.methods.POST, function (result) {
            if (result.code === true) {
                tool.messageToast('success','MESA '+ result.data.description + ' CRIADO COM SUCESSO!');
                tool.resetForm(board.form);
                board.tableActive.ajax.reload();
                board.tableDisabled.ajax.reload();
            } else {
                tool.messageToast('error', result.message  + ': ' + result.data.toString().replace(',', ' '));
            }
            tool.enabled(board.btn_submit);
        });
    },

    validador : function() {
        $(board.form).validate({
            rules: {
                description: {
                    required: true,
                    minlength: 1
                },
                status: {
                    required: true
                }
            },
            submitHandler: function() {
                board.saveBoard();
                return false;
            }
        });
    },
    
    loadBoardActive: function () {
        board.tableActive = $('#board-active').DataTable({

            // "bPaginate": false,
            // "bFilter": false,
            // "bInfo": false,

            "language": {
                "url": "js/Portuguese-Brasil.json"
            },
            "ajax": {
                "url":url.board.getBoardActivated,
                "dataSrc": "data"
            },
            "columns": [
                {"data": "description"},
                {"data": "id"}
            ],
            columnDefs: [
                {
                    render: function (data) {
                        return "<a href= '" + data + "' class='update-disabled'><span class='label label-danger'>DESATIVAR</span></a>";
                    },
                    targets: 1,
                    "searchable": false,
                    className: "dt-body-center"
                },
                {orderable: false, targets: [1]}
            ]
        });

        board.tableActive.on('click', '.update-disabled', function (e) {
            e.preventDefault();
            var id = $(this).attr('href');
            board.update(id, 0);
        });
    },

    loadBoardDisabled: function () {
        board.tableDisabled = $('#board-disabled').DataTable({

            // "bPaginate": false,
            // "bFilter": false,
            // "bInfo": false,

            "language": {
                "url": "js/Portuguese-Brasil.json"
            },
            "ajax": {
                "url": url.board.getBoardDisabled,
                "dataSrc": "data"
            },
            "columns": [
                {"data": "description"},
                {"data": "id"}
            ],
            columnDefs: [
                {
                    render: function (data) {
                        return "<a href= '" + data + "' class='update-active'><span class='label label-primary'>ATIVAR</span></a>";
                    },
                    targets: 1,
                    "searchable": false,
                    "className": "dt-body-center"
                },
                {orderable: false, targets: [1]}
            ]
        });

        board.tableDisabled.on('click', '.update-active', function (e) {
            e.preventDefault();
            var id = $(this).attr('href');
            board.update(id, 1);
        });
    },

    drawTables: function () {
        $('#li-active').click(function () {
            board.tableActive.ajax.reload();
        });
        $('#li-disabled').click(function () {
            board.tableDisabled.ajax.reload();
        });
    },

    update: function (id, status) {
        var data = {'status': status};
        tool.ajax(url.board.updateBoard+id, data, url.methods.POST, function (result) {
            if (result.code === true) {
                tool.messageToast('success', 'MESA ATUALIZADA COM SUCESSO!');
                board.tableActive.ajax.reload();
                board.tableDisabled.ajax.reload();
            } else if(result.code === false) {
                tool.messageToast('error', result.message  + ': ' + result.data);
            } else {
                tool.messageToast('info', result.message  + ': ' + result.data);
            }
        });
    }
};