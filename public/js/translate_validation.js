 /*
* Translated default messages for the jQuery validation plugin.
* Locale: PT_BR
*/
jQuery.extend(jQuery.validator.messages, {
    required: "CAMPO OBRIGATÓRIO.",
    remote: "POR FAVOR, CORRIJA ESTE CAMPO.",
    email: "POR FAVOR, FORNEÇA UM EMAIL VÁLIDO.",
    url: "POR FAVOR, FORNEÇA UMA URL VÁLIDA.",
    date: "POR FAVOR, FORNEÇA UMA DATA VÁLIDA.",
    dateISO: "POR FAVOR, FORNEÇA UMA DATA (ISO).",
    number: "POR FAVOR, FORNEÇA UM NÚMERO VÁLIDO.",
    digits: "POR FAVOR, FORNEÇA SOMENTE DIGITOS.",
    creditcard: "POR FAVOR, FORNEÇA UM CARACTER DE DIGITO VÁLIDO.",
    equalTo: "Por favor, forne&ccedil;a o mesmo valor novamente.",
    accept: "Por favor, forne&ccedil;a um valor com uma extens&atilde;o v&aacute;lida.",
    maxlength: jQuery.validator.format("Por favor, forne&ccedil;a n&atilde;o mais que {0} caracteres."),
    minlength: jQuery.validator.format("Por favor, forne&ccedil;a ao menos {0} caracteres."),
    rangelength: jQuery.validator.format("Por favor, forne&ccedil;a um valor entre {0} e {1} caracteres de comprimento."),
    range: jQuery.validator.format("Por favor, forne&ccedil;a um valor entre {0} e {1}."),
    max: jQuery.validator.format("Por favor, forne&ccedil;a um valor menor ou igual a {0}."),
    min: jQuery.validator.format("Por favor, forne&ccedil;a um valor maior ou igual a {0}.")
});