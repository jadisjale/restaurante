/**
 * Created by jsj on 23/07/2017.
 */
var waiter = {

    form: '#save_waiter',
    tableActive: null,
    tableDisabled: null,
    btn_submit: 'btn-submit',

    index: function () {
        waiter.validador();
        waiter.loadwaiterActive();
        waiter.loadwaiterDisabled();
        waiter.drawTables();
        $('#name').focus();
    },

    saveWaiter: function () {
        tool.disabled(waiter.btn_submit);
        tool.ajax(url.waiter.saveWaiter, $(waiter.form).serialize(), url.methods.POST, function (result) {
            if (result.code === true) {
                tool.messageToast('success', result.data.name + ' CRIADO COM SUCESSO');
                tool.resetForm(waiter.form);
                waiter.tableActive.ajax.reload();
                waiter.tableDisabled.ajax.reload();
            } else {
                tool.messageToast('error', result.message  + ': ' + result.data.toString().replace(',', ' '));
            }
            tool.enabled(waiter.btn_submit);
        });
    },

    validador : function() {
        $(waiter.form).validate({
            rules: {
                name: {
                    required: true,
                    minlength: 3
                },
                status: {
                    required: true
                }
            },
            submitHandler: function() {
                waiter.saveWaiter();
                return false;
            }
        });
    },
    
    loadwaiterActive: function () {
        waiter.tableActive = $('#waiter-active').DataTable({

            // "bPaginate": false,
            // "bFilter": false,
            // "bInfo": false,

            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            },
            "ajax": {
                "url":url.waiter.getWaiterActivated,
                "dataSrc": "data"
            },
            "columns": [
                {"data": "name"},
                {"data": "id"}
            ],
            columnDefs: [
                {
                    render: function (data) {
                        return "<a href= '" + data + "' class='update-disabled'><span class='label label-danger'>DESATIVAR</span></a>";
                    },
                    targets: 1,
                    "searchable": false,
                    className: "dt-body-center"
                },
                {orderable: false, targets: [1]}
            ]
        });

        waiter.tableActive.on('click', '.update-disabled', function (e) {
            e.preventDefault();
            var id = $(this).attr('href');
            waiter.update(id, 0);
        });
    },

    loadwaiterDisabled: function () {
        waiter.tableDisabled = $('#waiter-inactive').DataTable({

            // "bPaginate": false,
            // "bFilter": false,
            // "bInfo": false,

            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json" ///fazer alteração
            },
            "ajax": {
                "url": url.waiter.getWaiterDisabled,
                "dataSrc": "data"
            },
            "columns": [
                {"data": "name"},
                {"data": "id"}
            ],
            columnDefs: [
                {
                    render: function (data) {

                        return "<a href= '" + data + "' class='update-active'><span class='label label-primary'>ATIVAR</span></a>";
                    },
                    targets: 1,
                    "searchable": false,
                    "className": "dt-body-center"
                },
                {orderable: false, targets: [1]}
            ]
        });

        waiter.tableDisabled.on('click', '.update-active', function (e) {
            e.preventDefault();
            var id = $(this).attr('href');
            waiter.update(id, 1);
        });
    },

    drawTables: function () {
        $('#li-active').click(function () {
            waiter.tableActive.ajax.reload();
        });
        $('#li-disabled').click(function () {
            waiter.tableDisabled.ajax.reload();
        });
    },

    update: function (id, status) {
        var data = {'status': status};
        tool.ajax(url.waiter.updateWaiter+id, data, url.methods.POST, function (result) {
            if (result.code === true) {
                tool.messageToast('success', 'DADOS DO GARÇOM ATUALIZADO COM SUCESSO!');
                waiter.tableActive.ajax.reload();
                waiter.tableDisabled.ajax.reload();
            } else {
                console.log(result.data);
                tool.messageToast('error', result.message  + ': NÃO FOI POSSÍVEL ATUALIZAR OS DADOS');
            }
        });
    }
};