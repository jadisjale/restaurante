/**
 * Created by jsj on 23/07/2017.
 */
var service = {

    form: '#save_service',
    tableActive: null,
    tableDisabled: null,
    btn_submit: 'btn-submit',

    index: function () {
        service.validador();
        service.loadServiceActive();
        service.loadServiceDisabled();
        service.drawTables();
        service.buttonUpdateService();
        $('#name').focus();

        Number.prototype.formatMoney = function(c, d, t){
            var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };

    },

    saveService: function () {
        tool.disabled(service.btn_submit);
        var value = $('#value').val();
        value = value.split(' ')[1];
        var data = {
            'name': $('#name').val(),
            'value': tool.convertMoedaToFloat(value),
            'status': $('#status').val()
        };

        tool.ajax(url.service.save, data, url.methods.POST, function (result) {
            if (result.code === true) {
                tool.messageToast('success', 'SERVIÇO '+ result.data.name + ' CRIADO COM SUCESSO!');
                tool.resetForm(service.form);
                // service.tableActive.ajax.reload();
                // service.tableDisabled.ajax.reload();
            } else {
                tool.messageToast('error', result.message  + ': ' + result.data.toString().replace(',', ' '));
            }
            tool.enabled(service.btn_submit);
        });
    },

    validador : function() {
        $(service.form).validate({
            rules: {
                name: {
                    required: true,
                    minlength: 1
                },
                value: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            submitHandler: function() {
                service.saveService();
                return false;
            }
        });
    },
    
    loadServiceActive: function () {
        service.tableActive = $('#service-active').DataTable({

            // "bPaginate": false,
            // "bFilter": false,
            // "bInfo": false,

            "language": {
                "url": "js/Portuguese-Brasil.json"
            },
            "ajax": {
                "url":url.service.getServiceActivated,
                "dataSrc": "data"
            },
            "columns": [
                {"data": "name"},
                {"data": "value"},
                {"data": "id"},
                {"data": "id"}
            ],
            columnDefs: [
                {
                    render: function (data) {
                        return "<a href= '" + data + "' class='update-disabled'><span class='label label-danger'>DESATIVAR</span></a>";
                    },
                    targets: 2,
                    "searchable": false,
                    className: "dt-body-center"
                },
                {
                    render: function (data) {
                        return "<a href= '" + data + "' class='update-edit'><span class='label label-success'>EDITAR</span></a>";
                    },
                    targets: 3,
                    "searchable": false,
                    className: "dt-body-center"
                },
                {
                    render: function (data) {
                        data = parseFloat(data);
                        return 'R$ ' + data.formatMoney(2, ',', '.');
                    },
                    targets: 1
                },
                {orderable: false, targets: [2, 3]}
            ]
        });

        service.tableActive.on('click', '.update-disabled', function (e) {
            e.preventDefault();
            var id = $(this).attr('href');
            service.update(id, false);
        });

        service.tableActive.on('click', '.update-edit', function (e) {
            e.preventDefault();
            var tr = $(this).parent('td').parent('tr');
            var object = service.tableActive.row( tr ).data();
            service.modalUpdate(object);
        });
    },

    modalUpdate: function (object) {
        var value = parseFloat(object.value);
        value  = 'R$ ' + value.formatMoney(2, ',', '.');
        $('#modal-update-service').modal('show');
        $('#ul-name').text(object.name);
        $('#update-id').val(object.id);
        $('#ul-value').text(value);
    },

    buttonUpdateService: function () {
        $("#btn-update-service").click(function () {
            var value = $('#update-value').val();
            var name = $('#update-name').val();
            var id = $('#update-id').val();
            if (value !== '' && name !== '') {
                service.updateService(id, name, value);
            } else {
                tool.messageToast('error', 'INFORME O VALOR/NOME QUE DESEJA ATUALIZAR');
            }
        });
    },

    updateService: function (id, name, value) {
        value = value.split(' ')[1];
        var data = {
            'name': name,
            'value': tool.convertMoedaToFloat(value)
        };
        tool.ajax(url.service.updateServiceObject+id, data, url.methods.POST, function (result) {
            if (result.code === true) {
                tool.messageToast('success', 'SERVIÇO ATUALIZADO COM SUCESSO!');
                service.tableActive.ajax.reload();
                service.tableDisabled.ajax.reload();
                $('#modal-update-service').modal('hide');
            } else if(result.code === false) {
                tool.messageToast('error', result.message  + ': ' + result.data);
            } else {
                tool.messageToast('info', result.message  + ': ' + result.data);
            }
        });
    },

    loadServiceDisabled: function () {
        service.tableDisabled = $('#service-disabled').DataTable({

            // "bPaginate": false,
            // "bFilter": false,
            // "bInfo": false,

            "language": {
                "url": "js/Portuguese-Brasil.json"
            },
            "ajax": {
                "url": url.service.getServiceDisabled,
                "dataSrc": "data"
            },
            "columns": [
                {"data": "name"},
                {"data": "value"},
                {"data": "id"}
            ],
            columnDefs: [
                {
                    render: function (data) {
                        return "<a href= '" + data + "' class='update-active'><span class='label label-primary'>ATIVAR</span></a>";
                    },
                    targets: 2,
                    "searchable": false,
                    "className": "dt-body-center"
                },
                {
                    render: function (data) {
                        data = parseFloat(data);
                        return 'R$ ' + data.formatMoney(2, ',', '.');
                    },
                    targets: 1
                },
                {orderable: false, targets: [2]}
            ]
        });

        service.tableDisabled.on('click', '.update-active', function (e) {
            e.preventDefault();
            var id = $(this).attr('href');
            service.update(id, 1);
        });
    },

    drawTables: function () {
        $('#li-active').click(function () {
            service.tableActive.ajax.reload();
        });
        $('#li-disabled').click(function () {
            service.tableDisabled.ajax.reload();
        });
    },

    update: function (id, status) {
        var data = {'status': status};
        tool.ajax(url.service.updateService+id, data, url.methods.POST, function (result) {
            if (result.code === true) {
                tool.messageToast('success', 'SERVIÇO ATUALIZADO COM SUCESSO!');
                service.tableActive.ajax.reload();
                service.tableDisabled.ajax.reload();
            } else if(result.code === false) {
                tool.messageToast('error', result.message  + ': ' + result.data);
            } else {
                tool.messageToast('info', result.message  + ': ' + result.data);
            }
        });
    }
};